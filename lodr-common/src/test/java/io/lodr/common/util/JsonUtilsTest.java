package io.lodr.common.util;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public class JsonUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testListWithNullValues() throws JsonProcessingException {
		List<String> values = new ArrayList<>();
	
		values.add(null);
		values.add("value - 1");
		values.add(null);
		values.add("value - 2");
		values.add(null);
		
		values.forEach(v -> System.out.println(v));
		
		System.out.println("json = " + JsonUtils.toJsonString(values));
	}

}

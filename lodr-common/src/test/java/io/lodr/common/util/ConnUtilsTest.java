package io.lodr.common.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.lodr.common.model.ConnInfo;

public class ConnUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRedshiftCopyCommand() throws SQLException {
		ConnInfo connInfo = new ConnInfo();
		connInfo.setServerAddress("vero-customer-dw-instance.cul81qssgn06.us-west-2.redshift.amazonaws.com");
		connInfo.setUsername("veroadmin");
		connInfo.setPassword("ojAniluYiaT88");
		connInfo.setDatabaseName("lodr");
		connInfo.getProps().put("ssl", "true");
		connInfo.getProps().put("sslfactory", "com.amazon.redshift.ssl.NonValidatingFactory");
		
		try (Connection conn = ConnUtils.getConnection(connInfo);
				Statement statement = conn.createStatement()) {
			String copyCommand = "copy l_1Mil50Col from 's3://lodr-lambda/1/splits/3bce8bfc-6a5e-4eba-b3d1-43dbc8cc6d09_' credentials 'aws_access_key_id=AKIAJ6AUWRHEZWKBDESA;aws_secret_access_key=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' gzip removequotes region 'us-west-2'";
			long startTime = System.currentTimeMillis();
			statement.execute(copyCommand);
			System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms");
		}
	}

}

package io.lodr.common.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class DbUtilsTest {

	@Test
	public void testQuote() {
		assertEquals("\"foo\".\"bar\"", DbUtils.quote("foo.bar"));
		assertEquals("\"foo\"", DbUtils.quote("foo"));
		assertEquals("\"foo.bar\".\"cool\"", DbUtils.quote("foo.bar.cool"));
	}

}

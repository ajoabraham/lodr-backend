package io.lodr.common.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVPrinter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CSVUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCsvOutputWithEmptyValue() throws FileNotFoundException, UnsupportedEncodingException, IOException {
		try (CSVPrinter out = CSVUtils.createCSVPrinter(new PrintWriter("CsvWithEmptyValues.csv", StandardCharsets.UTF_8.name()), "|", "\"")) {
			List<String> values = new ArrayList<>();
			values.add("");
			for (int i = 0; i < 10; i++) {
				values.add("value-" + i);
			}
			
			out.printRecord(values);
		}
	}

}

package io.lodr.common.util;

import static org.junit.Assert.*;

import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.lodr.common.constant.LoadMode;
import io.lodr.common.model.RunJobInputData;

public class ValidatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRunJobInputDataValidation() {
		RunJobInputData input = new RunJobInputData();
		
		input.setJobId(UUID.randomUUID().toString());
		input.setLoadMode(LoadMode.MERGE);
		
		String msg = Validator.I.validateInput(input);
		System.out.println(msg);
		
		assertNotNull(msg);
	}

}

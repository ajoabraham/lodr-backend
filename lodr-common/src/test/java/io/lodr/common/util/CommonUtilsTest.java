package io.lodr.common.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import io.lodr.common.constant.CommonConstants;

public class CommonUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsDate() {
		assertTrue(CommonUtils.isDate("10/03/16"));
	}
	
	@Test
	public void testIsTimestamp() {
		assertTrue(CommonUtils.isTimestamp("2017-03-21T03:12:04.147714"));
		assertTrue(CommonUtils.isTimestamp("2017-03-21 03:12:09.150240"));
		assertTrue(CommonUtils.isTimestamp("2017-08-11T00:00:00.067Z"));
	}
	
	@Test
	public void testParseDate() {
		String dateString = CommonUtils.parseDate("09/10/16").toString();
		System.out.println(dateString);
		assertEquals("Sat Sep 10 00:00:00 EDT 2016", dateString);
	}
	
	@Test
	public void testBatchSizeSysProp(){
		System.setProperty("batchSize", "1024");
		assertEquals("1024", System.getProperty("batchSize"));
		assertEquals(1024*1024*1024, CommonConstants.DEFAULT_BATCH_SIZE);
	}
	
	@Test
	public void testDefaultBatchSize(){
		assertEquals(200*1024*1024, CommonConstants.DEFAULT_BATCH_SIZE);
	}
}

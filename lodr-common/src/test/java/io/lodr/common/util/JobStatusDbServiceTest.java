package io.lodr.common.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.lodr.common.constant.DataType;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.FieldError;
import io.lodr.common.model.JobStatus;

public class JobStatusDbServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDynamoDBCapacity() throws Exception {
		List<JobStatus> jobs = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			JobStatus job = new JobStatus();
			job.setJobId(UUID.randomUUID().toString());
			job.setBatchId(0);
			job.setStartTime(new Date());
			
			for (int j = 0; j < 100; j++) {
				ColumnInfo ci = new ColumnInfo();
				ci.setColumnName("COLUMN_" + j);
				ci.setColumnPosition(j);
				ci.setDataType(DataType.VARCHAR);
				ci.setLength(256);
				
				job.getColumnInfos().add(ci);
			}
			
			jobs.add(job);
		}
		
		JobStatusDbService.save(jobs);
	}
	
	@Test
	public void testJobStatusSave() throws Exception {
		JobStatus job = new JobStatus();
		job.setJobId(UUID.randomUUID().toString());
		job.setBatchId(0);
		job.setStartTime(new Date());
		
		for (int j = 0; j < 10; j++) {
			ColumnInfo ci = new ColumnInfo();
			ci.setColumnName("COLUMN_" + j);
			ci.setColumnPosition(j);
			ci.setDataType(DataType.VARCHAR);
			ci.setLength(256);
			
			job.getColumnInfos().add(ci);
		}
		
		FieldError fieldError = new FieldError("COLUMN_1");
		fieldError.incrementFailedRows();
		job.getFieldErrors().add(fieldError);
		
		JobStatusDbService.save(job);
	}
}

package io.lodr.common.model.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.FilterValueType;
import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.JsonUtils;

public class CommandTest {
	RowData row = null;
	
	@Before
	public void setUp() throws Exception {
		row = new RowData();
		row.getColumnDatas().add(new ColumnData("col0", "This is Column 0", 0));
		row.getColumnDatas().add(new ColumnData("col1", "This is Column 1", 1));
		row.getColumnDatas().add(new ColumnData("col2", "This is Column 2", 2));
		row.getColumnDatas().add(new ColumnData("col3", "This is Column 3", 3));
		row.getColumnDatas().add(new ColumnData("col4", "This is Column 4", 4));
		row.getColumnDatas().add(new ColumnData("col5", "This is Column 5", 5));
		row.getColumnDatas().add(new ColumnData("col6", "This is Column 6", 6));
		row.getColumnDatas().add(new ColumnData("col7", "This is Column 7", 7));
		row.getColumnDatas().add(new ColumnData("col8", "This is Column 8", 8));
		row.getColumnDatas().add(new ColumnData("col9", "This is Column 9", 9));
	}

	@After
	public void tearDown() throws Exception {
		row = null;
	}

	@Test
	public void testJson() throws IOException {
		List<Command> commands = new ArrayList<>();
		
		Command deleteCommand = new DeleteCommand();
		deleteCommand.setColumnName("col1");
		commands.add(deleteCommand);
		
		Command renameCommand = new RenameCommand();
		renameCommand.setColumnName("col1");
		commands.add(renameCommand);
		
		Command moveCommand = new MoveCommand();
		moveCommand.setColumnName("col1");
		moveCommand.getOpts().put("position", "1");
		commands.add(moveCommand);
		
		Command mergeCommand = new MergeCommand();
		mergeCommand.setColumnName("col1");
		mergeCommand.getOpts().put("delimiter", "-");
		commands.add(mergeCommand);
		
		Command upperCaseCommand = new UpperCaseCommand();
		upperCaseCommand.setColumnName("col1");
		commands.add(upperCaseCommand);
		
		Command lowerCaseCommand = new LowerCaseCommand();
		lowerCaseCommand.setColumnName("col1");
		commands.add(lowerCaseCommand);
		
		String json = JsonUtils.toJsonString(commands, new TypeReference<List<Command>>() {});
		System.out.println(json);
		
		List<Command> deserializedCommands = JsonUtils.toObject(new TypeReference<List<Command>>(){}, json);
		
		assertEquals(commands.size(), deserializedCommands.size());
	}
	
	@Test
	public void testDeleteCommand() {
		Command command = new DeleteCommand();
		command.setColumnName("col2");
		
		RowData r = command.execute(row).get(0);
		assertEquals(9, r.getColumnDatas().size());
		assertEquals("col3", r.getColumnDatas().get(2).getName());
		assertEquals(2, (int)r.getColumnDatas().get(2).getPosition());
	}
	
	@Test
	public void testLowerCaseCommand() {
		Command command = new LowerCaseCommand();
		command.setColumnName("col2");
		RowData r = command.execute(row).get(0);
		
		assertEquals("this is column 2", r.getColumnDatas().get(2).getValue());
	}
	
	@Test
	public void testMergeCommand() {
		Command command = new MergeCommand();
		command.setColumnName("col2");
		command.getOpts().put("target_column_name", "col6");
		command.getOpts().put("delimiter", "-");
		
		RowData r = command.execute(row).get(0);
		assertEquals(9, r.getColumnDatas().size());
		assertEquals("This is Column 2-This is Column 6", r.getColumnDatas().get(2).getValue());
	}
	
	@Test
	public void testMoveCommand() {
		Command command = new MoveCommand();
		command.setColumnName("col3");
		command.getOpts().put("position", "1");
	
		RowData r = command.execute(row).get(0);
		assertEquals(10, r.getColumnDatas().size());
		assertEquals("This is Column 3", r.getColumnDatas().get(1).getValue());
		assertEquals(1, (int) r.getColumnDatas().get(1).getPosition());
	}
	
	@Test
	public void testRenameCommand() {
		Command command = new RenameCommand();
		command.setColumnName("col3");
		command.getOpts().put("new_column_name", "col13");
		
		RowData r = command.execute(row).get(0);
		assertEquals("col13", r.getColumnDatas().get(3).getName());
	}
	
	@Test
	public void testUpperCaseCommand() {
		Command command = new UpperCaseCommand();
		command.setColumnName("col2");
		RowData r = command.execute(row).get(0);
		
		assertEquals("THIS IS COLUMN 2", r.getColumnDatas().get(2).getValue());
	}
	
	@Test
	public void testMultipleCommands() {
		row = new RowData();
		row.getColumnDatas().add(new ColumnData("ProductID", "43", 0));
		row.getColumnDatas().add(new ColumnData("date", "2/22/14", 1));
		row.getColumnDatas().add(new ColumnData("impressions", "32644", 2));
		row.getColumnDatas().add(new ColumnData("clicks", "1265", 3));
		row.getColumnDatas().add(new ColumnData("comments", "8", 4));
		row.getColumnDatas().add(new ColumnData("likes", "53044", 5));
		row.getColumnDatas().add(new ColumnData("source", "facebook", 6));
		row.getColumnDatas().add(new ColumnData("referrer", "https://www.veroanalytics.com/", 7));
		row.getColumnDatas().add(new ColumnData("landing_url", "/ 500", 8));
		
		Command deleteCommand = new DeleteCommand();
		deleteCommand.setColumnName("referrer");
		Command upperCaseCommand = new UpperCaseCommand();
		upperCaseCommand.setColumnName("source");
		Command mergeCommand = new MergeCommand();
		mergeCommand.setColumnName("source");
		mergeCommand.getOpts().put("target_column_name", "clicks");
		mergeCommand.getOpts().put("delimiter", "---");
		
		deleteCommand.execute(row);		
		upperCaseCommand.execute(row);		
		mergeCommand.execute(row);
		
		row.getColumnDatas().forEach(c -> System.out.println(c.getName() + ":" + c.getValue()));
		assertEquals("FACEBOOK---1265", row.getColumnDatas().get(5).getValue());
	}
	
	@Test
	public void testSplitPositionCommand() {
		Command command = new SplitPositionCommand();
		command.setColumnName("col7");
		command.getOpts().put("position", "5");
		command.getOpts().put("retain_column", "on");
		
		command.execute(row);
		
		assertEquals(12, row.getColumnDatas().size());
		assertEquals("This ", row.getColumnDatas().get(8).getValue());
		assertEquals("is Column 7", row.getColumnDatas().get(9).getValue());
	}
	
	@Test
	public void testSplitPositionCommandLastColumn() {
		Command command = new SplitPositionCommand();
		command.setColumnName("col9");
		command.getOpts().put("position", "5");
		command.getOpts().put("retain_column", "off");
		
		command.execute(row);
		
		assertEquals(11, row.getColumnDatas().size());
		assertEquals("This ", row.getColumnDatas().get(9).getValue());
		assertEquals("is Column 9", row.getColumnDatas().get(10).getValue());
	}
	
	@Test
	public void testSplitPositionCommandFirstColumn() {
		Command command = new SplitPositionCommand();
		command.setColumnName("col0");
		command.getOpts().put("position", "5");
		command.getOpts().put("retain_column", "on");
		
		command.execute(row);
		
		assertEquals(12, row.getColumnDatas().size());
		assertEquals("This ", row.getColumnDatas().get(1).getValue());
		assertEquals("is Column 0", row.getColumnDatas().get(2).getValue());
	}
	
	@Test
	public void testSplitPositionCommandZeroPosition() {
		Command command = new SplitPositionCommand();
		command.setColumnName("col7");
		command.getOpts().put("position", "0");
		command.getOpts().put("retain_column", "on");
		
		command.execute(row);
		
		assertEquals(12, row.getColumnDatas().size());
		assertEquals("", row.getColumnDatas().get(8).getValue());
		assertEquals("This is Column 7", row.getColumnDatas().get(9).getValue());
	}
	
	@Test
	public void testSplitPositionCommandOutOfBoundPosition() {
		Command command = new SplitPositionCommand();
		command.setColumnName("col7");
		command.getOpts().put("position", "20");
		command.getOpts().put("retain_column", "on");
		
		command.execute(row);
		
		assertEquals(12, row.getColumnDatas().size());
		assertEquals("This is Column 7", row.getColumnDatas().get(8).getValue());
		assertEquals("", row.getColumnDatas().get(9).getValue());
	}
	
	@Test
	public void testCapitalizeCommand() {
		row.getColumnDatas().add(new ColumnData("col10", "hello world", 10));
		Command command = new CapitalizeCommand();
		command.setColumnName("col10");
		
		command.execute(row);
		
		assertEquals("Hello world", row.getColumnDatas().get(10).getValue());
	}
	
	@Test
	public void testExtractBetweenCommand() {
		Command command = new ExtractBetweenCommand();
		command.setColumnName("col2");
		command.getOpts().put("begin", " ");
		command.getOpts().put("end", " ");
		command.getOpts().put("occurrence", "first");
		
		command.execute(row);
		assertEquals(11, row.getColumnDatas().size());
		assertEquals("is", row.getColumnDatas().get(3).getValue());
	}
	
	@Test
	public void testExtractBetweenCommandLast() {
		Command command = new ExtractBetweenCommand();
		command.setColumnName("col2");
		command.getOpts().put("begin", " ");
		command.getOpts().put("end", " ");
		command.getOpts().put("occurrence", "last");
		
		command.execute(row);
		assertEquals(11, row.getColumnDatas().size());
		assertEquals("Column", row.getColumnDatas().get(3).getValue());
	}
	
	@Test
	public void testExtractBetweenCommandNoMatch() {
		Command command = new ExtractBetweenCommand();
		command.setColumnName("col2");
		command.getOpts().put("begin", ".");
		command.getOpts().put("end", "a");
		command.getOpts().put("occurrence", "first");
		
		command.execute(row);
		assertEquals(11, row.getColumnDatas().size());
		assertEquals("", row.getColumnDatas().get(3).getValue());
	}
	
	@Test
	public void testExtractBetweenCommandEmpty() {
		Command command = new ExtractBetweenCommand();
		command.setColumnName("col2");
		command.getOpts().put("begin", " ");
		command.getOpts().put("end", " ");
		command.getOpts().put("occurrence", "first");
		
		row.getColumnDatas().get(2).setValue("");
		
		command.execute(row);
		assertEquals(11, row.getColumnDatas().size());
		assertEquals("", row.getColumnDatas().get(3).getValue());
	}
	
	@Test
	public void testExtractPatternCommand() {
		Command command = new ExtractPatternCommand();
		command.setColumnName("col2");
		command.getOpts().put("pattern", "((is)|(ol))");
		command.getOpts().put("limit", "4");
		
		command.execute(row);
		assertEquals(13, row.getColumnDatas().size());
		assertEquals("is", row.getColumnDatas().get(3).getValue());
		assertEquals("is", row.getColumnDatas().get(4).getValue());
		assertEquals("", row.getColumnDatas().get(5).getValue());
	}
	
	@Test
	public void testExtractPatternCommand2() {
		Command command = new ExtractPatternCommand();
		command.setColumnName("id");
		command.getOpts().put("pattern", "\\[([0-9a-z\\-]+)\\]");
		command.getOpts().put("limit", "1");
		
		row.getColumnDatas().add(new ColumnData("id", "[03cd804f-1e2a-4d03-8029-ed29b8f45c31]", 0));
		
		command.execute(row);
		assertEquals("id_0", row.getColumnDatas().get(11).getName());
		assertEquals("03cd804f-1e2a-4d03-8029-ed29b8f45c31", row.getColumnDatas().get(11).getValue());
	}
	
	@Test
	public void testFindReplaceCommand() {
		Command command = new FindReplaceCommand();
		command.setColumnName("col2");
		command.getOpts().put("find", "Is");
		command.getOpts().put("replace", "Ab");
		command.getOpts().put("case_insensitive", "on");
		command.getOpts().put("regex", "off");
		command.getOpts().put("replace_all", "on");
		
		command.execute(row);
		
		assertEquals("ThAb Ab Column 2", row.getColumnDatas().get(2).getValue());
	}
	
	@Test
	public void testSplitDelimiterCommand() {
		Command command = new SplitDelimiterCommand();
		command.setColumnName("col2");
		command.getOpts().put("retain_column", "off");
		command.getOpts().put("delimiter", "is");
		command.getOpts().put("limit", "3");
		command.getOpts().put("case_insensitive", "on");
		command.getOpts().put("regex", "off");
		
		command.execute(row);
		row.getColumnDatas().forEach(c -> System.out.println(c.getName() + ":" + c.getValue()));
		assertEquals(12, row.getColumnDatas().size());
		assertEquals("Th", row.getColumnDatas().get(2).getValue());
	}
	
	@Test
	public void testFilterCommandEqualTo() {
		FilterCommand command = new FilterCommand();
		command.setColumnName("col2");
		command.getOpts().put("right", "This is Column 2");
		command.getOpts().put("operator", "=");
		command.getOpts().put("value_type", FilterValueType.VALUE.name());
		command.preprocessFilterValue();
		
		RowData transformedRow = command.execute(row).get(0);
		assertEquals(row, transformedRow);
	}
	
	@Test
	public void testFilterCommandInvalid() {
		FilterCommand command = new FilterCommand();
		command.setColumnName("col2");
		command.getOpts().put("right", "This is Column 2");
		command.getOpts().put("operator", ">");
		command.getOpts().put("value_type", FilterValueType.VALUE.name());
		command.preprocessFilterValue();
		
		assertFalse(command.isValid());
		
		RowData transformedRow = command.execute(row).get(0);
		assertEquals(row, transformedRow);
	}
	
	@Test
	public void testRemoveSquareBrackets() {
		row = new RowData();
		row.getColumnDatas().add(new ColumnData("hit_date", "[2015-07-01]", 0));
		Command command = new FindReplaceCommand();
		command.setColumnName("hit_date");
		command.getOpts().put("find", "[\\[\\]]");
		command.getOpts().put("replace", "");
		command.getOpts().put("case_insensitive", "off");
		command.getOpts().put("regex", "on");
		command.getOpts().put("replace_all", "on");
		
		command.execute(row);
		
		assertEquals("2015-07-01", row.getColumnDatas().get(0).getValue());
	}
	
	@Test
	public void testInsertFileNameCommand() {
		InsertFileNameCommand command = new InsertFileNameCommand();
		command.setColumnName("col9");
		
		row.setSourceFileName("This is column 10");
		command.execute(row);
		
		assertEquals(11, row.getColumnDatas().size());
		assertEquals(10, row.getColumnDatas().get(10).getPosition());
		assertEquals(CommonConstants.DEFAULT_FILE_NAME_COLUMN_NAME, row.getColumnDatas().get(10).getName());
	}
	
	@Test
	public void testDeleteRowsCommand() {
		List<RowData> allRows = new ArrayList<>();
		
		for (int i = 0; i < 100; i++) {
			allRows.add((RowData) row.clone());
		}
		
		assertEquals(100, allRows.size());
		
		DeleteRowsCommand command = new DeleteRowsCommand();
		command.setAllRows(allRows);
		command.getOpts().put("start", "10");
		command.getOpts().put("end", "50");
		
		List<RowData> transformedRows = command.execute(null);
		
		assertEquals(59, transformedRows.size());
	}
	
	@Test
	public void testMergeAllRowsCommand() {
		List<RowData> allRows = new ArrayList<>();
		
		for (int i = 0; i < 10; i++) {
			allRows.add((RowData) row.clone());
		}
		
		assertEquals(10, allRows.size());
		
		MergeAllRowsCommand command = new MergeAllRowsCommand();
		command.setAllRows(allRows);
		
		List<RowData> transformedRows = command.execute(null);
		
		System.out.println("value = " + transformedRows.get(0).getColumnDatas().get(0).getValue());
		
		assertEquals(1, transformedRows.size());
		assertEquals(1, transformedRows.get(0).getColumnDatas().size());
	}
	
	@Test
	public void testMergeAllRowsCommandJson() throws IOException {
		List<RowData> allRows = new ArrayList<>();
		
		try (BufferedReader in = new BufferedReader(new InputStreamReader(CommandTest.class.getResourceAsStream("twitter_search.json")))) {
			String line = in.readLine();
			String columnName = line.trim();
			
			while ((line = in.readLine()) != null) {
				RowData rowData = new RowData();
				rowData.getColumnDatas().add(new ColumnData(columnName, line, 0));
				allRows.add(rowData);
			}
			
			MergeAllRowsCommand command = new MergeAllRowsCommand();
			command.setAllRows(allRows);
			
			List<RowData> transformedRows = command.execute(null);
			
			System.out.println("value = " + transformedRows.get(0).getColumnDatas().get(0).getValue());
			
			JsonUtils.toJsonNode(transformedRows.get(0).getColumnDatas().get(0).getValue());
			
			assertEquals(1, transformedRows.size());
			assertEquals(1, transformedRows.get(0).getColumnDatas().size());
		}
	}
	
	@Test
	public void testExtractJsonCommand() throws FileNotFoundException, IOException {		
		try (BufferedReader in = new BufferedReader(new InputStreamReader(CommandTest.class.getResourceAsStream("ExtractJsonCommandTestInput.json")))) {
			String json = in.readLine();
			
			RowData rowData = new RowData();
			rowData.getColumnDatas().add(new ColumnData("my_json", json, 0));
			
			ExtractJsonCommand command = new ExtractJsonCommand();
			
			command.setColumnName("my_json");
			command.getOpts().put("path", "$.labs[*].name, $.labs[*].location, $.medications[*].antianginal[*].strength");
			command.getOpts().put("retain_column", "on");
			command.getOpts().put("expand_array", "on");
			
			List<RowData> transformedRows = command.execute(rowData);
			
			transformedRows.forEach(r -> r.getColumnDatas().forEach(c -> System.out.println("Name = " + c.getName() + " value = " + c.getValue())));
			
			assertEquals(12, transformedRows.size());
			
			for (RowData r : transformedRows) {
				assertEquals(4, r.getColumnDatas().size());
			}
		}
	}
}

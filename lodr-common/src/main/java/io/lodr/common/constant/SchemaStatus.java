package io.lodr.common.constant;

public enum SchemaStatus {
	SCHEMA_FETCHING, SCHEMA_READY, SCHEMA_FAILED
}

package io.lodr.common.constant;

public enum RedshiftDistStyle {
	EVEN, KEY, ALL
}

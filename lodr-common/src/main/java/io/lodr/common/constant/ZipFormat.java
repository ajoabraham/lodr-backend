package io.lodr.common.constant;

public enum ZipFormat {
	ZIP(".zip"), GZ(".gz"), GZIP(".gzip"), BZ2(".bz2"), BZIP2(".bzip2");
	
	private String fileExtension = null;
	
	private ZipFormat(String fileExtension) {
		this.fileExtension = fileExtension;
	}
	
	public String getFileExtension() {
		return fileExtension;
	}
}

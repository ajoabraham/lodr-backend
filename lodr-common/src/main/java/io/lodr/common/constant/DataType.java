package io.lodr.common.constant;

import java.sql.Types;

public enum DataType {
	INTEGER,
	BIGINT,
	DECIMAL,
	BOOLEAN,
	VARCHAR,
	DATE,
	TIMESTAMP;
	
	public String getSqlTypeName() {
		String sqlTypeName = "varchar";
		switch (this) {
		case INTEGER:
			sqlTypeName = "integer";
			break;
		case BIGINT:
			sqlTypeName = "bigint";
			break;
		case DECIMAL:
			sqlTypeName = "decimal";
			break;
		case BOOLEAN:
			sqlTypeName = "boolean";
			break;
		case DATE:
			sqlTypeName = "date";
			break;
		case TIMESTAMP:
			sqlTypeName = "timestamp";
			break;
		default:
			// do nothing
		}
		
		return sqlTypeName;
	}
	
	public int getSqlType() {
		int sqlType = Types.VARCHAR;
		
		switch (this) {
		case INTEGER:
			sqlType = Types.INTEGER;
			break;
		case BIGINT:
			sqlType = Types.BIGINT;
			break;
		case DECIMAL:
			sqlType = Types.DECIMAL;
			break;
		case BOOLEAN:
			sqlType = Types.BOOLEAN;
			break;
		case DATE:
			sqlType = Types.DATE;
			break;
		case TIMESTAMP:
			sqlType = Types.TIMESTAMP;
			break;
		default:
			// do nothing
		}
		
		return sqlType;
	}
	
	public static DataType toDataType(int sqlType) {
		DataType dataType = DataType.VARCHAR;
		
		switch (sqlType) {
		case Types.SMALLINT:
		case Types.INTEGER:
			dataType = DataType.INTEGER;
			break;
		case Types.BIGINT:
			dataType = DataType.BIGINT;
			break;
		case Types.DECIMAL:
		case Types.REAL:
		case Types.DOUBLE:
		case Types.NUMERIC:
			dataType = DataType.DECIMAL;
			break;
		case Types.BOOLEAN:
		case Types.BIT:
			dataType = DataType.BOOLEAN;
			break;
		case Types.CHAR:
		case Types.VARCHAR:
			dataType = DataType.VARCHAR;
			break;
		case Types.DATE:
			dataType = DataType.DATE;
			break;
		case Types.TIMESTAMP:
		case Types.TIMESTAMP_WITH_TIMEZONE:
			dataType = DataType.TIMESTAMP;
			break;
		default:
			// do nothing
		}
		
		return dataType;
	}
}

package io.lodr.common.constant;

public enum ConfigParam {
	PARAM_APP_MODE("appMode", AppMode.TEST.getLambdaAlias()),
	PARAM_ACCESS_ID("accessId", null),
	PARAM_ACCESS_KEY("accessKey", null),
	PARAM_DEFAULT_REGION("defaultRegion", "us-west-2"),
	PARAM_UPLOAD_BUCKET("uploadBucket", null),
	PARAM_JOB_STATUS_TBL("jobStatusTbl", "JobStatus"),
	PARAM_COLUMN_INFO_TBL("columnInfoTbl", "ColumnInfo"),
	PARAM_CANCELED_JOB_TBL("canceledJobTbl", "CanceledJob"),
	PARAM_CANCELED_QUERY_TBL("canceledQueryTbl", "CanceledQuery"),
	PARAM_PROCESS_DATA_FUNCTION("processDataFunction", "ProcessDataFunction"),
	PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION("processDataInMemoryFunction", "ProcessDataInMemoryFunction"),
	PARAM_SAMPLE_SOURCE_FUNCTION("sampleSourceFunction", "SampleSourceFunction"),
	PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION("sampleWranglerSourceFunction", "SampleWranglerSourceFunction"),
	PARAM_LAMBDA_READ_BATCH_SIZE("lambdReadbatchSize", "200");
	
	private String name = null;
	private String defaultValue = null;
	
	private ConfigParam(String name, String defaultValue) {
		this.name = name;
		this.defaultValue = defaultValue;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
}

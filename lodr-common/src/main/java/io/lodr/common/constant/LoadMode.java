package io.lodr.common.constant;

public enum LoadMode {
	REPLACE, APPEND, MERGE;
}

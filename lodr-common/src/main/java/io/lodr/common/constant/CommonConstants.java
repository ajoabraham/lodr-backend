package io.lodr.common.constant;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static io.lodr.common.constant.ConfigParam.*;

/**
 * @author Tai Hu
 * 
 * All configurable parameters (could be either -D option, environment variable, or in Play server's application.conf file)
 * 
 * appMode
 * accessId
 * accessKey
 * defaultRegion
 * uploadBucket
 * jobStatusTbl
 * columnInfoTbl
 * canceledJobTbl
 * canceledQueryTbl
 * processDataFunction
 * processDataInMemoryFunction
 * sampleSourceFunction
 * sampleWranglerSourceFunction
 * 
 */
public interface CommonConstants {	
	public static final AppMode APP_MODE = AppMode.toAppMode(System.getProperty(PARAM_APP_MODE.getName(), PARAM_APP_MODE.getDefaultValue()));
	
	public static final String DEFAULT_ACCESS_ID = System.getProperty(PARAM_ACCESS_ID.getName(), System.getenv(PARAM_ACCESS_ID.getName()));
	public static final String DEFAULT_ACCESS_KEY = System.getProperty(PARAM_ACCESS_KEY.getName(), System.getenv(PARAM_ACCESS_KEY.getName()));
	public static final String DEFAULT_REGION = System.getenv(PARAM_DEFAULT_REGION.getName()) == null 
			? System.getProperty(PARAM_DEFAULT_REGION.getName(), PARAM_DEFAULT_REGION.getDefaultValue()) : System.getenv(PARAM_DEFAULT_REGION.getName());
	public static final String DEFAULT_S3_DELIMITER = "/";
	
	public static final String JOB_STATUS_TABLE = System.getenv(PARAM_JOB_STATUS_TBL.getName()) == null 
			? System.getProperty(PARAM_JOB_STATUS_TBL.getName(), PARAM_JOB_STATUS_TBL.getDefaultValue()) : System.getenv(PARAM_JOB_STATUS_TBL.getName());
	public static final String COLUMN_INFO_TABLE = System.getenv(PARAM_COLUMN_INFO_TBL.getName()) == null 
			? System.getProperty(PARAM_COLUMN_INFO_TBL.getName(), PARAM_COLUMN_INFO_TBL.getDefaultValue()) : System.getenv(PARAM_COLUMN_INFO_TBL.getName());
	public static final String CANCELED_JOB_TABLE = System.getenv(PARAM_CANCELED_JOB_TBL.getName()) == null 
			? System.getProperty(PARAM_CANCELED_JOB_TBL.getName(), PARAM_CANCELED_JOB_TBL.getDefaultValue()) : System.getenv(PARAM_CANCELED_JOB_TBL.getName());
	public static final String CANCELED_QUERY_TABLE = System.getenv(PARAM_CANCELED_QUERY_TBL.getName()) == null 
			? System.getProperty(PARAM_CANCELED_QUERY_TBL.getName(), PARAM_CANCELED_QUERY_TBL.getDefaultValue()) : System.getenv(PARAM_CANCELED_QUERY_TBL.getName());
	
	public static final String PROCESS_DATA_FUNCTION = System.getenv(PARAM_PROCESS_DATA_FUNCTION.getName()) == null 
			? System.getProperty(PARAM_PROCESS_DATA_FUNCTION.getName(), PARAM_PROCESS_DATA_FUNCTION.getDefaultValue()) : System.getenv(PARAM_PROCESS_DATA_FUNCTION.getName());
	public static final String PROCESS_DATA_IN_MEMORY_FUNCTION = System.getenv(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName()) == null 
			? System.getProperty(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName(), PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getDefaultValue()) : System.getenv(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName());
	public static final String SAMPLE_SOURCE_FUNCTION = System.getenv(PARAM_SAMPLE_SOURCE_FUNCTION.getName()) == null 
			? System.getProperty(PARAM_SAMPLE_SOURCE_FUNCTION.getName(), PARAM_SAMPLE_SOURCE_FUNCTION.getDefaultValue()) : System.getenv(PARAM_SAMPLE_SOURCE_FUNCTION.getName());
	public static final String SAMPLE_WRANGLER_SOURCE_FUNCTION = System.getenv(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName()) == null 
			? System.getProperty(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName(), PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getDefaultValue()) : System.getenv(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName());
	
	public static int DEFAULT_THREAD_POOL_SIZE = 10;
	// If file size bigger than 10MB use multipart upload
	public static long MULTIPART_UPLOAD_THRESHOLD = 10000000;
	
	// File path on S3 <bucket name>/<user id>/uploads
	// <bucket name>/<user id>/splits
	public static final String DEFAULT_UPLOADS_FOLDER = "uploads";
	public static final String DEFAULT_SPLITS_FOLDER = "splits";
	
	// Get uploadBucket parameter from system or command line. If it is not defined, use hard coded bucket names
	public static final String UPLOAD_BUCKET = System.getenv(PARAM_UPLOAD_BUCKET.getName()) == null ? System.getProperty(PARAM_UPLOAD_BUCKET.getName()) : System.getenv(PARAM_UPLOAD_BUCKET.getName());
	// Buckets in our S3 used for user uploaded files (use this list to determine if given file
	// is a user uploaded file or a direct load from user's S3 bucket
	public static final List<String> DEFAULT_BUCKET_NAMES = UPLOAD_BUCKET == null ? 
			Arrays.asList("lodr-user-uploads", "lodr-user-uploads-prod", "lodr-user-uploads-staging")
			: Arrays.asList(UPLOAD_BUCKET, UPLOAD_BUCKET, UPLOAD_BUCKET);
	public static final String DEFAULT_BUCKET_NAME = APP_MODE == AppMode.PROD ? DEFAULT_BUCKET_NAMES.get(1) : DEFAULT_BUCKET_NAMES.get(0);
	
	public static final String LOCAL_TEMP_DIR = "/tmp";
	public static final int DEFAULT_BUFFER_SIZE = 8 * 1024;
	public static final int DEFAULT_BATCH_SIZE = Integer.parseInt((System.getenv(PARAM_LAMBDA_READ_BATCH_SIZE.getName()) == null 
			? System.getProperty(PARAM_LAMBDA_READ_BATCH_SIZE.getName(), PARAM_LAMBDA_READ_BATCH_SIZE.getDefaultValue()) : System.getenv(PARAM_LAMBDA_READ_BATCH_SIZE.getName()))) * 1024 * 1024;
	
	// maximum wait time for run job function
	public static final int DEFAULT_MAX_WAIT_TIME = 5 * 60 * 1000;
	// ProcessDataFunction should post back for every 10000 rows processed.
	public static final int DEFAULT_POST_BACK_INTERVAL = 10000;
	
	public static final String DEFAULT_DYNAMODB_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	
	public static final String DEFAULT_CSV_DELIMITER = "|";
	public static final String DEFAULT_CSV_QUOTE = "\"";
	
	public static final long MAX_INT = 2147483647;
	public static final long MIN_INT = -2147483648;
	public static final int MAX_VARCHAR = 65535;
	
	// Character encoding used for all files generated during the process.
	public static final String DEFAULT_ENCODING_NAME = StandardCharsets.UTF_8.name();
	public static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;
	
	// ISO 8601 date format (recommended by PostgreSQL)
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	// 	ISO 8601 timestamp format (recommended by PostgreSQL)
	public static final String DEFAULT_TIMESTAMP_FORMAT = DEFAULT_DATE_FORMAT + " HH:mm:ss";
	// Date format used by fact data URL
	public static final String FACT_URL_DATE_FORMAT = "MM/dd/yyyy";
	
	public static final String[] DATE_FORMATS = {"MM/dd/yy", "dd-MMM-yy", "yyyy-MM-dd", 
			"MM/dd/yyyy", "MM-dd-yyyy", "yyyy/MM/dd"};
	public static final String[] TIMESTAMP_FORMATS = {"yyyy-MM-dd HH:mm:ss",
			"MM/dd/yyyy HH:mm:ss", "MM-dd-yyyy HH:mm:ss", "yyyy/MM/dd HH:mm:ss",
			"MM/dd/yyyy h:mm:ss a", "MM/dd/yyyy H:mm", "yyyy-MM-dd'T'HH:mm:ss.SSS",
			"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm:ss.SSS"};
		
	// If VARCHAR length is less than DEFAULT_VARCHAR_LENGTH_THRESHOLD, just return DEFAULT_VARCHAR_LENGTH
	public static final int DEFAULT_VARCHAR_LENGTH = 256;
	// If VARCHAR length is greater than DEFAULT_VARCHAR_LENGTH_THRESHOLD, add VARCHAR_LEGNTH_FACTOR% to the length
	public static final double VARCHAR_LENGTH_FACTOR = 0.2;
	public static final int DEFAULT_VARCHAR_LENGTH_THRESHOLD = DEFAULT_VARCHAR_LENGTH - (int)(DEFAULT_VARCHAR_LENGTH * VARCHAR_LENGTH_FACTOR);
	
	public static final String DEFAULT_FILE_NAME_COLUMN_NAME = "source_file_name";
}

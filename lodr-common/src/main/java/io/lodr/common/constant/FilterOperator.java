package io.lodr.common.constant;

public enum FilterOperator {
	LESS_THAN("<"), GREATER_THAN(">"), 
	LESS_THAN_OR_EQUAL_TO("<="), 
	GREATER_THAN_OR_EQUAL_TO(">="), 
	EQUAL_TO("=");
	
	private String symbol = null;
	
	FilterOperator(String symbol) {
		this.symbol = symbol;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
	public static FilterOperator fromSymbol(String symbol) {
		for (FilterOperator op : FilterOperator.values()) {
			if (op.getSymbol().equals(symbol)) {
				return op;
			}
		}
		
		return null;
	}
}

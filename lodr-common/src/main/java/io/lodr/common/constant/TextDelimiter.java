package io.lodr.common.constant;

public enum TextDelimiter {
	COMMA, SEMICOLON, PIPE, SPACE, TAB, OTHER;
	
	public String getDisplayName() {
		String name = "";
		switch (this) {
		case COMMA:
			name = "Comma (,)";
			break;
		case SEMICOLON:
			name = "Semicolon (;)";
			break;
		case PIPE:
			name = "Pipe (|)";
			break;
		case SPACE:
			name = "Whitespace";
			break;
		case TAB:
			name = "Tab";
			break;
		case OTHER:
			name = "Other";
			break;
		default:
			name = value();
		}
		
		return name;
	}
	
	public String value() {
		String value = null;
		switch (this) {
		case COMMA:
			value = ",";
			break;
		case SEMICOLON:
			value = ";";
			break;
		case PIPE:
			value = "|";
			break;
		case SPACE:
			value = " ";
			break;
		case TAB:
			value = "\t";
			break;
		default:
			value = ",";
		}
		
		return value;
	}
	
	public char charValue() {
		return value().charAt(0);
	}
	
	public static TextDelimiter forName(String value) {
		TextDelimiter d = OTHER;
		
		if (value != null) {
			if (COMMA.value().equals(value)) {
				d = COMMA;
			}
			else if (SEMICOLON.value().equals(value)) {
				d = SEMICOLON;
			}
			else if (PIPE.value().equals(value)) {
				d = PIPE;
			}
			else if (SPACE.value().equals(value)) {
				d = SPACE;
			}
			else if (TAB.value().equals(value)) {
				d = TAB;
			}
 		}
		
		return d;
	}
}

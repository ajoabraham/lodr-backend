package io.lodr.common.constant;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public interface CSVDefaultSettings {
	public static final String DELIMITER = TextDelimiter.COMMA.value();
	public static final char DELIMITER_CHAR = DELIMITER.charAt(0);
	public static final String QUOTE = "\"";
	public static final char QUOTE_CHAR = QUOTE.charAt(0);
	public static final String RECORD_SEPARATOR = "\r\n";
	public static final String COMMENT_MARKER = "#";
	public static final char COMMENT_MARKER_CHAR = COMMENT_MARKER.charAt(0);
	public static final Charset ENCODING = StandardCharsets.UTF_8;
	public static final boolean IGNORE_EMPTY_LINES = true;
	public static final boolean ALLOW_MISSING_COLUMN_NAMES = false;
	
	// When delimiter is '\0', change it to this string. UI's database
	// cannot handle '\0'
	public static final String NO_DELIMITER = "no_delimiter";
}

package io.lodr.common.constant;

public enum QueryState {
	RUNNING, COMPLETED, FAILED
}

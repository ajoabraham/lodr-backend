package io.lodr.common.constant;

public enum AppMode {
	DEV, TEST, PROD;
	
	public String getLambdaAlias() {
		return name().toLowerCase();
	}
	
	public static AppMode toAppMode(String name) {
		if (name.equalsIgnoreCase(AppMode.DEV.getLambdaAlias())) {
			return AppMode.DEV;
		}
		
		if (name.equalsIgnoreCase(AppMode.PROD.getLambdaAlias())) {
			return AppMode.PROD;
		}
		
		return AppMode.TEST;
	}
}

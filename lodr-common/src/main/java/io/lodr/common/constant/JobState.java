package io.lodr.common.constant;

public enum JobState {
	QUEUED, RUNNING, SUCCEEDED, FAILED, TIMEDOUT
}

package io.lodr.common.constant;

public enum UnzipState {
	UNZIPPING, SUCCEEDED, FAILED
}

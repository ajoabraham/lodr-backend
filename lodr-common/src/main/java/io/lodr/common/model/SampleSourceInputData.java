package io.lodr.common.model;

public class SampleSourceInputData extends InputDataBase {
	private static final long serialVersionUID = 1L;

	private boolean includesHeader = true;
	
	public SampleSourceInputData() {		
	}

	public boolean getIncludesHeader() {
		return includesHeader;
	}

	public void setIncludesHeader(boolean includesHeader) {
		this.includesHeader = includesHeader;
	}	
}

package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.lodr.common.constant.SchemaStatus;

public class PostBackDatabaseSchema {
	@JsonProperty("schema_error")
	private String schemaError = null;
	@JsonProperty("schema_status")
	private SchemaStatus schemaStatus = SchemaStatus.SCHEMA_FETCHING;
	@JsonProperty("tables_attributes")
	private List<SchemaTable> tablesAttributes = new ArrayList<>();
	
	public PostBackDatabaseSchema() {
	}

	public String getSchemaError() {
		return schemaError;
	}

	public void setSchemaError(String schemaError) {
		this.schemaError = schemaError;
	}

	public SchemaStatus getSchemaStatus() {
		return schemaStatus;
	}

	public void setSchemaStatus(SchemaStatus schemaStatus) {
		this.schemaStatus = schemaStatus;
	}

	public List<SchemaTable> getTablesAttributes() {
		return tablesAttributes;
	}

	public void setTablesAttributes(List<SchemaTable> tablesAttributes) {
		this.tablesAttributes = tablesAttributes;
	}
}
package io.lodr.common.model;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

//@DynamoDBTable(tableName = "CanceledQuery")
public class CanceledQuery {
	private String queryId = null;
	private Date canceledOn = new Date();
	
	public CanceledQuery() {		
	}
	
	public CanceledQuery(String queryId) {
		this.queryId = queryId;
	}

	@DynamoDBHashKey
	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	@DynamoDBAttribute
	public Date getCanceledOn() {
		return canceledOn;
	}

	public void setCanceledOn(Date canceledOn) {
		this.canceledOn = canceledOn;
	}
}
package io.lodr.common.model;

import java.io.Serializable;

import io.lodr.common.util.CommonUtils;

public final class ColumnData implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	private String name = null;
	private String value = null;
	private int position = 0;
	
	public ColumnData() {
	}
	
	public ColumnData(String name, String value, int position) {
		setName(name);
		setValue(value);
		setPosition(position);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = CommonUtils.isBlank(value) ? "" : value;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
	public Object clone() {
		return new ColumnData(name, value, position);
	}
}

package io.lodr.common.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.util.CommonUtils;

public class FileInfo extends InputDataBase {
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "encoding cannot be empty")
	private String encoding = null;
	@NotNull(message = "delimiter cannot be empty")
	private String delimiter = null;
	@NotNull(message = "quote cannot be empty")
	private String quote = null;
	private boolean includesHeader = true;
	private int rowCount = 0;
	
	public FileInfo() {
	}

	public String getEncoding() {
		return CommonUtils.isBlank(encoding)
				? CommonConstants.DEFAULT_ENCODING_NAME : encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getDelimiter() {
		return delimiter == null ? CommonConstants.DEFAULT_CSV_DELIMITER
				: delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public String getQuote() {
		return CommonUtils.isBlank(quote) ? CommonConstants.DEFAULT_CSV_QUOTE
				: quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}
	
	public boolean getIncludesHeader() {
		return includesHeader;
	}

	public void setIncludesHeader(boolean includesHeader) {
		this.includesHeader = includesHeader;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	@Override
	public Object clone() {
		FileInfo fileInfo = (FileInfo) super.clone();
		fileInfo.setEncoding(encoding);
		fileInfo.setDelimiter(delimiter);
		fileInfo.setQuote(quote);
		fileInfo.setIncludesHeader(includesHeader);
		fileInfo.setRowCount(rowCount);

		return fileInfo;
	}	
}

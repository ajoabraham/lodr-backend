package io.lodr.common.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

//@DynamoDBTable(tableName = "ColumnInfo")
public class ColumnInfoItem {
	private String jobId = null;
	private int batchId = 0;
	private List<ColumnInfo> columnInfos = new ArrayList<>();
	private Date createdOn = new Date();

	public ColumnInfoItem() {
	}

	@DynamoDBHashKey
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	@DynamoDBRangeKey
	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	@DynamoDBAttribute
	public List<ColumnInfo> getColumnInfos() {
		return columnInfos;
	}

	public void setColumnInfos(List<ColumnInfo> columnInfos) {
		this.columnInfos = columnInfos;
	}

	@DynamoDBAttribute
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}

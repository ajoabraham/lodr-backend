package io.lodr.common.model.command;

import java.util.List;

import io.lodr.common.model.RowData;

public class DeleteRowsCommand extends Command {
	private List<RowData> allRows = null;
	
	public DeleteRowsCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final int start = getStart();
		final int end = getEnd();
		
		if (start < 1 || end < 1 || end < start) return allRows;
		
		if (start > allRows.size()) return allRows;
		
		allRows.subList(start - 1, end > allRows.size() ? allRows.size() : end).clear();
		
		return allRows;
	}
	
	
	public void setAllRows(List<RowData> allRows) {
		this.allRows = allRows;
	}
	
	int getStart() {
		return Integer.parseInt(getOpt("start").toString());
	}
	
	int getEnd() {
		return Integer.parseInt(getOpt("end").toString());
	}
}

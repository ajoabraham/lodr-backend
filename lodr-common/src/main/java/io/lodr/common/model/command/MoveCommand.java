package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;

public class MoveCommand extends Command {
	
	public MoveCommand() {
		
	}
	
	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		final int position = getPosition();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny().ifPresent(c -> {
			ColumnData movedColumn = new ColumnData(c.getName(), c.getValue(), position);
			row.getColumnDatas().add(position, movedColumn);
			row.getColumnDatas().remove(c);
			
			resetColumnPositions(row);
		});
		
		return Lists.newArrayList(row);
	}

	int getPosition() {
		return Integer.parseInt(getOpt("position"));
	}
}

package io.lodr.common.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTyped;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.JobState;
import io.lodr.common.util.JobStatusDbService;

//@DynamoDBTable(tableName = "JobStatus")
public class JobStatus {
	private String jobId = null;
	private int batchId = 0;
	private JobState jobState = JobState.QUEUED;
	private Date startTime = new Date();
	private Date endTime = null;
	private long inputRows = 0;
	private long loadedRows = 0;
	private long failedRows = 0;
	private long filteredRows = 0;
	private long inputBytes = 0;
	private String outputFileObjectKey = null;
	private ProcessDataInputData inputData = null;
	private List<ColumnInfo> columnInfos = new ArrayList<>();
	private List<FieldError> fieldErrors = new ArrayList<>();
	private String error = null;
	private String errorDetails = null;
	private int attemptCount = 1;
	
	private int batchSize = CommonConstants.DEFAULT_BATCH_SIZE;
	private long startPosition = 0;

	public JobStatus() {
	}
	
	@DynamoDBAttribute
	public int getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}

	@DynamoDBAttribute
	public long getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(long startPosition) {
		this.startPosition = startPosition;
	}

	@DynamoDBHashKey
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	@DynamoDBRangeKey
	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	@DynamoDBAttribute
	@DynamoDBTyped(DynamoDBAttributeType.S)
	public JobState getJobState() {
		return jobState;
	}

	public void setJobState(JobState jobState) {
		this.jobState = jobState;
	}

	@DynamoDBAttribute
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@DynamoDBAttribute
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@DynamoDBAttribute
	public long getInputRows() {
		return inputRows;
	}

	public void setInputRows(long inputRows) {
		this.inputRows = inputRows;
	}
	
	@DynamoDBAttribute
	public long getLoadedRows() {
		return loadedRows;
	}

	public void setLoadedRows(long loadedRows) {
		this.loadedRows = loadedRows;
	}

	@DynamoDBAttribute
	public long getFailedRows() {
		return failedRows;
	}

	public void setFailedRows(long failedRows) {
		this.failedRows = failedRows;
	}

	@DynamoDBAttribute
	public long getFilteredRows() {
		return filteredRows;
	}

	public void setFilteredRows(long filteredRows) {
		this.filteredRows = filteredRows;
	}

	@DynamoDBAttribute
	public long getInputBytes() {
		return inputBytes;
	}

	public void setInputBytes(long inputBytes) {
		this.inputBytes = inputBytes;
	}

	@DynamoDBAttribute
	public String getOutputFileObjectKey() {
		return outputFileObjectKey;
	}

	public void setOutputFileObjectKey(String outputFileObjectKey) {
		this.outputFileObjectKey = outputFileObjectKey;
	}

	@DynamoDBIgnore
	public List<ColumnInfo> getColumnInfos() {
		if (columnInfos == null || columnInfos.isEmpty()) {
			try {
				columnInfos = JobStatusDbService.findColumnInfos(jobId, batchId);
			}
			catch (Exception e) {
				if (columnInfos == null) columnInfos = new ArrayList<>();
			}
		}
		
		return columnInfos;
	}

	public void setColumnInfos(List<ColumnInfo> columnInfos) {
		this.columnInfos = columnInfos;
	}

	@DynamoDBAttribute
	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	@DynamoDBIgnore
	public ProcessDataInputData getInputData() {
		return inputData;
	}

	public void setInputData(ProcessDataInputData inputData) {
		this.inputData = inputData;
	}

	@DynamoDBAttribute
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@DynamoDBAttribute
	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}
	
	@DynamoDBAttribute
	public int getAttemptCount() {
		return attemptCount;
	}
	
	public void setAttemptCount(int attemptCount){
		this.attemptCount = attemptCount;
	}
	
	@DynamoDBIgnore
	public boolean isTimedOut(){
		return jobState == JobState.TIMEDOUT;
	}
	
	public JobStatus retry(ProcessDataInputData inputData, int batchId, int batchSize, long startPosition){
		JobStatus js = new JobStatus();
		js.setBatchSize(batchSize);
		js.setStartPosition(startPosition);
		js.setBatchId(batchId);		
		js.setAttemptCount(attemptCount+1);
		js.setColumnInfos(getColumnInfos());
		js.setInputData(inputData);
		js.getInputData().setBatchId(batchId);
		js.setJobId(getJobId());
		js.setJobState(JobState.QUEUED);
		js.setOutputFileObjectKey(getOutputFileObjectKey());
		
		return js;
	}
}

package io.lodr.common.model.command;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.Lists;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class ExtractBetweenCommand extends Command {

	public ExtractBetweenCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		final String begin = getBegin();
		final String end = getEnd();
		final boolean isFirst = isFirst();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny()
			.ifPresent(c -> {
				final int index = row.getColumnDatas().indexOf(c);
				final ColumnData newColumn = new ColumnData(
						CommonUtils.getUniqueColumnName(getColumnNames(row), columnName), "", 0);
				
				if (!CommonUtils.isBlank(c.getValue())) {
    				String regex = String.format("(%s)(.*?)(%s)", Pattern.quote(begin), Pattern.quote(end));
    				Matcher matcher = Pattern.compile(regex).matcher(c.getValue());
    				MatchResult matchResult = null;
    				
    				final int maxStartIndex = c.getValue().length() - 1;
    				int startIndex = 0;
    				while (matcher.find(startIndex++)) {
    					matchResult = matcher.toMatchResult();
    					
    					if (isFirst || startIndex >= maxStartIndex) break;
    				}
    				
    				if (matchResult != null) {
    					newColumn.setValue(matchResult.group(2));
    				}
				}
			
				row.getColumnDatas().add(index + 1, newColumn);
				resetColumnPositions(row);
			});		

		return Lists.newArrayList(row);
	}

	String getBegin() {
		return getOpt("begin");
	}
	
	String getEnd() {
		return getOpt("end");
	}
	
	boolean isFirst() {
		String occurrence = getOpt("occurrence");
		
		return occurrence == null ? true : "first".equalsIgnoreCase(occurrence);
	}
}

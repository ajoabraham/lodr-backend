package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SampleWranglerSourceOutputData extends OutputDataBase {
	private static final long serialVersionUID = 1L;

	private boolean isEstimatedRowCount = false;
	private long totalRowCount = 0;
	private List<ColumnInfo> columnInfos = new ArrayList<>();
	private List<List<String>> previewData = new ArrayList<>();
	
	public SampleWranglerSourceOutputData() {
		
	}

	@JsonIgnore
	public boolean isEstimatedRowCount() {
		return isEstimatedRowCount;
	}

	public void setEstimatedRowCount(boolean isEstimatedRowCount) {
		this.isEstimatedRowCount = isEstimatedRowCount;
	}

	public long getTotalRowCount() {
		return totalRowCount;
	}

	public void setTotalRowCount(long totalRowCount) {
		this.totalRowCount = totalRowCount;
	}

	public List<ColumnInfo> getColumnInfos() {
		return columnInfos;
	}

	public void setColumnInfos(List<ColumnInfo> columnInfos) {
		this.columnInfos = columnInfos;
	}

	public List<List<String>> getPreviewData() {
		return previewData;
	}

	public void setPreviewData(List<List<String>> previewData) {
		this.previewData = previewData;
	}
}

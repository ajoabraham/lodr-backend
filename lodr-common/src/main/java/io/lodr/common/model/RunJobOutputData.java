package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import io.lodr.common.constant.JobState;

public class RunJobOutputData extends OutputDataBase {
	private static final long serialVersionUID = 1L;
	
	private String jobId = null;
	private long totalRowCount = 0;
	private JobState jobState = JobState.QUEUED;
	private List<ColumnInfo> columnInfos = new ArrayList<>();
	
	public RunJobOutputData() {
		
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public List<ColumnInfo> getColumnInfos() {
		return columnInfos;
	}

	public void setColumnInfos(List<ColumnInfo> columnInfos) {
		this.columnInfos = columnInfos;
	}

	public long getTotalRowCount() {
		return totalRowCount;
	}

	public void setTotalRowCount(long totalRowCount) {
		this.totalRowCount = totalRowCount;
	}

	public JobState getJobStatus() {
		return jobState;
	}

	public void setJobStatus(JobState jobState) {
		this.jobState = jobState;
	}
}

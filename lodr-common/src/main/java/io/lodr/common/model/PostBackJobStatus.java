package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.lodr.common.constant.JobState;

@JsonInclude(Include.NON_NULL)
public class PostBackJobStatus {
	@JsonProperty("job_id")
	private String jobId = null;
	@JsonProperty("info")
	private String info = null;
	@JsonProperty("status")
	private JobState status = JobState.QUEUED;
	@JsonProperty("total_rows_loaded")
	private long totalRowsLoaded = 0;
	@JsonProperty("input_rows")
	private long inputRows = 0;
	@JsonProperty("updated_rows")
	private long updatedRows = 0;
	@JsonProperty("inserted_rows")
	private long insertedRows = 0;
	@JsonProperty("filtered_rows")
	private long filteredRows = 0;
	@JsonProperty("execution_errors")
	private String executionErrors = null;
	@JsonProperty("total_failed_rows")
	private long totalFailedRows = 0;
	@JsonProperty("finished_at")
	private String finishedAt = null;
	@JsonProperty("lambda_done_at")
	private String lambdaDoneAt = null;
	@JsonProperty("input_bytes")
	private long inputBytes = 0;
	@JsonProperty("field_errors")
	private List<FieldError> fieldErrors = new ArrayList<>();
	@JsonProperty("selected_file")
	private String selectedFile = null;
	
	public PostBackJobStatus() {
		
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public JobState getStatus() {
		return status;
	}

	public void setStatus(JobState status) {
		this.status = status;
	}

	public long getTotalRowsLoaded() {
		return totalRowsLoaded;
	}

	public void setTotalRowsLoaded(long totalRowsLoaded) {
		this.totalRowsLoaded = totalRowsLoaded;
	}

	public long getInputRows() {
		return inputRows;
	}

	public void setInputRows(long inputRows) {
		this.inputRows = inputRows;
	}

	public long getUpdatedRows() {
		return updatedRows;
	}

	public void setUpdatedRows(long updatedRows) {
		this.updatedRows = updatedRows;
	}

	public long getInsertedRows() {
		return insertedRows;
	}

	public void setInsertedRows(long insertedRows) {
		this.insertedRows = insertedRows;
	}

	public long getFilteredRows() {
		return filteredRows;
	}

	public void setFilteredRows(long filteredRows) {
		this.filteredRows = filteredRows;
	}

	public String getExecutionErrors() {
		return executionErrors;
	}

	public void setExecutionErrors(String executionErrors) {
		this.executionErrors = executionErrors;
	}

	public long getTotalFailedRows() {
		return totalFailedRows;
	}

	public void setTotalFailedRows(long totalFailedRows) {
		this.totalFailedRows = totalFailedRows;
	}

	public String getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(String finishedAt) {
		this.finishedAt = finishedAt;
	}

	public String getLambdaDoneAt() {
		return lambdaDoneAt;
	}

	public void setLambdaDoneAt(String lambdaDoneAt) {
		this.lambdaDoneAt = lambdaDoneAt;
	}

	public long getInputBytes() {
		return inputBytes;
	}

	public void setInputBytes(long inputBytes) {
		this.inputBytes = inputBytes;
	}

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public String getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(String selectedFile) {
		this.selectedFile = selectedFile;
	}
}

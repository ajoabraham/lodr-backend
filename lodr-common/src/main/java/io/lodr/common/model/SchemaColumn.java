package io.lodr.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SchemaColumn {
	@JsonProperty("name")
	private String name = null;
	@JsonProperty("data_type")
	private String dataType = null;
	@JsonProperty("compression")
	private String compressionType = null;
	@JsonProperty("precision")
	private int precision = 0;
	@JsonProperty("scale")
	private int scale = 0;
	@JsonProperty("length")
	private int length = 0;
	@JsonProperty("is_distribution_key")
	private boolean isDistKey = false;
	@JsonProperty("is_primary_key")
	private boolean isPrimaryKey = false;
	@JsonProperty("sort_key")
	private int sortKey = 0;
	@JsonIgnore
	private int JdbcType = 0;
	
	public SchemaColumn() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getCompressionType() {
		return compressionType;
	}

	public void setCompressionType(String compressionType) {
		this.compressionType = compressionType;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isDistKey() {
		return isDistKey;
	}

	public void setDistKey(boolean isDistKey) {
		this.isDistKey = isDistKey;
	}

	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	public void setPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public int getSortKey() {
		return sortKey;
	}

	public void setSortKey(int sortKey) {
		this.sortKey = sortKey;
	}

	public int getJdbcType() {
		return JdbcType;
	}

	public void setJdbcType(int jdbcType) {
		JdbcType = jdbcType;
	}
}

package io.lodr.common.model;

public class SampleZipInputData extends InputDataBase {
	private static final long serialVersionUID = 1L;

	private String webhook = null;
	private String destAccessId = null;
	private String destAccessKey = null;
	private String destBucketName = null;
	private String destRegion = null;
	private boolean diffBucket = false;
	
	public SampleZipInputData() {
	}

	public String getWebhook() {
		return webhook;
	}

	public void setWebhook(String webhook) {
		this.webhook = webhook;
	}

	public String getDestAccessId() {
		return destAccessId;
	}

	public void setDestAccessId(String destAccessId) {
		this.destAccessId = destAccessId;
	}

	public String getDestAccessKey() {
		return destAccessKey;
	}

	public void setDestAccessKey(String destAccessKey) {
		this.destAccessKey = destAccessKey;
	}

	public String getDestBucketName() {
		return destBucketName;
	}

	public void setDestBucketName(String destBucketName) {
		this.destBucketName = destBucketName;
	}

	public String getDestRegion() {
		return destRegion;
	}

	public void setDestRegion(String destRegion) {
		this.destRegion = destRegion;
	}

	public boolean isDiffBucket() {
		return diffBucket;
	}

	public void setDiffBucket(boolean diffBucket) {
		this.diffBucket = diffBucket;
	}
}

package io.lodr.common.model;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

//@DynamoDBTable(tableName = "CanceledJob")
public class CanceledJob {
	private String jobId = null;
	private Date canceledOn = new Date();
	
	public CanceledJob() {		
	}
	
	public CanceledJob(String jobId) {
		this.jobId = jobId;
	}

	@DynamoDBHashKey
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	@DynamoDBAttribute
	public Date getCanceledOn() {
		return canceledOn;
	}

	public void setCanceledOn(Date canceledOn) {
		this.canceledOn = canceledOn;
	}
}

package io.lodr.common.model.command;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import io.lodr.common.constant.FilterOperator;
import io.lodr.common.constant.FilterValueType;
import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class FilterCommand extends Command {
	private static final Logger logger = LoggerFactory.getLogger(FilterCommand.class);
		
	private FilterOperator filterOperator = null;
	private FilterValue filterValue = null;
	private FilterValueType filterValueType = null;
	
	private boolean isValid = true;
	
	public FilterCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		if (!isValid()) return row == null ? null : Lists.newArrayList(row);
		
		final String columnName = getColumnName();
		Optional<ColumnData> column = row.getColumnDatas().stream().filter(
				c -> columnName.equals(c.getName())).findAny();
		
		if (!column.isPresent()) {
			markInvalid("Column " + columnName + " does not exist.");
			return Lists.newArrayList(row);
		}
		
		if (FilterValueType.VALUE == getValueType()) {
			if (FilterOperator.EQUAL_TO == getOperator()) {
				if (StringUtils.equals(getFilterValue().getStringValue(), column.get().getValue())) {
					return Lists.newArrayList(row);
				}
			}
			else if (!CommonUtils.isBlank(column.get().getValue())) {
				if (getFilterValue().getDataType() == FilterValue.DataType.NUMERIC) {
					try {
						final float left = Float.parseFloat(column.get().getValue());
						
						if (compareValue(left, getFilterValue().getNumericValue(), getOperator())) {
							return Lists.newArrayList(row);
						}
					}
					catch (Exception e) {						
					}
				}
				else if (getFilterValue().getDataType() == FilterValue.DataType.DATE) {
					final Date left = CommonUtils.parseDate(column.get().getValue());
					
					if (left != null && compareValue(left, getFilterValue().getDateValue(), getOperator())) {
						return Lists.newArrayList(row);
					}
				}
			}
		}		
		else if (FilterValueType.COLUMN == getValueType()) {
			final String valueColumnName = getRight();
			Optional<ColumnData> valueColumn = row.getColumnDatas().stream().filter(
					c -> valueColumnName.equals(c.getName())).findAny();
			
			if (!valueColumn.isPresent()) {
				markInvalid("Value column " + valueColumnName + " does not exist.");
				return Lists.newArrayList(row);
			}
			
			Date left = null;
			Date right = null;
			if (FilterOperator.EQUAL_TO == getOperator()) {
				if (StringUtils.equals(column.get().getValue(), valueColumn.get().getValue())) {
					return Lists.newArrayList(row);
				}
			}
			else if (CommonUtils.isNumeric(column.get().getValue())
					&& CommonUtils.isNumeric(valueColumn.get().getValue())) {
				try {
					if (compareValue(Float.parseFloat(column.get().getValue()), 
							Float.parseFloat(valueColumn.get().getValue()), getOperator())) {
						return Lists.newArrayList(row);
					}
				}
				catch (Exception e) {					
				}
			}
			else if ((left = CommonUtils.parseDate(column.get().getValue())) != null
					&& (right = CommonUtils.parseDate(valueColumn.get().getValue())) != null) {
				if (compareValue(left, right, getOperator())) {
					return Lists.newArrayList(row);
				}
			}
		}
		
		return null;
	}
	
	private void markInvalid(String reason) {
		logger.info("Invalid filter command - {}", reason);
		isValid = false;
	}
	
	public String getRight() {
		return getOpt("right");
	}
	
	public FilterOperator getOperator() {
		if (filterOperator == null) {
			filterOperator = FilterOperator.fromSymbol(getOpt("operator")); 
		}
		return filterOperator;
	}
	
	FilterValueType getValueType() {
		if (filterValueType == null) {
			filterValueType = FilterValueType.valueOf(getOpt("value_type"));
		}
		return filterValueType;
	}
	
	FilterValue getFilterValue() {
		if (filterValue == null) {
			filterValue = computeFilterValue(getRight());
		}
		
		return filterValue;
	}
	
	// Preprocess value of filter expression to determine its data type
	public void preprocessFilterValue() {
		if (FilterValueType.VALUE == getValueType()) {
			filterValue = computeFilterValue(getRight());
			
			if (filterValue == null) {
				markInvalid("filter value is invalid (" + getRight() + ")");
			}
		}
	}
	
	private FilterValue computeFilterValue(String value) {
		if (FilterValueType.COLUMN == getValueType()) return null;
		
		FilterValue filterValue = new FilterValue();
		try {			
			switch (getOperator()) {
			case EQUAL_TO:
				filterValue.setStringValue(value);
				break;
			case LESS_THAN:
			case GREATER_THAN:
			case LESS_THAN_OR_EQUAL_TO:
			case GREATER_THAN_OR_EQUAL_TO:
				if (CommonUtils.isBlank(value)) {
					filterValue = null;
				}
				else {
					Date dateValue = null;
					if (CommonUtils.isNumeric(value)) {
						filterValue.setNumericValue(Float.parseFloat(value));
						filterValue.setDataType(FilterValue.DataType.NUMERIC);
					}
					else if ((dateValue = CommonUtils.parseDate(value)) != null) {
						filterValue.setDateValue(dateValue);
						filterValue.setDataType(FilterValue.DataType.DATE);
					}
					else {
						filterValue = null;
					}
				}
				break;
			default:
				// do nothing
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			filterValue = null;
		}
		
		return filterValue;
	}
	
	public boolean isValid() {
		return isValid;
	}
	
	private boolean compareValue(float left, float right, FilterOperator operator) {
		boolean result = false;
		switch (operator) {
		case GREATER_THAN:
			result = left > right;
			break;
		case LESS_THAN:
			result = left < right;
			break;
		case GREATER_THAN_OR_EQUAL_TO:
			result = left >= right;
			break;
		case LESS_THAN_OR_EQUAL_TO:
			result = left <= right;
			break;
		default:
				
		}
		
		return result;
	}
	
	private boolean compareValue(Date left, Date right, FilterOperator operator) {
		boolean result = false;
		switch (operator) {
		case GREATER_THAN:
			result = left.after(right);
			break;
		case LESS_THAN:
			result = left.before(right);
			break;
		case GREATER_THAN_OR_EQUAL_TO:
			result = left.compareTo(right) >= 0;
			break;
		case LESS_THAN_OR_EQUAL_TO:
			result = left.compareTo(right) <= 0;
			break;
		default:
				
		}
		
		return result;
	}
		
	private static class FilterValue {
		public enum DataType {STRING, NUMERIC, DATE};
		
		private float numericValue = 0;
		private String stringValue = null;
		private Date dateValue = null;
		private DataType dataType = DataType.STRING;
		
		public FilterValue() {			
		}

		public float getNumericValue() {
			return numericValue;
		}

		public void setNumericValue(float numericValue) {
			this.numericValue = numericValue;
		}

		public String getStringValue() {
			return stringValue;
		}

		public void setStringValue(String stringValue) {
			this.stringValue = stringValue;
		}

		public Date getDateValue() {
			return dateValue;
		}

		public void setDateValue(Date dateValue) {
			this.dateValue = dateValue;
		}

		public DataType getDataType() {
			return dataType;
		}

		public void setDataType(DataType dataType) {
			this.dataType = dataType;
		}
	}
}

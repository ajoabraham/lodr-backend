package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import io.lodr.common.constant.CSVDefaultSettings;

public class SampleSourceOutputData extends OutputDataBase {
	private static final long serialVersionUID = 1L;
	
	private String quote = null;
	private String delimiter = null;
	private String encoding = null;
	private List<String> columnNames = new ArrayList<>();
	private long fileSize;
	private List<List<String>> previewData = new ArrayList<>();
	
	public SampleSourceOutputData() {		
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		if (delimiter != null && delimiter.length() == 1 && delimiter.charAt(0) == '\0') {
			this.delimiter = CSVDefaultSettings.NO_DELIMITER;
		}
		else {
			this.delimiter = delimiter;
		}
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public List<List<String>> getPreviewData() {
		return previewData;
	}

	public void setPreviewData(List<List<String>> previewData) {
		this.previewData = previewData;
	}
}

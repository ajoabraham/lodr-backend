package io.lodr.common.model.command;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class InsertFileNameCommand extends Command {	
	public InsertFileNameCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny().ifPresent(c -> {
			final List<String> columnNames = row.getColumnDatas().stream().map(ColumnData::getName).collect(Collectors.toList());
			final int index = row.getColumnDatas().indexOf(c);
			row.getColumnDatas().add(index + 1, 
					new ColumnData(CommonUtils.getUniqueColumnName(columnNames, CommonConstants.DEFAULT_FILE_NAME_COLUMN_NAME), row.getSourceFileName(), 0));
			
			resetColumnPositions(row);
		});
		return Lists.newArrayList(row);
	}
}

package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;

public class TrimCommand extends Command {

	public TrimCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName()) && c.getValue() != null).findAny()
			.ifPresent(c -> c.setValue(c.getValue().trim()));
		return Lists.newArrayList(row);
	}

}

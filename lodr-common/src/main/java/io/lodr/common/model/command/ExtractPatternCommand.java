package io.lodr.common.model.command;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.Lists;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class ExtractPatternCommand extends Command {

	public ExtractPatternCommand() {
	}

	// TH 03/26/2017, this is the old implementation. It will match with given pattern
	// and use the limit as the number of match.
//	@Override
//	public List<RowData> execute(RowData row) {
//		final String columnName = getColumnName();
//		
//		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny()
//			.ifPresent(c -> {
//				final int limit = getLimit();
//				final String regex = convertToJavaRegex(getPattern());
//				final RowData newRow = new RowData();
//				final List<String> columnNames = getColumnNames(row);
//				
//				for (int i = 0; i < limit; i++) {
//					String newColumnName = CommonUtils.getUniqueColumnName(columnNames, columnName);
//					columnNames.add(newColumnName);
//					newRow.getColumnDatas().add(new ColumnData(newColumnName, "", i));
//				}
//				
//				int limitCount = 0;
//				Matcher matcher = Pattern.compile(regex).matcher(c.getValue());
//				
//				while (matcher.find() && limitCount < limit) {
//					String value = matcher.group();
//					newRow.getColumnDatas().get(limitCount).setValue(value);
//					limitCount++;
//				}
//				
//				row.getColumnDatas().addAll(row.getColumnDatas().indexOf(c) + 1, newRow.getColumnDatas());
//				
//				resetColumnPositions(row);
//			});
//		return Lists.newArrayList(row);
//	}
	
	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny()
			.ifPresent(c -> {
				final int limit = getLimit();
				final String regex = convertToJavaRegex(getPattern());
				final List<ColumnData> newColumns = Lists.newArrayList();
				final List<String> columnNames = getColumnNames(row);
				
				for (int i = 0; i < limit; i++) {
					String newColumnName = CommonUtils.getUniqueColumnName(columnNames, columnName);
					newColumns.add(new ColumnData(newColumnName, "", i));
					columnNames.add(newColumnName);
				}
										
				Matcher matcher = Pattern.compile(regex).matcher(c.getValue());
				
				if (matcher.find()) {
					if (matcher.groupCount() == 0) {
						newColumns.get(0).setValue(CommonUtils.isBlank(matcher.group()) ? "" : matcher.group());
					}
					else {
						for (int i = 1; i <= Math.min(matcher.groupCount(), limit); i++) {
							newColumns.get(i - 1).setValue(CommonUtils.isBlank(matcher.group(i)) ? "" : matcher.group(i));
						}
					}
				}
				
				row.getColumnDatas().addAll(row.getColumnDatas().indexOf(c) + 1, newColumns);
				resetColumnPositions(row);	
			});
		return Lists.newArrayList(row);
	}

	String getPattern() {
		return getOpt("pattern");
	}
	
	int getLimit() {
		return Integer.parseInt(getOpt("limit"));
	}
}

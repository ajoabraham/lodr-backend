package io.lodr.common.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import io.lodr.common.constant.CommonConstants;

public final class DataItem {
	private Map<String, String> values = null;
	
	public DataItem() {
		// Use LinkedHashMap to make sure that Map will keep the order
		// of key entered.
		values = new LinkedHashMap<>();
	}
	
	public Map<String, String> getValueMap() {
		return values;
	}
	
	public List<String> getValues() {
		return values.entrySet().stream().map(Entry::getValue).collect(Collectors.toList());
	}
	
	public String getValue(String headerName) {
		return values.get(headerName);
	}
	
	public void addValue(String headerName, String value) {
		// FIXME Should fix this in Microstrategy
		if (value.startsWith(";"))
			value = value.substring(1, value.length());
		values.put(headerName, value);
	}
	
	public int size() {
		return values.size();
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		int index = 0;
		for (Entry<String, String> entry : values.entrySet()) {
			sb.append(entry.getValue());
			
			if (index++ != (values.size() - 1)) {
				sb.append(CommonConstants.DEFAULT_CSV_DELIMITER);
			}
		}
		
		return sb.toString();
	}
}

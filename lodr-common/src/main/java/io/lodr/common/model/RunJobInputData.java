package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.lodr.common.constant.LoadMode;
import io.lodr.common.constant.RedshiftDistStyle;
import io.lodr.common.model.command.Command;
import io.lodr.common.util.ValidateFileInfos;
import io.lodr.common.util.ValidateLoadMode;

@ValidateFileInfos
@ValidateLoadMode
@JsonIgnoreProperties(ignoreUnknown = true)
public class RunJobInputData extends InputDataBase {
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "jobId cannot be empty")
	private String jobId = null;
	@NotEmpty(message = "fileInfos cannot be empty")
	private List<FileInfo> fileInfos = new ArrayList<>();
	private List<ColumnInfo> columnInfos = new ArrayList<>();
	@JsonProperty("wrangleScript")
	private List<Command> commands = new ArrayList<>();
	@NotNull(message = "connInfo cannot be empty")
	private ConnInfo connInfo = null;
	@NotBlank(message = "tableName cannot be empty")
	private String tableName = null;
	private List<String> primaryKeys = new ArrayList<>();
	private List<String> sortKeys = new ArrayList<>();
	private RedshiftDistStyle distStyle = RedshiftDistStyle.EVEN;
	private String distKey = null;
	private LoadMode loadMode = LoadMode.REPLACE;
	private List<String> mergeKeys = new ArrayList<>();
//	private boolean includesHeader = true;
	private String webhook = null;
	@NotEmpty(message = "originalColumnNames cannot be empty")
	private List<String> originalColumnNames = new ArrayList<>();
	private String preSql = null;
	private String postSql = null;
	private String filePrefix = null;
	private boolean useFilePrefix = false;

	public RunJobInputData() {
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public List<ColumnInfo> getColumnInfos() {
		return columnInfos;
	}

	public void setColumnInfos(List<ColumnInfo> columnInfos) {
		this.columnInfos = columnInfos;
	}

	public List<Command> getCommands() {
		return commands;
	}

	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}

	public ConnInfo getConnInfo() {
		return connInfo;
	}

	public void setConnInfo(ConnInfo connInfo) {
		this.connInfo = connInfo;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<String> getPrimaryKeys() {
		return primaryKeys;
	}

	public void setPrimaryKeys(List<String> primaryKeys) {
		this.primaryKeys = primaryKeys;
	}

	public RedshiftDistStyle getDistStyle() {
		return distStyle;
	}

	public void setDistStyle(RedshiftDistStyle distStyle) {
		this.distStyle = distStyle;
	}

	public String getDistKey() {
		return distKey;
	}

	public void setDistKey(String distKey) {
		this.distKey = distKey;
	}

	public List<String> getSortKeys() {
		return sortKeys;
	}

	public void setSortKeys(List<String> sortKeys) {
		this.sortKeys = sortKeys;
	}

	public LoadMode getLoadMode() {
		return loadMode;
	}

	public void setLoadMode(LoadMode loadMode) {
		this.loadMode = loadMode;
	}

	public List<String> getMergeKeys() {
		return this.mergeKeys;
	}

	public void setMergeKeys(List<String> mergeKeys) {
		this.mergeKeys = mergeKeys;
	}

	public List<FileInfo> getFileInfos() {
		return fileInfos;
	}

	public void setFileInfos(List<FileInfo> fileInfos) {
		this.fileInfos = fileInfos;
	}

//	public boolean getIncludesHeader() {
//		return includesHeader;
//	}
//
//	public void setIncludesHeader(boolean includesHeader) {
//		this.includesHeader = includesHeader;
//	}

	public String getWebhook() {
		return webhook;
	}

	public void setWebhook(String webhook) {
		this.webhook = webhook;
	}

	public List<String> getOriginalColumnNames() {
		return originalColumnNames;
	}

	public void setOriginalColumnNames(List<String> originalColumnNames) {
		this.originalColumnNames = originalColumnNames;
	}

	public String getPreSql() {
		return preSql;
	}

	public void setPreSql(String preSql) {
		this.preSql = preSql;
	}

	public String getPostSql() {
		return postSql;
	}

	public void setPostSql(String postSql) {
		this.postSql = postSql;
	}

	public String getFilePrefix() {
		return filePrefix;
	}

	public void setFilePrefix(String filePrefix) {
		this.filePrefix = filePrefix;
	}

	public boolean getUseFilePrefix() {
		return useFilePrefix;
	}

	public void setUseFilePrefix(boolean useFilePrefix) {
		this.useFilePrefix = useFilePrefix;
	}
}

package io.lodr.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class RowData implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	private final List<ColumnData> columnDatas = new ArrayList<>();
	private String sourceFileName = null;
	
	public RowData() {
		
	}

	public List<ColumnData> getColumnDatas() {
		return columnDatas;
	}

//	public void setColumnDatas(List<ColumnData> columnDatas) {
//		this.columnDatas = columnDatas;
//	}
	
	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}
	
	public List<String> getColumnNames() {
		return columnDatas.stream().map(ColumnData::getName).collect(Collectors.toList());
	}
	
	public Object clone() {
		RowData rowData = new RowData();
		
		rowData.setSourceFileName(sourceFileName);		
		rowData.getColumnDatas().addAll(getColumnDatas().stream().map(ColumnData::clone).map(c -> (ColumnData) c).collect(Collectors.toList()));
		
		return rowData;
	}
}

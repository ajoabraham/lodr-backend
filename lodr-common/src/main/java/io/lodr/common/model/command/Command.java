package io.lodr.common.model.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;

@JsonTypeInfo(
	    use = JsonTypeInfo.Id.NAME,
	    include = JsonTypeInfo.As.PROPERTY,
	    property = "command"
)

@JsonSubTypes({
	    @JsonSubTypes.Type(value = DeleteCommand.class, name = "delete"),
	    @JsonSubTypes.Type(value = RenameCommand.class, name = "rename"),
	    @JsonSubTypes.Type(value = MoveCommand.class, name = "move"),
	    @JsonSubTypes.Type(value = MergeCommand.class, name = "merge"),
	    @JsonSubTypes.Type(value = UpperCaseCommand.class, name = "upper_case"),
	    @JsonSubTypes.Type(value = LowerCaseCommand.class, name = "lower_case"),
	    @JsonSubTypes.Type(value = CapitalizeCommand.class, name = "capitalize"),
	    @JsonSubTypes.Type(value = TitleizeCommand.class, name = "titleize"),
	    @JsonSubTypes.Type(value = SnakeCaseCommand.class, name = "snake_case"),
	    @JsonSubTypes.Type(value = TrimCommand.class, name = "trim"),
	    @JsonSubTypes.Type(value = SplitDelimiterCommand.class, name = "split_delimiter"),
	    @JsonSubTypes.Type(value = SplitPositionCommand.class, name = "split_position"),
	    @JsonSubTypes.Type(value = ExtractBetweenCommand.class, name = "extract_between"),
	    @JsonSubTypes.Type(value = ExtractPatternCommand.class, name = "extract_pattern"),
	    @JsonSubTypes.Type(value = FindReplaceCommand.class, name = "find_replace"),
	    @JsonSubTypes.Type(value = MergeAllRowsCommand.class, name = "merge_all_rows"),
	    @JsonSubTypes.Type(value = InsertFileNameCommand.class, name = "insert_file_name"),
	    @JsonSubTypes.Type(value = ExtractJsonCommand.class, name = "extract_json"),
	    @JsonSubTypes.Type(value = DeleteRowsCommand.class, name = "delete_rows")
})
public abstract class Command {
	@JsonProperty("columnName")
	private String columnName = null;
	@JsonProperty("opts")
	@JsonInclude(Include.NON_EMPTY)
	private Map<String, String> opts = new HashMap<>();
	
	public Command() {		
	}
		
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName == null ? null : columnName.trim();
	}
	
	public Map<String, String> getOpts() {
		return opts;
	}
	
	@JsonIgnore
	public String getOpt(String key) {
		return opts.get(key);
	}
	
	List<String> getColumnNames(RowData row) {
		return row.getColumnDatas().stream().map(ColumnData::getName).collect(Collectors.toList());
	}
	
	void resetColumnPositions(RowData row) {
		for (int i = 0; i < row.getColumnDatas().size(); i++) {
			row.getColumnDatas().get(i).setPosition(i);
		}
	}
	
	String convertToJavaRegex(String regex) {
		// TH 02/20/2017, forgot about why need to replace \\ with \\\\.
		// However, this replacement cause issues when regex pattern
		// escape some characters.
//		return regex.replace("\\", "\\\\");
		return regex;
	}

	public abstract List<RowData> execute(RowData row);
}

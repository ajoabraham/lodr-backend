package io.lodr.common.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.util.ValidateBucketName;
import io.lodr.common.util.ValidateObjectKey;

@ValidateBucketName
@ValidateObjectKey
@DynamoDBDocument
public abstract class InputDataBase implements InputData, Cloneable {
	private static final long serialVersionUID = 1L;
	
	private String accessId = null;
	private String accessKey = null;
	private String region = null;
	private String bucketName = null;
	private String objectKey = null;
	private String userId = null;
	
	public InputDataBase() {		
	}

	@DynamoDBAttribute
	public String getAccessId() {
		return accessId;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	@DynamoDBAttribute
	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	@DynamoDBAttribute
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@DynamoDBAttribute
	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	@DynamoDBAttribute
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@DynamoDBAttribute
	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}
	
	@DynamoDBIgnore
	public String getOutputFolderObjectKey() {
		return getUserId() + "/" + CommonConstants.DEFAULT_SPLITS_FOLDER + "/";
	}

	@Override
	protected Object clone() {
		try {
			InputDataBase clone = getClass().newInstance();
			
			clone.setAccessId(accessId);
			clone.setAccessKey(accessKey);
			clone.setBucketName(bucketName);
			clone.setObjectKey(objectKey);
			clone.setRegion(region);
			clone.setUserId(userId);
			
			return clone;
		}
		catch (Exception e) {			
			throw new RuntimeException(e);
		}
	}	
}

package io.lodr.common.model.command;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class CapitalizeCommand extends Command {

	public CapitalizeCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName()) && !CommonUtils.isBlank(c.getValue())).findAny()
			.ifPresent(c -> c.setValue(StringUtils.capitalize(c.getValue())));
		
		return Lists.newArrayList(row);
	}
}

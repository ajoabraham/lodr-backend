package io.lodr.common.model.command;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.JsonUtils;

public class ExtractJsonCommand extends Command {
	private static final Logger logger = LoggerFactory.getLogger(ExtractJsonCommand.class.getName());
	
	private enum PathLevel {SAME, DIFF, HIGHER, LOWER};

	public ExtractJsonCommand() {
	}

	@Override
	public List<RowData> execute(RowData rowData) {
		List<RowData> transformedRows = new ArrayList<>();
		
		List<String> currColumnNames = getColumnNames(rowData);
		
		final String cn = getColumnName();
		rowData.getColumnDatas().stream().filter(c -> cn.equals(c.getName())).findAny().ifPresent(c -> {
			final String jsonString = c.getValue();
			final boolean retainColumn = doesRetainColumn();
			final boolean expandArray = doesExpandArray();
			final String[] paths = getPaths().split(",");

			final List<List<ColumnData>> tempRows = new ArrayList<>();
			for (int i = 0; i < paths.length; i++) {
				final String columnName = CommonUtils.getUniqueColumnName(currColumnNames, pathToColumnName(paths[i]));
				currColumnNames.add(columnName);

				if (i == 0) {
					final Object result = JsonUtils.jsonPath(jsonString, paths[i]);

					if (expandArray && result instanceof ArrayList && !((ArrayList<?>) result).isEmpty()) {
						for (Object o : (ArrayList<?>) result) {
							tempRows.add(Lists.newArrayList(new ColumnData(columnName, convertToString(o), 0)));
						}
					}
					else {
						tempRows.add(Lists.newArrayList(new ColumnData(columnName, convertToString(result), 0)));
					}
				}
				else {
					final Object result = JsonUtils.jsonPath(jsonString, paths[i]);					
					final PathLevel level = comparePaths(paths[0], paths[i]);

					if (expandArray && level != PathLevel.DIFF && level != PathLevel.LOWER 
							&& result instanceof ArrayList && !((ArrayList<?>)result).isEmpty()) {
						if (level == PathLevel.HIGHER && ((ArrayList<?>) result).size() == 1) {
							final Object o = ((ArrayList<?>) result).get(0);
							final String value = convertToString(o);

							for (List<ColumnData> tempRow : tempRows) {
								tempRow.add(new ColumnData(columnName, value, 0));
							}
						}
						else {
							int j = 0;
							for (Object o : (ArrayList<?>) result) {
								if (tempRows.size() <= j) {
									List<ColumnData> tempRow = new ArrayList<>();

									for (int index = 0; index < tempRows.get(0).size() - 1; index++) {
										final ColumnData clonedColumn = (ColumnData) tempRows.get(0).get(index).clone();
										clonedColumn.setValue("");
										tempRow.add(clonedColumn);
									}

									tempRows.add(tempRow);
								}

								tempRows.get(j).add(new ColumnData(columnName, convertToString(o), 0));
								j++;
							}
							
							if (j < tempRows.size()) {
								for (int k = j; k < tempRows.size(); k++) {
									tempRows.get(k).add(new ColumnData(columnName, "", 0));
								}
							}
						}
					}
					else {
						final String value = convertToString(result);

						for (List<ColumnData> tempRow : tempRows) {
							tempRow.add(new ColumnData(columnName, value, 0));
						}
					}
				}
			}

			transformedRows.add(rowData);

			for (int i = 0; i < (tempRows.size() - 1); i++) {
				transformedRows.add((RowData) rowData.clone());
			}

			int insertIndex = rowData.getColumnDatas().indexOf(c) + 1;

			for (int i = 0; i < transformedRows.size(); i++) {
				transformedRows.get(i).getColumnDatas().addAll(insertIndex, tempRows.get(i));
				
				if (!retainColumn) {
					transformedRows.get(i).getColumnDatas().remove(insertIndex - 1);
				}
				
				resetColumnPositions(transformedRows.get(i));
			}
		});
		
		if (transformedRows.isEmpty()) transformedRows.add(rowData);
		
		return transformedRows;
	}

	String getPaths() {
		return getOpt("path");
	}
	
	boolean doesRetainColumn() {
		final String retainColumn = getOpt("retain_column");
		return retainColumn == null ? false
				: "on".equalsIgnoreCase(retainColumn);
	}

	boolean doesExpandArray() {
		final String expandArray = getOpt("expand_array");
		return expandArray == null ? true : "on".equalsIgnoreCase(expandArray);
	}
	
	private String pathToColumnName(String path) {
//		return path.trim().replaceAll("[\\[\\]\\*\\d\\$-]+", "").replaceAll(
//				"\\.+", "_").replaceAll("^_", "");
		String colName = path.trim().replaceAll("(\\.|\\s)+", "_")
					.replaceAll("[^a-zA-Z0-9_]+", "")
					.replaceAll("^_", "");
		if(colName == null || colName.trim().equals("")){
			colName = "COLUMN";
		}		
		
		logger.info("path to columnName: " +path+ " ==> " + colName);
		return colName;
	}

	private PathLevel comparePaths(String firstPath, String path) {
		final String[] fp = firstPath.trim().split("\\.");
		final String[] p = path.trim().split("\\.");

		PathLevel level = PathLevel.SAME;

		if (p.length > fp.length) {
			level = PathLevel.LOWER;

			for (int i = 0; i < fp.length - 1; i++) {
				if (!p[i].equals(fp[i])) {
					level = PathLevel.DIFF;
					break;
				}
			}
		}
		else if (p.length < fp.length) {
			level = PathLevel.HIGHER;

			for (int i = 0; i < p.length - 1; i++) {
				if (!p[i].equals(fp[i])) {
					level = PathLevel.DIFF;
					break;
				}
			}
		}
		else {
			for (int i = 0; i < p.length - 1; i++) {
				if (!p[i].equals(fp[i])) {
					level = PathLevel.DIFF;
					break;
				}
			}
		}
		return level;
	}

	private String convertToString(Object result) {
		String value = null;

		if (result instanceof ArrayList && ((ArrayList<?>)result).isEmpty()) {
			value = "";
		}
		else if (!(result instanceof String)) {
			try {
				value = JsonUtils.toJsonString(result);
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		else {
			value = result.toString();
		}

		return value;
	}
}

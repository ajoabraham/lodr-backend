package io.lodr.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.lodr.common.constant.QueryState;

public class PostBackQueryResults {
	@JsonProperty("query_id")
	private String queryId = null;
	@JsonProperty("result_data")
	private QueryResultData resultData = new QueryResultData();
	@JsonProperty("info")
	private String info = null;
	@JsonProperty("total_rows")
	private long totalRows = 0;
	@JsonProperty("fetched_rows")
	private long fetchedRows = 0;
	@JsonProperty("finished_at")
	private String finishedAt = null;
	@JsonProperty("last_executed_query")
	private String lastExecutedQuery = null;
	@JsonProperty("status")
	private QueryState status = QueryState.COMPLETED;
	@JsonProperty("query_error")
	private String queryError = null;
	
	public PostBackQueryResults() {		
	}
	
	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public QueryResultData getResultData() {
		return resultData;
	}

	public void setResultData(QueryResultData resultData) {
		this.resultData = resultData;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public long getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(long totalRows) {
		this.totalRows = totalRows;
	}

	public long getFetchedRows() {
		return fetchedRows;
	}

	public void setFetchedRows(long fetchedRows) {
		this.fetchedRows = fetchedRows;
	}

	public String getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(String finishedAt) {
		this.finishedAt = finishedAt;
	}

	public String getLastExecutedQuery() {
		return lastExecutedQuery;
	}

	public void setLastExecutedQuery(String lastExecutedQuery) {
		this.lastExecutedQuery = lastExecutedQuery;
	}

	public QueryState getStatus() {
		return status;
	}

	public void setStatus(QueryState status) {
		this.status = status;
	}

	public String getQueryError() {
		return queryError;
	}

	public void setQueryError(String queryError) {
		this.queryError = queryError;
	}

	public void clear() {
		resultData.getColumns().clear();
		resultData.getData().clear();
		queryError = null;
		status = QueryState.COMPLETED;
		info = null;
		lastExecutedQuery = null;
	}
}

package io.lodr.common.model;

import io.lodr.common.constant.LoadMode;

public final class PublishResult {
	private LoadMode loadMode = null;
//	private long totalRowsLoaded = 0;
	private long updatedRows = 0;
	private long insertedRows = 0;

	public PublishResult() {
	}

	public LoadMode getLoadMode() {
		return loadMode;
	}

	public void setLoadMode(LoadMode loadMode) {
		this.loadMode = loadMode;
	}

//	public long getTotalRowsLoaded() {
//		return totalRowsLoaded;
//	}
//
//	public void setTotalRowsLoaded(long totalRowsLoaded) {
//		this.totalRowsLoaded = totalRowsLoaded;
//	}

	public long getUpdatedRows() {
		return updatedRows;
	}

	public void setUpdatedRows(long updatedRows) {
		this.updatedRows = updatedRows;
	}

	public long getInsertedRows() {
		return insertedRows;
	}

	public void setInsertedRows(long insertedRows) {
		this.insertedRows = insertedRows;
	}

}

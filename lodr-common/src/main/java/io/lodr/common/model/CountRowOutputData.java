package io.lodr.common.model;

public class CountRowOutputData extends OutputDataBase {
	private static final long serialVersionUID = 1L;

	private long rowCount = 0;
	
	public CountRowOutputData() {
	}

	public long getRowCount() {
		return rowCount;
	}

	public void setRowCount(long rowCount) {
		this.rowCount = rowCount;
	}
}

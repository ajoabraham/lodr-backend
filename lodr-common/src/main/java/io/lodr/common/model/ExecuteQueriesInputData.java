package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

import io.lodr.common.util.CommonUtils;

public class ExecuteQueriesInputData implements InputData {
	private static final long serialVersionUID = 1L;

	private String queryId = null;
	@JsonProperty("sql")
	private String sql = null;
	@JsonIgnore
	private List<String> queries = new ArrayList<>();
	private int rowLimit = 2000;
	private ConnInfo connInfo = null;
	private String webhook = null;
	
	public ExecuteQueriesInputData() {		
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public List<String> getQueries() {
		if (queries.isEmpty() && !CommonUtils.isBlank(sql)) {
			queries.addAll(Lists.newArrayList(sql.split(";")).stream()
					.filter(q -> !CommonUtils.isBlank(q))
					.map(q -> q.trim())
					.collect(Collectors.toList()));
		}
		return queries;
	}

//	public void setQueries(List<String> queries) {
//		this.queries = queries;
//	}

	public int getRowLimit() {
		return rowLimit;
	}

	public void setRowLimit(int rowLimit) {
		this.rowLimit = rowLimit;
	}

	public ConnInfo getConnInfo() {
		return connInfo;
	}

	public void setConnInfo(ConnInfo connInfo) {
		this.connInfo = connInfo;
	}

	public String getWebhook() {
		return webhook;
	}

	public void setWebhook(String webhook) {
		this.webhook = webhook;
	}
}

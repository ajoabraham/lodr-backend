package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class SplitPositionCommand extends Command {

	public SplitPositionCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny().ifPresent(c -> {
			List<String> columnNames = getColumnNames(row);
			String column0Name = CommonUtils.getUniqueColumnName(columnNames, columnName);
			columnNames.add(column0Name);
			String column1Name = CommonUtils.getUniqueColumnName(columnNames, columnName);
			ColumnData newColumn0 = new ColumnData(column0Name, "", c.getPosition());
			ColumnData newColumn1 = new ColumnData(column1Name, "", c.getPosition());
			
			final String value = c.getValue();
			final int position = getPosition();
			
			if (!CommonUtils.isBlank(value)) {
				if (position <= 0) {
					newColumn1.setValue(value);
				}
				else if (position >= value.length()) {
					newColumn0.setValue(value);
				}
				else {
					newColumn0.setValue(value.substring(0, position));
					newColumn1.setValue(value.substring(position));
				}
			}
			
			final int index = row.getColumnDatas().indexOf(c);
			row.getColumnDatas().add(index + 1, newColumn0);
			row.getColumnDatas().add(index + 2, newColumn1);
			
			if (!doesRetainColumn()) {
				row.getColumnDatas().remove(c);
			}
			
			resetColumnPositions(row);
		});
		
		return Lists.newArrayList(row);
	}

	int getPosition() {
		return Integer.parseInt(getOpt("position").toString());
	}
	
	boolean doesRetainColumn() {
		final String retainColumn = getOpt("retain_column");
		return retainColumn == null ? false : "on".equalsIgnoreCase(retainColumn);
	}
}

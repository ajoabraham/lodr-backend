package io.lodr.common.model;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.util.CommonUtils;

public class SampleWranglerSourceInputData extends InputDataBase {
	private static final long serialVersionUID = 1L;

	// How many rows should be included in the sample
	private static final int DEFAULT_SAMPLE_ROW_COUNT = 1000;

//	@NotNull(message = "quote cannot be empty")
	private String quote = null;
//	@NotNull(message = "message cannot be empty")
	private String delimiter = null;
//	@NotBlank(message = "encoding cannot be empty")
	private String encoding = null;
	private int sampleRowCount = 0;
	private boolean includesHeader = true;

	public SampleWranglerSourceInputData() {

	}

	public String getQuote() {
		return CommonUtils.isBlank(quote) ? CommonConstants.DEFAULT_CSV_QUOTE : quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public String getDelimiter() {
//		return delimiter == null ? CommonConstants.DEFAULT_CSV_DELIMITER : delimiter;
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public String getEncoding() {
		return CommonUtils.isBlank(encoding) ? CommonConstants.DEFAULT_ENCODING_NAME : encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public int getSampleRowCount() {
		return sampleRowCount <= 0 ? DEFAULT_SAMPLE_ROW_COUNT : sampleRowCount;
	}

	public void setSampleRowCount(int sampleRowCount) {
		this.sampleRowCount = sampleRowCount;
	}

	public boolean getIncludesHeader() {
		return includesHeader;
	}

	public void setIncludesHeader(boolean includesHeader) {
		this.includesHeader = includesHeader;
	}
}

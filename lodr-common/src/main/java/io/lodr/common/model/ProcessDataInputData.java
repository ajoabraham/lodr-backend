package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.command.Command;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.JsonUtils;

@DynamoDBDocument
public class ProcessDataInputData extends InputDataBase {
	private static final Logger logger = LoggerFactory.getLogger(ProcessDataInputData.class);
	
	private static final long serialVersionUID = 1L;

	private String jobId = null;
	private int batchId = 0;
	private String quote = null;
	private String delimiter = null;
	private String encoding = null;
	private List<ColumnInfo> columnInfos = new ArrayList<>();
	private List<Command> commands = new ArrayList<>();
	private String wrangleScript = null;
	private String outputFilePrefix = null;
	private boolean includesHeader = true;
	private List<String> columnNames = new ArrayList<>();
	private List<String> originalColumnNames = new ArrayList<>();
	private int postBackInterval = CommonConstants.DEFAULT_POST_BACK_INTERVAL;
	private List<FileInfo> fileInfos = new ArrayList<>();
	private String destAccessId = null;
	private String destAccessKey = null;
	private String destBucketName = null;
	private String destRegion = null;

	public ProcessDataInputData() {
	}

	@DynamoDBAttribute
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	@DynamoDBAttribute
	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	@DynamoDBAttribute
	public List<ColumnInfo> getColumnInfos() {
		return columnInfos;
	}

	public void setColumnInfos(List<ColumnInfo> columnInfos) {
		this.columnInfos = columnInfos;
	}

	@DynamoDBAttribute
	public String getQuote() {
		return CommonUtils.isBlank(quote) ? CommonConstants.DEFAULT_CSV_QUOTE
				: quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	@DynamoDBAttribute
	public String getDelimiter() {
		return delimiter == null ? CommonConstants.DEFAULT_CSV_DELIMITER : delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	@DynamoDBAttribute
	public String getEncoding() {
		return CommonUtils.isBlank(encoding) ? CommonConstants.DEFAULT_ENCODING_NAME : encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@DynamoDBAttribute
	public String getOutputFilePrefix() {
		return outputFilePrefix;
	}

	public void setOutputFilePrefix(String outputFilePrefix) {
		this.outputFilePrefix = outputFilePrefix;
	}

	@DynamoDBAttribute
	public boolean getIncludesHeader() {
		return includesHeader;
	}

	public void setIncludesHeader(boolean includesHeader) {
		this.includesHeader = includesHeader;
	}

	@DynamoDBAttribute
	public String getWrangleScript() {
		return wrangleScript;
	}

	public void setWrangleScript(String wrangleScript) {
		this.wrangleScript = wrangleScript;
	}

	@DynamoDBIgnore
	public List<Command> getCommands() {
		if (commands.isEmpty() && !CommonUtils.isBlank(getWrangleScript())) {
			try {
				commands = JsonUtils.toObject(new TypeReference<List<Command>>() {}, getWrangleScript());
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		return commands;
	}

	public List<String> getOriginalColumnNames() {
		return originalColumnNames;
	}

	public void setOriginalColumnNames(List<String> originalColumnNames) {
		this.originalColumnNames = originalColumnNames;
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

	public int getPostBackInterval() {
		return postBackInterval;
	}

	public void setPostBackInterval(int postBackInterval) {
		this.postBackInterval = postBackInterval;
	}

	public List<FileInfo> getFileInfos() {
		return fileInfos;
	}

	public void setFileInfos(List<FileInfo> fileInfos) {
		this.fileInfos = fileInfos;
	}

	public String getDestAccessId() {
		return destAccessId;
	}

	public void setDestAccessId(String destAccessId) {
		this.destAccessId = destAccessId;
	}

	public String getDestAccessKey() {
		return destAccessKey;
	}

	public void setDestAccessKey(String destAccessKey) {
		this.destAccessKey = destAccessKey;
	}

	public String getDestBucketName() {
		return destBucketName;
	}

	public void setDestBucketName(String destBucketName) {
		this.destBucketName = destBucketName;
	}

	public String getDestRegion() {
		return destRegion;
	}

	public void setDestRegion(String destRegion) {
		this.destRegion = destRegion;
	}
	
	@Override
	public ProcessDataInputData clone() {
		ProcessDataInputData id = new ProcessDataInputData();
		id.setAccessId(getAccessId());
		id.setAccessKey(getAccessKey());
		id.setBucketName(getBucketName());
		id.setColumnInfos(getColumnInfos());
		id.setColumnNames(getColumnNames());
		id.setDelimiter(getDelimiter());
		id.setDestAccessId(getDestAccessId());
		id.setDestAccessKey(getDestAccessKey());
		id.setDestBucketName(getDestBucketName());
		id.setDestRegion(getDestRegion());
		id.setEncoding(getEncoding());
		id.setFileInfos(getFileInfos());
		id.setIncludesHeader(getIncludesHeader());
		id.setJobId(getJobId());
		id.setObjectKey(getObjectKey());
		id.setOriginalColumnNames(getOriginalColumnNames());
		id.setOutputFilePrefix(getOutputFilePrefix());
		id.setPostBackInterval(getPostBackInterval());
		id.setQuote(getQuote());
		id.setRegion(getRegion());
		id.setUserId(getUserId());
		id.setWrangleScript(getWrangleScript());
		
		return id;
	}
}

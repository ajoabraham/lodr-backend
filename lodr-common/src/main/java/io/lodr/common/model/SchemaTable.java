package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SchemaTable {
	public enum TableType {TABLE, VIEW};
	
	@JsonProperty("name")
	private String name = null;
	@JsonProperty("schema")
	private String schema = null;
	@JsonProperty("table_type")
	private TableType tableType = TableType.TABLE;
	@JsonProperty("columns")
	private List<SchemaColumn> columns = new ArrayList<>();
	@JsonProperty("view_def")
	private String viewDef = null;
	@JsonProperty("table_size")
	private int tableSize = 0;
	
	public SchemaTable() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public TableType getTableType() {
		return tableType;
	}

	public void setTableType(TableType tableType) {
		this.tableType = tableType;
	}

	public List<SchemaColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<SchemaColumn> columns) {
		this.columns = columns;
	}

	public String getViewDef() {
		return viewDef;
	}

	public void setViewDef(String viewDef) {
		this.viewDef = viewDef;
	}

	public int getTableSize() {
		return tableSize;
	}

	public void setTableSize(int tableSize) {
		this.tableSize = tableSize;
	}
}

package io.lodr.common.model.command;

import java.util.List;
import java.util.regex.Pattern;

import com.google.common.collect.Lists;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class SplitDelimiterCommand extends Command {

	public SplitDelimiterCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny()
			.ifPresent(c -> {
				final int limit = getLimit();
				final String delimiter = getDelimiter();
				final RowData newRow = new RowData();
				final List<String> columnNames = getColumnNames(row);
				
				for (int i = 0; i < limit; i++) {
					String newColumnName = CommonUtils.getUniqueColumnName(columnNames, columnName);
					columnNames.add(newColumnName);
					newRow.getColumnDatas().add(new ColumnData(newColumnName, "", i));
				}
				
				StringBuffer regex = new StringBuffer();
				
				if (isCaseInsensitive()) {
					regex.append("(?i)");
				}
				
				if (isRegex()) {
					regex.append(convertToJavaRegex(delimiter));
				}
				else {
					regex.append(Pattern.quote(delimiter));
				}
				
				String value = c.getValue() == null ? "" : c.getValue();
				String[] splitted = value.split(regex.toString(), limit);
				int size = Math.min(splitted.length, newRow.getColumnDatas().size());
				
				for (int i = 0; i < size; i++) {
					newRow.getColumnDatas().get(i).setValue(splitted[i]);
				}
				
				row.getColumnDatas().addAll(row.getColumnDatas().indexOf(c) + 1, newRow.getColumnDatas());
				
				if (!doesRetainColumn()) {
					row.getColumnDatas().remove(c);
				}
				
				resetColumnPositions(row);
			});

		return Lists.newArrayList(row);
	}
	
	String getDelimiter() {
		return getOpt("delimiter");
	}

	boolean doesRetainColumn() {
		final String retainColumn = getOpt("retain_column");
		return retainColumn == null ? false : "on".equalsIgnoreCase(retainColumn);
	}
	
	int getLimit() {
		return Integer.parseInt(getOpt("limit"));
	}
	
	boolean isCaseInsensitive() {
		final String caseInsensitive = getOpt("case_insensitive");
		return caseInsensitive == null ? true : "on".equalsIgnoreCase(caseInsensitive);
	}
	
	boolean isRegex() {
		final String regex = getOpt("regex");
		return regex == null ? false : "on".equalsIgnoreCase(regex);
	}
}

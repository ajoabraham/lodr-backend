package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.lodr.common.constant.SchemaStatus;
import io.lodr.common.model.SchemaTable.TableType;

public class PostBackTableSchema {
	@JsonProperty("schema_status")
	private SchemaStatus schemaStatus = SchemaStatus.SCHEMA_FETCHING;
	@JsonProperty("schema_error")
	private String schemaError = null;
	@JsonProperty("name")
	private String name = null;
	@JsonProperty("schema")
	private String schema = null;
	@JsonProperty("table_type")
	private TableType tableType = TableType.TABLE;
	@JsonProperty("columns")
	private List<SchemaColumn> columns = new ArrayList<>();
	@JsonProperty("view_def")
	private String viewDef = null;
	@JsonProperty("table_size")
	private int tableSize = 0;
	
	public PostBackTableSchema() {

	}

	public String getSchemaError() {
		return schemaError;
	}

	public void setSchemaError(String schemaError) {
		this.schemaError = schemaError;
	}

	public SchemaStatus getSchemaStatus() {
		return schemaStatus;
	}

	public void setSchemaStatus(SchemaStatus schemaStatus) {
		this.schemaStatus = schemaStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public TableType getTableType() {
		return tableType;
	}

	public void setTableType(TableType tableType) {
		this.tableType = tableType;
	}

	public List<SchemaColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<SchemaColumn> columns) {
		this.columns = columns;
	}

	public String getViewDef() {
		return viewDef;
	}

	public void setViewDef(String viewDef) {
		this.viewDef = viewDef;
	}

	public int getTableSize() {
		return tableSize;
	}

	public void setTableSize(int tableSize) {
		this.tableSize = tableSize;
	}
}

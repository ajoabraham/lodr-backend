package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;

public class RenameCommand extends Command {
	
	public RenameCommand() {
		
	}
	
	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		final String newColumnName = getNewColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny().ifPresent(c -> c.setName(newColumnName));
		
		return Lists.newArrayList(row);
	}
	
	String getNewColumnName() {
		return getOpt("new_column_name");
	}
}

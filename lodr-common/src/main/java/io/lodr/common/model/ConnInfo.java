package io.lodr.common.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public final class ConnInfo implements InputData {
	private static final long serialVersionUID = 1L;
	
	private static final String jdbcDriver = "com.amazon.redshift.jdbc42.Driver";
//	private static final String jdbcDriver = "com.amazon.redshift.jdbc41.Driver";
	private static final String jdbcUrlTemplate = "jdbc:redshift://%s:%d/%s";
	
	private String serverAddress = null;
	private int port = 5439;
	private String databaseName = null;
	private String username = null;
	private String password = null;
	private String jdbcURL = null;
	private Map<String, String> props = new HashMap<>();
	
	public ConnInfo() {
		
	}

	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJdbcURL() {
		return jdbcURL;
	}

	public void setJdbcURL(String jdbcURL) {
		this.jdbcURL = jdbcURL;
	}

	public Map<String, String> getProps() {
		return props;
	}

	public void setProps(Map<String, String> props) {
		this.props = props;
	}
	
	@JsonIgnore
	public String getJdbcDriver() {
		return jdbcDriver;
	}
	
	@JsonIgnore
	public String getJdbcUrlTemplate() {
		return jdbcUrlTemplate;
	}
}

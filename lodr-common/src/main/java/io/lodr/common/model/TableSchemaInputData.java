package io.lodr.common.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TableSchemaInputData implements InputData {
	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "connInfo cannot be empty")
	private ConnInfo connInfo = null;
	private String schemaName = "public";
	@NotBlank(message = "table name cannot be empty.")
	private String tableName = null;
	private String webhook = null;

	public TableSchemaInputData() {
	}

	public ConnInfo getConnInfo() {
		return connInfo;
	}

	public void setConnInfo(ConnInfo connInfo) {
		this.connInfo = connInfo;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getWebhook() {
		return webhook;
	}

	public void setWebhook(String webhook) {
		this.webhook = webhook;
	}
}

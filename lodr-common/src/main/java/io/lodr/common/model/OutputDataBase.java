package io.lodr.common.model;

import io.lodr.common.util.CommonUtils;

public class OutputDataBase implements OutputData {
	private static final long serialVersionUID = 1L;

	private String error = null;
	private String errorDetails = null;
	
	public OutputDataBase() {		
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}
	
	public void setThrowable(Throwable t) {
		this.error = t.getMessage();
		this.errorDetails = CommonUtils.convertToString(t);
	}
}

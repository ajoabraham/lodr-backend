package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

public class ProcessDataOutputData extends OutputDataBase {
	private static final long serialVersionUID = 1L;

	private String jobId = null;
	private int batchId = 0;
	private long rowCount = 0;
	private String outputFileObjectKey = null;
	private List<ColumnInfo> columnInfos = new ArrayList<>();
 	
	public ProcessDataOutputData() {
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	public long getRowCount() {
		return rowCount;
	}

	public void setRowCount(long rowCount) {
		this.rowCount = rowCount;
	}

	public List<ColumnInfo> getColumnInfos() {
		return columnInfos;
	}

	public void setColumnInfos(List<ColumnInfo> columnInfos) {
		this.columnInfos = columnInfos;
	}

	public String getOutputFileObjectKey() {
		return outputFileObjectKey;
	}

	public void setOutputFileObjectKey(String outputFileObjectKey) {
		this.outputFileObjectKey = outputFileObjectKey;
	}
}

package io.lodr.common.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTyped;

import io.lodr.common.constant.DataType;

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDBDocument
public final class ColumnInfo implements Cloneable {	
	private int columnPosition = 0;
	private String columnName = null;
	private DataType dataType = null;
	private int length = 0;
	private int precision = 0;
	private int scale = 0;
	private boolean isNullValue = false;
	
	public ColumnInfo() {	
	}
	
	public ColumnInfo(String columnName, int columnPosition) {
		this(columnName, columnPosition, null);
	}
	
	public ColumnInfo(String columnName, int columnPosition, DataType dataType) {
		this.columnName = columnName;
		this.columnPosition = columnPosition;
		this.dataType = dataType;
	}
	
	@DynamoDBAttribute
	public int getColumnPosition() {
		return columnPosition;
	}

	public void setColumnPosition(int columnPosition) {
		this.columnPosition = columnPosition;
	}

	@DynamoDBAttribute
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	@DynamoDBAttribute(attributeName = "data_type")
	@DynamoDBTyped(DynamoDBAttributeType.S)
	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	@DynamoDBAttribute
	public int getLength() {		
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@DynamoDBAttribute
	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	@DynamoDBAttribute
	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}
	
	public String generateTypeSql() {
		StringBuffer sb = new StringBuffer();
		
		sb.append(dataType.getSqlTypeName());
		
		if (dataType == DataType.VARCHAR) {
			sb.append("(").append(getLength()).append(")");
		}
		else if (dataType == DataType.DECIMAL) {
			sb.append("(").append(getPrecision()).append(",")
				.append(getScale()).append(")");
		}
		
		return sb.toString();
	}
	
	@DynamoDBAttribute
	public boolean isNullValue() {
		return isNullValue;
	}

	public void setNullValue(boolean isNullValue) {
		this.isNullValue = isNullValue;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		
		if (obj == null || obj.getClass() != getClass()) {
			result = false;
		}
		else if (obj == this) {
			result = true;
		}
		else {
			ColumnInfo columnInfo = (ColumnInfo) obj;
			
			if (getColumnPosition() == columnInfo.getColumnPosition()
				&& getDataType() == columnInfo.getDataType()
				&& (getDataType() != DataType.VARCHAR || getLength() == columnInfo.getLength())
				&& ((getColumnName() != null && columnInfo.getColumnName() != null && getColumnName().equals(columnInfo.getColumnName())
					 || getColumnName() == null && columnInfo.getColumnName() == null))) {								
				if (getDataType() == DataType.DECIMAL
					&& (getPrecision() != columnInfo.getPrecision() || getScale() != columnInfo.getScale())) {
					result = false;
				}
				else {
					result = true;
				}
			}
		}
		
		return result;
	}
	
	@Override
	public int hashCode() {
		return getColumnPosition() + getDataType().hashCode() + getLength()
			+ (getColumnName() == null ? 0 : getColumnName().hashCode());
	}
	
	@Override
	public Object clone() {
		ColumnInfo columnInfo = new ColumnInfo(getColumnName(), getColumnPosition(), getDataType());
		columnInfo.setLength(getLength());
		columnInfo.setPrecision(getPrecision());
		columnInfo.setScale(getScale());
		columnInfo.setNullValue(isNullValue());
		
		return columnInfo;
	}
}

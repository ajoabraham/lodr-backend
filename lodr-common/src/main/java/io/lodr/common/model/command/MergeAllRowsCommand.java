package io.lodr.common.model.command;

import java.util.List;
import java.util.stream.Collectors;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class MergeAllRowsCommand extends Command {
	private List<RowData> allRows = null;
	
	public MergeAllRowsCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {		
		RowData firstRow = allRows.get(0);		
		ColumnData firstColumn = firstRow.getColumnDatas().get(0);
		
		String mergedValue = allRows.stream().map(r -> r.getColumnDatas().stream().filter(c -> !CommonUtils.isBlank(c.getValue())).map(ColumnData::getValue).map(String::trim).collect(Collectors.joining(","))).collect(Collectors.joining(" "));
		firstColumn.setValue(mergedValue);
		
		allRows.clear();
		firstRow.getColumnDatas().clear();
		firstRow.getColumnDatas().add(firstColumn);
		allRows.add(firstRow);
		
		return allRows;
	}

	public void setAllRows(List<RowData> allRows) {
		this.allRows = allRows;
	}
}

package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class MergeCommand extends Command {

	public MergeCommand() {
		
	}
	
	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		final String targetColumnName = getTargetColumnName();
		final String delimiter = getDelimiter();
		
		row.getColumnDatas().stream().filter(targetColumn -> targetColumnName.equals(targetColumn.getName())).findAny().ifPresent(targetColumn -> 
			row.getColumnDatas().stream().filter(sourceColumn -> columnName.equals(sourceColumn.getName())).findAny().ifPresent(sourceColumn -> {				
				boolean isSourceBlank = CommonUtils.isBlank(sourceColumn.getValue());
				boolean isTargetBlank = CommonUtils.isBlank(targetColumn.getValue());
				
				String mergedColumn = "";
				if (!isSourceBlank && !isTargetBlank) {
					mergedColumn = sourceColumn.getValue() + delimiter + targetColumn.getValue();
				}
				else if (isSourceBlank && !isTargetBlank) {
					mergedColumn = targetColumn.getValue();
				}
				else if (!isSourceBlank && isTargetBlank) {
					mergedColumn = sourceColumn.getValue();
				}
				
				sourceColumn.setValue(mergedColumn);

				
				row.getColumnDatas().remove(targetColumn);
				
				resetColumnPositions(row);
			}));
				
		return Lists.newArrayList(row);
	}
	
	String getDelimiter() {
		String delimiter = getOpt("delimiter");
		return delimiter == null ? "" : delimiter;
	}
	
	String getTargetColumnName() {
		return getOpt("target_column_name");
	}
}

package io.lodr.common.model;

public class TestConnectionOutputData extends OutputDataBase {
	private static final long serialVersionUID = 1L;

	private boolean isSucceeded = false;
	
	public TestConnectionOutputData() {
	}

	public TestConnectionOutputData(boolean isSucceeded, Throwable t) {
		this.isSucceeded = isSucceeded;
		setThrowable(t);
	}
	
	public boolean isSucceeded() {
		return isSucceeded;
	}

	public void setSucceeded(boolean isSucceeded) {
		this.isSucceeded = isSucceeded;
	}
}

package io.lodr.common.model.command;

import java.util.List;

import io.lodr.common.model.RowData;

public class DummyCommand extends Command {

	public DummyCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		return null;
	}

}

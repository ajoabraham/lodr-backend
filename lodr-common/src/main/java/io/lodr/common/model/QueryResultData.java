package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QueryResultData {
	@JsonProperty("columns")
	private List<SchemaColumn> columns = new ArrayList<>();
	@JsonProperty("data")
	private List<List<String>> data = new ArrayList<>();
	
	public QueryResultData() {		
	}

	public List<SchemaColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<SchemaColumn> columns) {
		this.columns = columns;
	}

	public List<List<String>> getData() {
		return data;
	}

	public void setData(List<List<String>> data) {
		this.data = data;
	}
}

package io.lodr.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.lodr.common.constant.UnzipState;

@JsonInclude(Include.NON_NULL)
public class PostBackUnzipStatus {
	@JsonProperty("failed_reason")
	private String failedReason = null;
	@JsonProperty("status")
	private UnzipState status = UnzipState.UNZIPPING;
	@JsonProperty("zip_files")
	private List<ZipFileInfo> zipFiles = new ArrayList<>();
	@JsonProperty("dest_access_id")
	private String destAccessId = null;
	@JsonProperty("dest_access_key")
	private String destAccessKey = null;
	@JsonProperty("dest_region")
	private String destRegion = null;
	@JsonProperty("dest_bucket_name")
	private String destBucketName = null;
	
	public PostBackUnzipStatus() {
	}
	
	public String getFailedReason() {
		return failedReason;
	}

	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}

	public UnzipState getStatus() {
		return status;
	}

	public void setStatus(UnzipState status) {
		this.status = status;
	}

	public List<ZipFileInfo> getZipFiles() {
		return zipFiles;
	}

	public void setZipFiles(List<ZipFileInfo> zipFiles) {
		this.zipFiles = zipFiles;
	}
	
	public String getDestAccessId() {
		return destAccessId;
	}

	public void setDestAccessId(String destAccessId) {
		this.destAccessId = destAccessId;
	}

	public String getDestAccessKey() {
		return destAccessKey;
	}

	public void setDestAccessKey(String destAccessKey) {
		this.destAccessKey = destAccessKey;
	}

	public String getDestRegion() {
		return destRegion;
	}

	public void setDestRegion(String destRegion) {
		this.destRegion = destRegion;
	}

	public String getDestBucketName() {
		return destBucketName;
	}

	public void setDestBucketName(String destBucketName) {
		this.destBucketName = destBucketName;
	}


	public static class ZipFileInfo {
		@JsonProperty("name")
		private String name = null;
		@JsonProperty("size")
		private long size = 0;
		@JsonProperty("object_key")
		private String objectKey = null;
		
		public ZipFileInfo() {	
		}
		
		public ZipFileInfo(String name, long size, String objectKey) {
			this.name = name;
			this.size = size;
			this.objectKey = objectKey;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public long getSize() {
			return size;
		}

		public void setSize(long size) {
			this.size = size;
		}

		public String getObjectKey() {
			return objectKey;
		}

		public void setObjectKey(String objectKey) {
			this.objectKey = objectKey;
		}
	}
}

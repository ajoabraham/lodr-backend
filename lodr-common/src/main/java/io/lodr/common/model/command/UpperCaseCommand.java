package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class UpperCaseCommand extends Command {
	public UpperCaseCommand() {
		
	}
	
	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName()) && !CommonUtils.isBlank(c.getValue())).findAny()
			.ifPresent(c -> c.setValue(c.getValue().toUpperCase()));
		return Lists.newArrayList(row);
	}

}

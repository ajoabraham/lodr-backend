package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class LowerCaseCommand extends Command {

	public LowerCaseCommand() {
		
	}
	
	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName()) && !CommonUtils.isBlank(c.getValue())).findAny()
			.ifPresent(c -> c.setValue(c.getValue().toLowerCase()));
		return Lists.newArrayList(row);
	}
}

package io.lodr.common.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatabaseSchemaInputData implements InputData {
	private static final long serialVersionUID = 1L;

	@NotNull(message = "connInfo cannot be empty")
	private ConnInfo connInfo = null;
	private String schemaName = null;
	private String webhook = null;
	
	public DatabaseSchemaInputData() {
	}

	public ConnInfo getConnInfo() {
		return connInfo;
	}

	public void setConnInfo(ConnInfo connInfo) {
		this.connInfo = connInfo;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public String getWebhook() {
		return webhook;
	}

	public void setWebhook(String webhook) {
		this.webhook = webhook;
	}
}

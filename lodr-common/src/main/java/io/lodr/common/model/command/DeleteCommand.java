package io.lodr.common.model.command;

import java.util.List;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;

public class DeleteCommand extends Command {

	public DeleteCommand() {		
	}
	
	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName())).findAny().ifPresent(c -> {
			row.getColumnDatas().remove(c);
			
			resetColumnPositions(row);
		});
		
		return Lists.newArrayList(row);
	}
}

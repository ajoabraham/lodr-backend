package io.lodr.common.model;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDBDocument
public final class FieldError implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String columnName = null;
	private int failedRows = 0;
	
	public FieldError() {		
	}

	public FieldError(String columnName) {
		this.columnName = columnName;
	}
	
	public FieldError(String columnName, int failedRows) {
		this.columnName = columnName;
		this.failedRows = failedRows;
	}

	@DynamoDBAttribute
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	@DynamoDBAttribute
	public int getFailedRows() {
		return failedRows;
	}

	public void setFailedRows(int failedRows) {
		this.failedRows = failedRows;
	}
	
	public int incrementFailedRows() {
		return ++failedRows;
	}
}

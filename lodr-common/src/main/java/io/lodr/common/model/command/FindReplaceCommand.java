package io.lodr.common.model.command;

import java.util.List;
import java.util.regex.Pattern;

import com.google.common.collect.Lists;

import io.lodr.common.model.RowData;
import io.lodr.common.util.CommonUtils;

public class FindReplaceCommand extends Command {

	public FindReplaceCommand() {
	}

	@Override
	public List<RowData> execute(RowData row) {
		final String columnName = getColumnName();
		
		row.getColumnDatas().stream().filter(c -> columnName.equals(c.getName()) && !CommonUtils.isBlank(c.getValue())).findAny()
			.ifPresent(c -> {
				final String value = c.getValue();
				final String find = getFind();
				final String replace = getReplace();
				
				StringBuffer regex = new StringBuffer();
				
				if (isCaseInsensitive()) {
					regex.append("(?i)");
				}
				
				if (isRegex()) {
					regex.append(convertToJavaRegex(find));
				}
				else {
					regex.append(Pattern.quote(find));
				}
				
				String updatedValue = "";

				if (doesReplaceAll()) {					
					updatedValue = value.replaceAll(regex.toString(), replace);					
				}
				else {
					updatedValue = value.replaceFirst(regex.toString(), replace);
				}
				
				c.setValue(updatedValue);
			});
		
		return Lists.newArrayList(row);
	}

	String getFind() {
		return getOpt("find");
	}
	
	String getReplace() {
		return getOpt("replace");
	}
	
	boolean doesReplaceAll() {
		final String replaceAll = getOpt("replace_all");
		return replaceAll == null ? true : "on".equalsIgnoreCase(replaceAll);
	}
	
	boolean isCaseInsensitive() {
		final String caseInsensitive = getOpt("case_insensitive");
		return caseInsensitive == null ? true : "on".equalsIgnoreCase(caseInsensitive);
	}
	
	boolean isRegex() {
		final String regex = getOpt("regex");
		return regex == null ? false : "on".equalsIgnoreCase(regex);
	}
}

package io.lodr.common.util;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

public enum Validator {
	I;
	
	private javax.validation.Validator validator = null;
	
	private Validator() {		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}
	
	public <T> Set<ConstraintViolation<T>> validate(T obj, Class<?> ...groups) {
		return validator.validate(obj, groups);
	}
	
	public <T> Set<ConstraintViolation<T>> validateProperty(T obj, String propertyName, Class<?> ...groups) {
		return validator.validateProperty(obj, propertyName, groups);
	}
	
	public <T> Set<ConstraintViolation<T>> validateValue(Class<T> beanType, String propertyName, Object value, Class<?> ...groups) {
		return validator.validateValue(beanType, propertyName, value, groups);
	}
	
	public <T> String validateInput(T obj, Class<?> ...groups) {
		return createMessage(validate(obj, groups));
	}
	
	public <T> String validateInputProperty(T obj, String propertyName, Class<?> ...groups) {
		return createMessage(validator.validateProperty(obj, propertyName, groups));
	}
	
	public <T> String validateInputValue(Class<T> beanType, String propertyName, Object value, Class<?> ...groups) {
		return createMessage(validator.validateValue(beanType, propertyName, value, groups));
	}
	
	private <T> String createMessage(Set<ConstraintViolation<T>> violations) {
		if (violations == null || violations.isEmpty()) {
			return null;
		}
		
		return violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(";"));
	}
}

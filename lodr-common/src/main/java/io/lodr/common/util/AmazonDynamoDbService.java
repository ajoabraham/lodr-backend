package io.lodr.common.util;

import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.google.common.collect.Lists;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.CanceledJob;
import io.lodr.common.model.CanceledQuery;
import io.lodr.common.model.ColumnInfoItem;
import io.lodr.common.model.JobStatus;

public enum AmazonDynamoDbService {
	I;
	
//	private DynamoDB dynamoDb = null;
	private DynamoDBMapper mapper = null;
	
	private AmazonDynamoDbService() {
		AmazonDynamoDBClient dynamoDbClient = new AmazonDynamoDBClient(new BasicAWSCredentials(CommonConstants.DEFAULT_ACCESS_ID, CommonConstants.DEFAULT_ACCESS_KEY));
		dynamoDbClient.setRegion(Region.getRegion(Regions.fromName(CommonConstants.DEFAULT_REGION)));
//		dynamoDb = new DynamoDB(dynamoDbClient);
		mapper = new DynamoDBMapper(dynamoDbClient);
	}
	
//	public DynamoDB getDynamoDb() {
//		return dynamoDb;
//	}
//	
//	public DynamoDBMapper getDynamoDbMapper() {
//		return dynamoDbMapper;
//	}
	
	public <T> void batchSave(List<T> objs) throws Exception {
		if (objs != null && !objs.isEmpty())
			mapper.batchWrite(objs, Lists.newArrayList(), getConfig(objs.get(0).getClass()));
	}
	
	public <T> void save(T obj) throws Exception {
		if (obj != null) {
			mapper.save(obj, getConfig(obj.getClass()));
		}
	}
	
	public <T> void delete(T obj) throws Exception {
		if (obj != null) {
			mapper.delete(obj,getConfig(obj.getClass()));
		}
	}
	
	public <T> void batchDelete(List<T> objs) throws Exception {
		if (objs != null && !objs.isEmpty()) {
			mapper.batchWrite(Lists.newArrayList(), objs, getConfig(objs.get(0).getClass()));
		}
	}
	
	public <T> T load(Class<T> clazz, String hashKey) throws Exception {
		return mapper.load(clazz, hashKey, getConfig(clazz));
	}
	
	public <T> T load(Class<T> clazz, String hashKey, int rangeKey) throws Exception {
		return mapper.load(clazz, hashKey, rangeKey, getConfig(clazz));
	}
	
	public <T> List<T> query(Class<T> clazz, DynamoDBQueryExpression<T> expression) throws Exception {
		return mapper.query(clazz, expression, getConfig(clazz));
	}
	
	public <T> List<T> scan(Class<T> clazz, DynamoDBScanExpression expression) throws Exception {
		return mapper.scan(clazz, expression, getConfig(clazz));
	}
	
	private <T> DynamoDBMapperConfig getConfig(Class<T> clazz) throws Exception {
		String tableName = "";
		
		if (clazz == JobStatus.class) {
			tableName = CommonConstants.JOB_STATUS_TABLE;
		}
		else if (clazz == ColumnInfoItem.class) {
			tableName = CommonConstants.COLUMN_INFO_TABLE;
		}
		else if (clazz == CanceledJob.class) {
			tableName = CommonConstants.CANCELED_JOB_TABLE;
		}
		else if (clazz == CanceledQuery.class) {
			tableName = CommonConstants.CANCELED_QUERY_TABLE;
		}
		else {
			throw new Exception("Unsupported DynamoDB table - " + clazz);
		}
		
		return DynamoDBMapperConfig.builder().withTableNameOverride(TableNameOverride.withTableNameReplacement(tableName)).build();
	}
}

package io.lodr.common.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import io.lodr.common.constant.LoadMode;
import io.lodr.common.model.RunJobInputData;

public class ValidateLoadModeValidator
		implements ConstraintValidator<ValidateLoadMode, RunJobInputData> {

	@Override
	public void initialize(ValidateLoadMode constraintAnnotation) {
	}

	@Override
	public boolean isValid(RunJobInputData input, ConstraintValidatorContext context) {
		if (input != null && input.getLoadMode() == LoadMode.MERGE) {
			return input.getMergeKeys() != null && !input.getMergeKeys().isEmpty();
		}
		return true;
	}
}

package io.lodr.common.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

import io.lodr.common.constant.CSVDefaultSettings;
import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.TextDelimiter;
import io.lodr.common.constant.ZipFormat;

public final class CommonUtils {
	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class.getName());
	
	private CommonUtils() {		
	}
		
	public static Charset detectEncoding(byte[] source) {
		try {
			CharsetDetector detector = new CharsetDetector();
			detector.setText(source);
			CharsetMatch match = detector.detect();

			if (match != null) {
				return Charset.forName(match.getName());
			}
		}
		catch (Exception e) {
		}

		return Charset.defaultCharset();
	}
	
	private static final String NEWLINE = "[\r]?\n";
	public static char detectDelimiter(final String source, final boolean isCsv) {
		char c;
		boolean inQuotes = false;
		LineDelimiters delimiters = new LineDelimiters();

		for (String line : source.split(NEWLINE)) {
			// ignore comments and empty lines
			if (isBlank(line) || line.trim().charAt(0) == CSVDefaultSettings.COMMENT_MARKER_CHAR) { 
				continue;
			}
			else {
				for (int i = 0; i < line.length(); ++i) {
					c = line.charAt(i);
					switch (c) {
    					case ',':
    					case ';':
    					case '|':
    					case ' ':
    					case '\t': {
    						if (!inQuotes) {
    							delimiters.increment(c);
    						}
    						break;
    					}
    					case '"': {
    						inQuotes = !inQuotes;
    						break;
    					}
					}
				}
			}
		}

		char delimiter = delimiters.max();
		if (isCsv && delimiter == TextDelimiter.SPACE.charValue()) {
			delimiter = delimiters.maxWithoutSpace();
		}

		return delimiter;
	}

	public static char detectQuoteCharacter(final String source, final boolean isCsv) {
		char quotingCharacter = '"';

		char c;
		int singleQuotes = 0;
		int doubleQuotes = 0;
		for (String line : source.split(NEWLINE)) {
			// ignore comments and empty lines
			if (isBlank(line) || line.trim().charAt(0) == CSVDefaultSettings.COMMENT_MARKER_CHAR) { 
				continue;
			}
			else {
    			for (int i = 0; i < line.length(); ++i) {
    				c = line.charAt(i);
    				switch (c) {
        				case '"': {
        					doubleQuotes++;
        					break;
        				}
        				case '\'': {
        					singleQuotes++;
        					break;
        				}
    				}
    			}
			}
		}

		if (singleQuotes == 0 && doubleQuotes == 0) {
			quotingCharacter = '\0';
		}
		else if (singleQuotes > doubleQuotes && (singleQuotes % 2 == 0)) {
			quotingCharacter = '\'';
		}

		// TH 03/09/2016, if this is a CSV file and quote character did not
		// detect, just use double
		// quote as default.
		if (isCsv && quotingCharacter == '\0') {
			quotingCharacter = '"';
		}

		return quotingCharacter;
	}
	
	private static final int BUFFER_SIZE = 4096;
	public static void unzipFile(File basePath, String fileName) throws Exception {
		InputStream is = CommonUtils.class.getResourceAsStream(fileName);
		if (is == null)
			throw new Exception("Cannot find defaultProject.zip in classpath");

		try (ZipInputStream zis = new ZipInputStream(is)) {
			ZipEntry entry = zis.getNextEntry();

			while (entry != null) {
				File file = new File(basePath, entry.getName());
				if (!entry.isDirectory()) {
					try (FileOutputStream fos = new FileOutputStream(file);
							BufferedOutputStream bos = new BufferedOutputStream(fos)) {
						byte[] bytesIn = new byte[BUFFER_SIZE];
						int read;
						while ((read = zis.read(bytesIn)) != -1) {
							bos.write(bytesIn, 0, read);
						}
					}
				}
				else {
					file.mkdir();
				}

				zis.closeEntry();
				entry = zis.getNextEntry();
			}
		}
	}
	
	public static boolean isBlank(String str) {
		return str == null || str.trim().equals("");
	}
	
	public static void createErrorMsg(Map<String, Object> output, Exception e) {
		output.clear();
		output.put("error", e.getMessage());
	}
	
	public static boolean isNumeric(String str) {
		if (!isBlank(str) && str.trim().length() > 1 && str.startsWith("0") && !str.contains(".")) {
			return false;
		}

		return NumberUtils.isParsable(str);
	}
	
	public static int getStringLength(String str) {
		return str.getBytes(CommonConstants.DEFAULT_ENCODING).length;
	}
	
	public static String truncateString(String str, int size) {
		byte[] bytes = str.getBytes(CommonConstants.DEFAULT_ENCODING);
		if (size >= bytes.length) return str;
		
		byte[] newBytes = new byte[size];
		for (int i = 0; i < newBytes.length; i++) {
			newBytes[i] = bytes[i];
		}
		
		return new String(newBytes, CommonConstants.DEFAULT_ENCODING);
	}
	
	public static boolean isDate(String input) {
		return parseDate(input, CommonConstants.DATE_FORMATS) != null;
	}

	public static boolean isTimestamp(String input) {
		return parseDate(input, CommonConstants.TIMESTAMP_FORMATS) != null;
	}
	
	public static Date parseDate(String input, String... parsePatterns) {
		Date parsedDate = null;
		
		try {
			parsedDate = DateUtils.parseDateStrictly(input, parsePatterns);
		}
		catch (Exception e) {
			try {
				parsedDate = DateUtils.parseDate(input, parsePatterns);
			}
			catch (Exception ex) {				
			}
		}
		
		return parsedDate;
	}
	
	public static Date parseDate(String input) { 
		Date parsedDate = null;
		
		if ((parsedDate = parseDate(input, CommonConstants.DATE_FORMATS)) == null) {
			parsedDate = parseDate(input, CommonConstants.TIMESTAMP_FORMATS);	
		}
				
		return parsedDate;
	}
		
	public static boolean containsPassword(Throwable e, String userName, String password) {
		if (!isBlank(e.getMessage())) {
			String message = e.getMessage().toLowerCase();
			if (message.contains("password")) {
				return true;
			}

			if (userName != null && message.contains(userName)) {
				return true;
			}

			if (password != null && message.contains(password)) {
				return true;
			}
		}

		if (e.getCause() != null) {
			return containsPassword(e.getCause(), userName, password);
		}

		return false;
	}
	
	public static String sanitizeMessage(Throwable e, String userName, String password) {
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));

		String message = writer.toString();

		if (!isBlank(userName)) {
			message = message.replace(userName, generateMask(userName.length()));
		}

		if (!isBlank(password)) {
			message = message.replace(password, generateMask(password.length()));
		}

		return message;
	}

	private static String generateMask(int length) {
		return StringUtils.leftPad("", length, "*");
	}
	
	public static boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	public static String convertToDefaultDateFormat(String input) {
		Date date = parseDate(input, CommonConstants.DATE_FORMATS);
		return date == null ? input : (new SimpleDateFormat(CommonConstants.DEFAULT_DATE_FORMAT).format(date));
	}

	public static String convertToDefaultTimestampFormat(String input) {
		Date date = parseDate(input, CommonConstants.TIMESTAMP_FORMATS);
		return date == null ? input : (new SimpleDateFormat(CommonConstants.DEFAULT_TIMESTAMP_FORMAT).format(date));
	}
		
	public static String convertToString(Throwable t) {
		StringWriter writer = new StringWriter();
		t.printStackTrace(new PrintWriter(writer));
		return writer.toString();
	}
	
	public static String createOutputFileObjectKey(String objectKey, String filePrefix, int batchId) {
		return new StringBuffer().append(objectKey).append(filePrefix).append("_").append(batchId).append(".csv.gzip").toString();
	}
	
	public static String getUniqueColumnName(List<String> columnNames, String columnName) {
		int suffix = 0;		
		String uniqueColumnName = columnName;
		
		while (columnNames.contains(uniqueColumnName)) {
			uniqueColumnName = columnName + "_" + suffix++;
		}
		
		return uniqueColumnName;
	}
	
	public static String[] getPathAndFileName(String objectKey) {
		if (isBlank(objectKey)) return null;
		
		String[] pathAndFileName = {"", ""};
		
		int index = objectKey.lastIndexOf(CommonConstants.DEFAULT_S3_DELIMITER);
		if (index == -1) {
			pathAndFileName[1] = objectKey;
		}
		else {
			pathAndFileName[0] = objectKey.substring(0, index);
			pathAndFileName[1] = objectKey.substring(index + 1);
		}
		
		return pathAndFileName;
	}
	
	public static String getSourceFileName(String bucketName, String objectKey) {
		return CommonConstants.DEFAULT_BUCKET_NAMES.contains(bucketName) ? getPathAndFileName(objectKey)[1] : objectKey;
	}
	
	public static boolean isZipFile(String fileName) {
		if (isBlank(fileName)) return false;
		
		fileName = fileName.toLowerCase();
		
		for (ZipFormat zipFormat : ZipFormat.values()) {
			if (fileName.endsWith(zipFormat.getFileExtension())) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isJsonFile(String objectKey) {
		String fileName = getPathAndFileName(objectKey)[1];
		
		boolean isJsonFile = false;
		
		int index = fileName.lastIndexOf(".");
		
		if (index < (fileName.length() - 1)) {
			String fileExtension = fileName.substring(index + 1, fileName.length());
			isJsonFile = fileExtension.toLowerCase().contains("json");
		}
		
		return isJsonFile;
	}
	
	public static boolean isHandledFileType(String fileName) {
		if (isBlank(fileName)) return false;
				
		if (StringUtils.endsWithAny(fileName.trim().toLowerCase(), ".xls", ".xlt", ".xlm", ".xlsx", ".xltx", ".xltm")) {
			return false;
		}
		
		return true;
	}
	
	public static String getDefaultUploadObjectKeyPrefix(String userId) {
		return userId + CommonConstants.DEFAULT_S3_DELIMITER 
				+ CommonConstants.DEFAULT_UPLOADS_FOLDER 
				+ CommonConstants.DEFAULT_S3_DELIMITER;
					
	}
	
	public static InputStream getInputStream(String objectKey, InputStream s3In) throws IOException {
		InputStream in = null;
		String inputFileName = objectKey.toLowerCase();
		
		if (inputFileName.endsWith(ZipFormat.ZIP.getFileExtension())) {
			ZipInputStream zipIn = new ZipInputStream(s3In);
			
			ZipEntry e = null;
			while ((e = zipIn.getNextEntry()) != null) {
				if (!e.isDirectory() && CommonUtils.isHandledFileType(e.getName())) {
					in = zipIn;
					break;
				}
			}
			
			if (in == null) {
				logger.error("Compressed file {} does not contain a valid file.", objectKey);
				in = s3In;
			}
		}
		else if (inputFileName.endsWith(ZipFormat.GZ.getFileExtension()) || inputFileName.endsWith(ZipFormat.GZIP.getFileExtension())) {
			in = new GZIPInputStream(s3In);
		}
		else if (inputFileName.endsWith(ZipFormat.BZ2.getFileExtension()) || inputFileName.endsWith(ZipFormat.BZIP2.getFileExtension())) {
			in = new BZip2CompressorInputStream(s3In);
		}
		else {
			in = s3In;
		}
		
		return in;
	}
		
	private static final class Delimiter {
		private final char delimiter;
		private int count;

		Delimiter(char delimiter) {
			this.delimiter = delimiter;
		}

		public static Delimiter newInstance(char delimiter) {
			return new Delimiter(delimiter);
		}

		public int increment() {
			this.count++;
			return this.count;
		}

		public int count() {
			return this.count;
		}

		public char delimiter() {
			return this.delimiter;
		}

		@Override
		public String toString() {
			return "{" + "delimiter='" + delimiter + "', count=" + count + "}";
		}
	}

	private static final class LineDelimiters {
		final Map<Character, Delimiter> delimiters = new HashMap<>();

		public void increment(final char c) {
			Delimiter delimiter = delimiters.get(c);
			if (delimiter == null) {
				delimiter = Delimiter.newInstance(c);
			}
			delimiter.increment();
			delimiters.put(c, delimiter);
		}

		public char max() {
			Optional<Delimiter> max = delimiters.values().stream().max(
					(delimiter1, delimiter2) -> Integer.compare(delimiter1.count(), delimiter2.count()));

			if (max.isPresent()) {
				return max.get().delimiter();
			}
			else {
				return '\0';
			}
		}

		public char maxWithoutSpace() {
			Optional<Delimiter> max = delimiters.values().stream().filter(d -> d.delimiter() != ' ').max(
					(delimiter1, delimiter2) -> Integer.compare(delimiter1.count(), delimiter2.count()));

			if (max.isPresent()) {
				return max.get().delimiter();
			}
			else {
				return '\0';
			}
		}
	}
}

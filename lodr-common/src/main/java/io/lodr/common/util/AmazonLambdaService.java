package io.lodr.common.util;

import static io.lodr.common.constant.CommonConstants.DEFAULT_ACCESS_ID;
import static io.lodr.common.constant.CommonConstants.DEFAULT_ACCESS_KEY;
import static io.lodr.common.constant.CommonConstants.DEFAULT_REGION;

import java.util.concurrent.Future;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.PredefinedClientConfigurations;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.AWSLambdaAsyncClient;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;

import io.lodr.common.model.InputData;
import io.lodr.common.util.JsonUtils;

public enum AmazonLambdaService {
	I;

	private AWSLambdaAsyncClient lambda = null;

	private AmazonLambdaService() {
		ClientConfiguration clientConfiguration = PredefinedClientConfigurations.defaultConfig();
		clientConfiguration.setSocketTimeout(330000);

		lambda = (AWSLambdaAsyncClient) AWSLambdaAsyncClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(DEFAULT_ACCESS_ID, DEFAULT_ACCESS_KEY)))
				.withClientConfiguration(clientConfiguration).withRegion(DEFAULT_REGION).build();
	}

	public <T> T getLambdaFunction(Class<T> funcProxyClass) {
		return LambdaInvokerFactory.builder().lambdaClient(lambda).build(funcProxyClass);
	}
	
	public Future<InvokeResult> invokeAsync(String functionName, String alias, InputData inputData) throws Exception {
		InvokeRequest req = new InvokeRequest().withFunctionName(functionName)
				.withInvocationType(InvocationType.Event)
				.withQualifier(alias)
				.withPayload(JsonUtils.toJsonString(inputData));
		
		return lambda.invokeAsync(req);
	}
	
	public InvokeResult invoke(String functionName, String alias, InputData inputData) throws Exception {
		InvokeRequest req = new InvokeRequest().withFunctionName(functionName)
				.withInvocationType(InvocationType.RequestResponse)
				.withQualifier(alias)
				.withPayload(JsonUtils.toJsonString(inputData));
		
		return lambda.invoke(req);
	}
}

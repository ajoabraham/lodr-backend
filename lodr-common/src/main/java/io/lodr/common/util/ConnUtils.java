package io.lodr.common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import io.lodr.common.model.ConnInfo;
import io.lodr.common.util.CommonUtils;

/**
 * 
 * @author Tai Hu
 *
 *         Centralized place to acquire database connections
 */
public final class ConnUtils {

	private ConnUtils() {
	}

	public static Connection getConnection(ConnInfo connInfo) throws SQLException {
		try {
			String dbUrl = getDbConnectionUrl(connInfo);

			Connection connection = null;

			Class.forName(connInfo.getJdbcDriver());

			Properties props = new Properties();
			if (!CommonUtils.isBlank(connInfo.getUsername()))
				props.put("UID", connInfo.getUsername());
			if (!CommonUtils.isBlank(connInfo.getPassword()))
				props.put("PWD", connInfo.getPassword());

			if (connInfo.getProps() != null && !connInfo.getProps().isEmpty()) {
				props.putAll(connInfo.getProps());
			}

			connection = DriverManager.getConnection(dbUrl, props);

			return connection;
		}
		catch (SQLException | ClassNotFoundException e) {
			if (CommonUtils.containsPassword(e, connInfo.getUsername(),
					connInfo.getPassword())) {
				String sanitizedMessage = CommonUtils.sanitizeMessage(e,
						connInfo.getUsername(), connInfo.getPassword());
				throw new SQLException(sanitizedMessage);
			}
			else {
				throw new SQLException(buildClearDBErrorMessage(e));
			}
		}
	}
	
	private static final String TEST_QUERY = "SELECT 1+1";
	public static boolean testConnection(ConnInfo connInfo) throws SQLException {
		try (Connection connection = getConnection(connInfo)) {
			String query = TEST_QUERY;
			connection.createStatement().execute(query);

			return true;
		}
		catch (Exception e) {
			if (CommonUtils.containsPassword(e, connInfo.getUsername(),
					connInfo.getPassword())) {
				String sanitizedMessage = CommonUtils.sanitizeMessage(e,
						connInfo.getUsername(), connInfo.getPassword());
				throw new SQLException(sanitizedMessage);
			}
			else {
				throw new SQLException(buildClearDBErrorMessage(e));
			}
		}
	}

	private static String buildClearDBErrorMessage(Exception e) {
		String msg = "Failed to connect to Database. Possible Reasons:";
		if (e.getCause() != null) {
			if (e.getCause().getCause() != null) {
				msg += "\n\t"
						+ e.getCause().getCause().getClass().getSimpleName()
						+ ": " + e.getCause().getCause().getMessage();
				if (e.getCause().getCause().getCause() != null) {
					msg += "\n\t"
							+ e.getCause().getCause().getCause().getClass().getSimpleName()
							+ ": "
							+ e.getCause().getCause().getCause().getMessage();
				}
			}
			msg += "\n\t" + e.getCause().getLocalizedMessage();
		}
		msg += "\n\t" + e.getLocalizedMessage();
		return msg;
	}

	private static String getDbConnectionUrl(ConnInfo connInfo) {
		return CommonUtils.isBlank(connInfo.getJdbcURL()) ?
				String.format(connInfo.getJdbcUrlTemplate(), connInfo.getServerAddress(), 
						connInfo.getPort(), connInfo.getDatabaseName()) 
				: connInfo.getJdbcURL();
	}
}

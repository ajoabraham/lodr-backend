package io.lodr.common.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.csv.CSVPrinter;

public class CSVFileGenerator {
	private int numberOfLines = 3000000;
	private int numberOfColumns = 50;
	private int numberOfDateColumns = 5;
	private int numberOfIntegerColumns = 5;
	private int numberOfDecimalColumns = 3;
	private int numberOfBooleanColumns = 3;
//	private int maxWidthOfTextColumn = 256;	
	private String outputCSVFileName = null;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public CSVFileGenerator(String fileName, int numberOfLines, int numberOfColumns, int numberOfDateColumns, int numberOfIntegerColumns,
			int numberOfDecimalColumns, int numberOfBooleanColumns) {
		this.outputCSVFileName = fileName;
		this.numberOfLines = numberOfLines;
		this.numberOfColumns = numberOfColumns;
		this.numberOfDateColumns = numberOfDateColumns;
		this.numberOfIntegerColumns = numberOfIntegerColumns;
		this.numberOfDecimalColumns = numberOfDecimalColumns;
		this.numberOfBooleanColumns = numberOfBooleanColumns;
	}
	
	public void generateCSVFile() throws FileNotFoundException, UnsupportedEncodingException, IOException {
		if (numberOfColumns < (numberOfDateColumns + numberOfIntegerColumns + numberOfDecimalColumns + numberOfBooleanColumns)) 
			throw new IOException("Too much date, integer, decimal, and boolean columns");
		
		try (CSVPrinter out = CSVUtils.createCSVPrinter(new PrintWriter(outputCSVFileName, StandardCharsets.UTF_8.name()), "|", "\"")) {
			// Print out header
			List<String> columnNames = new ArrayList<>();
			for (int i = 0; i < numberOfColumns; i++) {
				columnNames.add("COLUMN_" + i);
			}
			
			out.printRecord(columnNames);
			Random random = new Random();
			for (int i = 0; i < numberOfLines; i++) {
				List<Object> rowData = new ArrayList<>();
				for (int j = 0; j < numberOfIntegerColumns; j++) {
					rowData.add(random.nextInt(100000000));
				}
				
				for (int j = 0; j < numberOfDateColumns; j++) {
					rowData.add(dateFormat.format(new Date(System.currentTimeMillis() - random.nextInt(100000000))));
				}
				
				int randomFactor = random.nextInt(100000000);
				for (int j = 0; j < numberOfDecimalColumns; j++) {
					rowData.add(random.nextFloat() * randomFactor);
				}
				
				for (int j = 0; j < numberOfBooleanColumns; j++) {
					rowData.add(random.nextBoolean());
				}
				
				for (int j = 0; j < numberOfColumns - numberOfIntegerColumns - numberOfDateColumns - numberOfDecimalColumns - numberOfBooleanColumns; j++) {
					rowData.add("Hello World; Hello World: Hello World");
				}
				
				out.printRecord(rowData);
			}
		}
	}
	
	public int getNumberOfLines() {
		return numberOfLines;
	}

	public void setNumberOfLines(int numberOfLines) {
		this.numberOfLines = numberOfLines;
	}

	public int getNumberOfColumns() {
		return numberOfColumns;
	}

	public void setNumberOfColumns(int numberOfColumns) {
		this.numberOfColumns = numberOfColumns;
	}

	public int getNumberOfDateColumns() {
		return numberOfDateColumns;
	}

	public void setNumberOfDateColumns(int numberOfDateColumns) {
		this.numberOfDateColumns = numberOfDateColumns;
	}

	public int getNumberOfIntegerColumns() {
		return numberOfIntegerColumns;
	}

	public void setNumberOfIntegerColumns(int numberOfIntegerColumns) {
		this.numberOfIntegerColumns = numberOfIntegerColumns;
	}
	
	public int getNumberOfDecimalColumns() {
		return numberOfDecimalColumns;
	}

	public void setNumberOfDecimalColumns(int numberOfDecimalColumns) {
		this.numberOfDecimalColumns = numberOfDecimalColumns;
	}

	public int getNumberOfBooleanColumns() {
		return numberOfBooleanColumns;
	}

	public void setNumberOfBooleanColumns(int numberOfBooleanColumns) {
		this.numberOfBooleanColumns = numberOfBooleanColumns;
	}

	public String getOutputCSVFileName() {
		return outputCSVFileName;
	}

	public void setOutputCSVFileName(String outputCSVFileName) {
		this.outputCSVFileName = outputCSVFileName;
	}

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException {
		if (args.length != 7) {
			System.err.println("Usage: java CSVFileGenerator <output filename> <# of lines> <# of columns> <# of datetime columns> <# of integer columns> "
					+ "<# of decimal columns> <# of boolean columns>");
			System.exit(0);
		}
		
		long startTime = System.currentTimeMillis();
		CSVFileGenerator generator = new CSVFileGenerator(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]),
				Integer.parseInt(args[5]), Integer.parseInt(args[6]));
		generator.generateCSVFile();
		System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to generate a file with " + generator.getNumberOfLines() 
				+ " lines and " + generator.getNumberOfColumns() + " columns");
	}
}

package io.lodr.common.util;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lodr.common.constant.CSVDefaultSettings;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.DbUtils;

public final class CSVUtils {
	private static final Logger logger = LoggerFactory.getLogger(CSVUtils.class);
	
	private CSVUtils() {
		
	}
	
	// Create a base CSVFormat
	public static CSVFormat createCSVFormat(String delimiter, String quote) {
		delimiter = (delimiter == null || delimiter.equals("") || CSVDefaultSettings.NO_DELIMITER.equals(delimiter)) ? String.valueOf('\0') : delimiter;
		
		CSVFormat csvFormat = CommonUtils.isBlank(quote) ? CSVFormat.newFormat(delimiter.charAt(0))
				: CSVFormat.newFormat(delimiter.charAt(0)).withQuote(quote.charAt(0));
		return csvFormat.withRecordSeparator(CSVDefaultSettings.RECORD_SEPARATOR)
				.withCommentMarker(CSVDefaultSettings.COMMENT_MARKER_CHAR)
				.withIgnoreEmptyLines(true);
	}
			
	public static List<String> getColumnNames(String source, CSVFormat csvFormat, boolean includesHeader) throws IOException {
		List<String> columnNames = new ArrayList<>();
		
		if (source == null) return columnNames;
				
		try (CSVParser parser = CSVParser.parse(source, csvFormat)) {
			Iterator<CSVRecord> it = parser.iterator();
			
			if (it.hasNext()) {
				CSVRecord headerRecord = it.next();
				
				if (includesHeader) {
    				List<String> originalColumnNames = new ArrayList<>();
    				for (int i = 0; i < headerRecord.size(); i++) {
    					originalColumnNames.add(headerRecord.get(i).trim());
    				}
    				
    				columnNames.addAll(DbUtils.updateColumnNames(originalColumnNames));
				}
				else {
					for (int i = 0; i < headerRecord.size(); i++) {
						columnNames.add("COLUMN_" + i);
					}
				}
			}
		}
		
		return columnNames;
	}

	// Read given number of rows from input for preview purpose.
	public static List<List<String>> getPreviewData(String source, List<String> columnNames, CSVFormat csvFormat, int rowCount, boolean includesHeader) throws IOException {
		List<List<String>> previewData = new ArrayList<>();
				
		// If includesHeader is true, just skip the first line. Otherwise, do not skip the first line
		try (CSVParser parser = CSVParser.parse(source, 
				includesHeader ? csvFormat.withHeader(columnNames.toArray(new String[0])).withSkipHeaderRecord()
						: csvFormat.withHeader(columnNames.toArray(new String[0])))) {
			Iterator<CSVRecord> it = parser.iterator();
			
			int count = 0;
			while (count++ < rowCount) {
				try {
					CSVRecord r = it.next();
					List<String> values = new ArrayList<>();

					for (String columnName : columnNames) {
						String value = "";
						try {
							value = r.get(columnName);
						}
						catch (Exception e) {							
						}
						
						values.add(value);
					}
					
					previewData.add(values);
				}
				catch (NoSuchElementException e) {
					// End of input
					break;
				}
				catch (Exception e) {
					logger.info("In getPreviewData(), line {} is invalid and skipped", parser.getCurrentLineNumber());
				}
			}
		}
		
		return previewData;
	}
	
	public static CSVPrinter createCSVPrinter(Writer writer, String delimiter, String quote) throws IOException {
		return createCSVFormat(delimiter, quote).withRecordSeparator(System.lineSeparator()).print(writer);
	}
}

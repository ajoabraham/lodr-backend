package io.lodr.common.util;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import io.lodr.common.model.FileInfo;
import io.lodr.common.model.RunJobInputData;

public class ValidateFileInfosValidator implements ConstraintValidator<ValidateFileInfos, RunJobInputData> {

	@Override
	public void initialize(ValidateFileInfos constraintAnnotation) {
	}

	@Override
	public boolean isValid(RunJobInputData input, ConstraintValidatorContext context) {
		List<FileInfo> fileInfos = input.getFileInfos();
		
		if (fileInfos.isEmpty()) return false;
		
		if (fileInfos.size() > 1) {		
    		final FileInfo fileInfo = input.getFileInfos().get(0);
    		final String accessId = fileInfo.getAccessId();
    		final String accessKey = fileInfo.getAccessKey();
    		final String region = fileInfo.getRegion();
    		final String bucketName = fileInfo.getBucketName();
    		final String userId = fileInfo.getUserId();
    		
    		for (int i = 1; i < fileInfos.size(); i++) {
    			FileInfo f = fileInfos.get(i);
    			
    			if (StringUtils.compare(accessId, f.getAccessId()) != 0) {
    				return false;
    			}
    			
    			if (StringUtils.compare(accessKey, f.getAccessKey()) != 0) {
    				return false;
    			}
    			
    			if (StringUtils.compare(region, f.getRegion()) != 0) {
    				return false;
    			}
    			
    			if (StringUtils.compare(bucketName, f.getBucketName()) != 0) {
    				return false;
    			}
    			
    			if (StringUtils.compare(userId, f.getUserId()) != 0) {
    				return false;
    			}
    		}
		}
		
		return true;
	}
}

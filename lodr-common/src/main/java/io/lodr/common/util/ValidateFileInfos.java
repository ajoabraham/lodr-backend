package io.lodr.common.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidateFileInfosValidator.class })
@Documented
public @interface ValidateFileInfos {
	String message() default "All files should come from same region and bucket with same S3 credentials";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}

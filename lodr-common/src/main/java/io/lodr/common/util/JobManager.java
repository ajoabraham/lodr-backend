package io.lodr.common.util;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import io.lodr.common.constant.CommonConstants;

public enum JobManager {
	I;
	
	private ExecutorService threadPool = null;
	
	private JobManager() {		
		threadPool = Executors.newFixedThreadPool(CommonConstants.DEFAULT_THREAD_POOL_SIZE);
	}
	
	public <T> Future<T> execute(Callable<T> job) {
		return threadPool.submit(job);
	}
	
	public void shutdown() {
		threadPool.shutdown();
	}
}

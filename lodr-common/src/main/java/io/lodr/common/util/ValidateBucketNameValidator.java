package io.lodr.common.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import io.lodr.common.model.InputDataBase;
import io.lodr.common.model.RunJobInputData;

public class ValidateBucketNameValidator implements ConstraintValidator<ValidateBucketName, InputDataBase> {

	@Override
	public void initialize(ValidateBucketName constraintAnnotation) {
	}

	@Override
	public boolean isValid(InputDataBase input, ConstraintValidatorContext context) {
		if (!(input instanceof RunJobInputData)) {
			return !CommonUtils.isBlank(input.getBucketName());
		}
		
		return true;
	}
}

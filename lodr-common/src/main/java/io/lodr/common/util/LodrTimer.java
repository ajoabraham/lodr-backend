package io.lodr.common.util;

import java.util.HashMap;
import java.util.Map;

public final class LodrTimer {
	private String timerName = "";
	private long startTime = -1;
	private long endTime = -1;
	private Map<String, Long> childStartTimes = new HashMap<>();
	private Map<String, Long> childEndTimes = new HashMap<>();
	
	private LodrTimer(String name) {		
		this.timerName = name;
	}
	
	public static LodrTimer newInstance() {
		return newInstance("");
	}
	
	public static LodrTimer newInstance(String name) {
		return new LodrTimer(name);
	}
	
	public void setName(String name) {
		this.timerName = name;
	}
	
	public String getName() {
		return timerName;
	}
	
	// Start total time timer
	public void start() {
		startTime = System.currentTimeMillis();
	}
	
	public void stop() {
		endTime = System.currentTimeMillis();
	}
	
	public void startChild(String timerName) {
		childStartTimes.put(timerName, System.currentTimeMillis());
	}
	
	public void stopChild(String timerName) {
		childEndTimes.put(timerName, System.currentTimeMillis());
	}
	
	public long getDuration() {
		return (endTime == -1 ? System.currentTimeMillis() : endTime) - startTime; 
	}
	
	public String getDurationDesc() {
		return "It took total " + getDuration() + "ms for " + ("".equals(timerName) ? "this process." : timerName);
	}
	
	public long getChildDuration(String timerName) {
		return (childEndTimes.containsKey(timerName) ? childEndTimes.get(timerName) : System.currentTimeMillis())
				- (childStartTimes.containsKey(timerName) ? childStartTimes.get(timerName) : 0);
	}
	
	public String getChildDurationDesc(String timerName) {
		return "It took " + getChildDuration(timerName) + "ms for " + timerName;
	}
}

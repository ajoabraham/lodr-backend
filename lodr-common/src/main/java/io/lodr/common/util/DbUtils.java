package io.lodr.common.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import io.lodr.common.model.ColumnInfo;

public final class DbUtils {
	private static int MAX_COL_LENGTH = 127;
	private static String QUOTE_STRING = "\"";
	
	private DbUtils() {		
	}
	
	public static String escapeForCopyCommand(String input) {
		// Remove new line characters
		String output = input.replaceAll("\\r\\n|\\r|\\n", " ");
		if (output.trim().startsWith("'") || output.trim().endsWith("'")) {
			output = StringUtils.wrap(output, "\"");
		}

		return output;
	}
	
	public static String getStageTableName(String tableName) {
		return tableName + "_stage";
	}

	public static String getTempColumnName(String columnName) {
		return columnName + "_ldrtemp";
	}
	
	private static final String TABLE_EXISTS_QUERY = "select count(distinct tablename) from pg_table_def where "
			+ "schemaname = '%s' and tablename = '%s'";
	public static boolean doesTableExist(Connection conn, String tableName) {
		boolean tableExists = false;
		
		String[] parts = tableName.split("\\.");
		
		String tn = null;
		String sn = "public";
		
		if (parts.length == 1) {
			tn = parts[0];
		}
		else if (parts.length > 1) {
			sn = parts[0];
			tn = parts[1];
		}
					
		String query = String.format(TABLE_EXISTS_QUERY, sn.toLowerCase(), tn.toLowerCase());
		try (Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(query)) {
			if (resultSet.next()) {
				tableExists = resultSet.getInt(1) > 0;
			}
		}
		catch (Exception e) {
		}

		return tableExists;
	}
	
	private static String RESERVED_WORDS = "";
	private static String VALID_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789_$";
	private static String INVALID_FIRST_CHARS = "0123456789$";
	private static String REPLACEMENT_CHAR = "_";
	public static String normalizeColumnName(String name) {
		// Contain only ASCII letters, digits, underscore characters (_), or
		// dollar signs ($).
		// Begin with an alphabetic character or underscore character.
		// Subsequent characters may include letters, digits, underscores, or
		// dollar signs.
		// Be between 1 and 127 characters in length, not including quotes for
		// delimited identifiers.
		// Contain no quotation marks and no spaces.
		// Not be a reserved SQL key word
		StringBuffer sb = new StringBuffer();
		// To lowercase and remove all extra whitespaces.
		name = StringUtils.normalizeSpace(name.toLowerCase());
		for (char c : name.toCharArray()) {
			if (VALID_CHARS.contains(String.valueOf(c))) {
				if (sb.length() == 0 && INVALID_FIRST_CHARS.contains(String.valueOf(c))) {
					// TH 10/30/2016, since later we will trim leading and trailing underscore,
					// do not add a underscore, instead adding a character d
//					sb.append(REPLACEMENT_CHAR);
					sb.append("d").append(c);
				}
				else {
					sb.append(c);
				}
			}
			else {
				sb.append(REPLACEMENT_CHAR);
			}
		}

		String normalizedName = sb.toString();
		// Remove leading and trailing underscore and multiple underscores
		normalizedName = StringUtils.replacePattern(normalizedName, "_{2,}", REPLACEMENT_CHAR);
		normalizedName = StringUtils.strip(normalizedName, REPLACEMENT_CHAR);

		if (RESERVED_WORDS.contains(normalizedName)) {
			normalizedName = REPLACEMENT_CHAR + normalizedName;
		}

		return normalizedName.length() > MAX_COL_LENGTH ? normalizedName.substring(0, MAX_COL_LENGTH) : normalizedName;
	}
	
	// Make sure there is no duplicated or empty header name
	public static List<String> updateColumnNames(List<String> columnNames) {
		List<String> updatedColumnNames = new ArrayList<>();
		String columnNamePrefix = "COLUMN_";
		int index = 0;
		
		for (String columnName : columnNames) {
			if (CommonUtils.isBlank(columnName)) {
				String name = columnNamePrefix + index++;
				while (updatedColumnNames.contains(name)) {
					name = columnNamePrefix + index++;
				}
				
				updatedColumnNames.add(name);
			}
			else {
				String name = columnName;
				while (updatedColumnNames.contains(name)) {
					name = columnName + "_" + index++;
				}
				
				updatedColumnNames.add(name);
			}
		}
		
		return updatedColumnNames;
	}
	
	// Normalize all column name and make sure there is no duplicates
	public static List<ColumnInfo> normalizeColumnNames(List<ColumnInfo> columnInfos) {
		List<String> normalizedColumnNames = columnInfos.stream().map(ci -> normalizeColumnName(ci.getColumnName())).collect(Collectors.toList());
		normalizedColumnNames = updateColumnNames(normalizedColumnNames);
		
		List<ColumnInfo> normalizedColumnInfos = new ArrayList<>();
		for (int i = 0; i < columnInfos.size(); i++) {
			ColumnInfo normalizedColumnInfo = (ColumnInfo) columnInfos.get(i).clone();
			normalizedColumnInfo.setColumnName(normalizedColumnNames.get(i));
			normalizedColumnInfos.add(normalizedColumnInfo);
		}
		
		return normalizedColumnInfos;
	}
	
	public static String appendLimitClause(String query, int limit) {
		// If query already has limit clause, do nothing
		if (CommonUtils.isBlank(query)) return query;
		
		String[] splitted = query.split("\\s+");
		
		String modifiedQuery = query;
		
		if (splitted.length > 2 && !"limit".equalsIgnoreCase(splitted[splitted.length - 2])) {
			modifiedQuery = query + " limit " + limit;
		}
		
		return modifiedQuery;
	}
	
	public static String quote(String ident) {
		int lastIndex = ident.lastIndexOf(".");
		
		if (lastIndex == -1) {
			return QUOTE_STRING.concat(ident).concat(QUOTE_STRING);
		}
		else {
			return QUOTE_STRING.concat(ident.substring(0, lastIndex))
					.concat(QUOTE_STRING).concat(".").concat(QUOTE_STRING)
					.concat(ident.substring(lastIndex + 1)).concat(QUOTE_STRING);
		}
	}
	
	// Find all user's non empty schemas exclude public, pg_catalog and information_schema
	private static final String USER_SCHEMA_QUERY = "select distinct table_schema from information_schema.columns where table_schema not in ('information_schema', 'pg_catalog', 'public')";
	public static List<String> getUserSchemas(Connection conn) {
		List<String> userSchemas = new ArrayList<>();
		
		try (Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(USER_SCHEMA_QUERY)) {
			while (resultSet.next()) {
				userSchemas.add(resultSet.getString(1));
			}
		}
		catch (Exception e) {
		}
		
		return userSchemas;
	}
	
	private static final String SET_SEARCH_PATH_QUERY = "set search_path to '$user', 'public', %s";
	public static void setSearchPath(Connection conn, List<String> userSchemas) {
		if (userSchemas == null || userSchemas.isEmpty()) return;
		
		String query = String.format(SET_SEARCH_PATH_QUERY, userSchemas.stream().map(s -> "'" + s + "'").collect(Collectors.joining(",")));
		
		try (Statement statement = conn.createStatement()) {
			statement.execute(query);
		}
		catch (Exception e) {
		}
	}
}
 
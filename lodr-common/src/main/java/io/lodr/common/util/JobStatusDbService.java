package io.lodr.common.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.CanceledJob;
import io.lodr.common.model.CanceledQuery;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.ColumnInfoItem;
import io.lodr.common.model.JobStatus;

public final class JobStatusDbService {
	private static final Logger logger = LoggerFactory.getLogger(JobStatusDbService.class);
	
	private static final AmazonDynamoDbService dynamo = AmazonDynamoDbService.I;
	
	private JobStatusDbService() {
	}
	
	public static <T> void save(List<T> jobs) throws Exception {
		dynamo.batchSave(jobs);
	}
	
	public static <T> void save(T job) throws Exception {
		dynamo.save(job);
	}
	
	public static <T> void delete(T job) throws Exception{
		dynamo.delete(job);
	}
	
	public static void saveJobStatusWithColumnInfos(JobStatus job) throws Exception {
		save(job);
		
		if (job.getColumnInfos() != null && !job.getColumnInfos().isEmpty()) {
    		ColumnInfoItem columnInfoItem = new ColumnInfoItem();
    		columnInfoItem.setJobId(job.getJobId());
    		columnInfoItem.setBatchId(job.getBatchId());
    		columnInfoItem.setColumnInfos(job.getColumnInfos());
    		save(columnInfoItem);
		}
	}
	
	public static JobStatus find(String jobId, int batchId) throws Exception {
		return dynamo.load(JobStatus.class, jobId, batchId);
	}
	
	public static List<ColumnInfo> findColumnInfos(String jobId, int batchId) throws Exception {
		ColumnInfoItem item = dynamo.load(ColumnInfoItem.class, jobId, batchId);
		return item == null ? new ArrayList<>() : item.getColumnInfos();
	}
	
	public static List<JobStatus> findAll(String jobId) throws Exception {
		Map<String, AttributeValue> params = new HashMap<>();
		params.put(":jobId", new AttributeValue().withS(jobId));
		DynamoDBQueryExpression<JobStatus> expression = new DynamoDBQueryExpression<JobStatus>()
				.withKeyConditionExpression("jobId = :jobId")
				.withExpressionAttributeValues(params);
		
		return dynamo.query(JobStatus.class, expression);
	}
	
	public static void cancelJob(String jobId) throws Exception {
		CanceledJob canceledJob = new CanceledJob(jobId);		
		save(canceledJob);	
	}
	
	public static void cancelQuery(String queryId) throws Exception {
		CanceledQuery canceledQuery = new CanceledQuery(queryId);		
		save(canceledQuery);	
	}
	
	public static boolean isCanceledJob(String jobId) {
		boolean isCanceled = false;
		
		try {
			CanceledJob canceledJob = dynamo.load(CanceledJob.class, jobId);
			if (canceledJob != null) {
				isCanceled = true;
			}
		}
		catch (Exception e) {
		}
		
		return isCanceled;
	}
	
	public static boolean isCanceledQuery(String queryId) {
		boolean isCanceled = false;
		
		try {
			CanceledQuery canceledQuery = dynamo.load(CanceledQuery.class, queryId);
			if (canceledQuery != null) {
				isCanceled = true;
			}
		}
		catch (Exception e) {
		}
		
		return isCanceled;
	}
	
	// Clean data older than given number of hours
	public static void cleanData(int olderThan) throws Exception {		
		Date hoursAgo = new Date();
		hoursAgo.setTime(new Date().getTime() - (60L * 60L * 1000L * olderThan));
		SimpleDateFormat dateFormatter = new SimpleDateFormat(CommonConstants.DEFAULT_DYNAMODB_DATE_FORMAT);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String hoursAgoStr = dateFormatter.format(hoursAgo);

        Map<String, AttributeValue> values = new HashMap<>();
        values.put(":val1", new AttributeValue().withS(hoursAgoStr));
        
        long startTime = System.currentTimeMillis();
        DynamoDBScanExpression jobStatusScan = new DynamoDBScanExpression()
        		.withFilterExpression("startTime < :val1")
        		.withExpressionAttributeValues(values);
        List<JobStatus> oldJobStatuses = dynamo.scan(JobStatus.class, jobStatusScan);
        dynamo.batchDelete(oldJobStatuses);        
        logger.info("Deleted {} old items from JobStatus in {} ms", oldJobStatuses.size(), (System.currentTimeMillis() - startTime));
        
        startTime = System.currentTimeMillis();
        DynamoDBScanExpression columnInfoScan = new DynamoDBScanExpression()
        		.withFilterExpression("createdOn < :val1")
        		.withExpressionAttributeValues(values);
        List<ColumnInfoItem> oldColumnInfos = dynamo.scan(ColumnInfoItem.class, columnInfoScan);
        dynamo.batchDelete(oldColumnInfos);        
        logger.info("Deleted {} old items from ColumnInfo in {} ms", oldColumnInfos.size(), (System.currentTimeMillis() - startTime));
        
        startTime = System.currentTimeMillis();
        DynamoDBScanExpression canceledJobScan = new DynamoDBScanExpression()
        		.withFilterExpression("canceledOn < :val1")
        		.withExpressionAttributeValues(values);
        List<CanceledJob> oldCanceledJobs = dynamo.scan(CanceledJob.class, canceledJobScan);
        dynamo.batchDelete(oldCanceledJobs);        
        logger.info("Deleted {} old items from CanceledJob in {} ms", oldCanceledJobs.size(), (System.currentTimeMillis() - startTime));
        
        startTime = System.currentTimeMillis();
        DynamoDBScanExpression canceledQueryScan = new DynamoDBScanExpression()
        		.withFilterExpression("canceledOn < :val1")
        		.withExpressionAttributeValues(values);
        List<CanceledQuery> oldCanceledQueries = dynamo.scan(CanceledQuery.class, canceledQueryScan);
        dynamo.batchDelete(oldCanceledQueries);        
        logger.info("Deleted {} old items from CanceledQuery in {} ms", oldCanceledQueries.size(), (System.currentTimeMillis() - startTime));
	}
}

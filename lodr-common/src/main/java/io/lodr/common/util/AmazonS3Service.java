package io.lodr.common.util;

import static io.lodr.common.constant.CommonConstants.DEFAULT_ACCESS_ID;
import static io.lodr.common.constant.CommonConstants.DEFAULT_ACCESS_KEY;
import static io.lodr.common.constant.CommonConstants.DEFAULT_REGION;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;

import io.lodr.common.constant.CommonConstants;

public final class AmazonS3Service implements Closeable {
	private static final Logger logger = LoggerFactory.getLogger(AmazonS3Service.class);
	
	private AmazonS3Client s3 = null;
	private TransferManager tm = null;
	
	private AmazonS3Service(String accessKeyId, String accessKey, String regionName) {
		s3 = new AmazonS3Client(new BasicAWSCredentials(
				CommonUtils.isBlank(accessKeyId) ? DEFAULT_ACCESS_ID : accessKeyId, 
						CommonUtils.isBlank(accessKey) ? DEFAULT_ACCESS_KEY : accessKey));
		s3.setRegion(com.amazonaws.regions.Region.getRegion(Regions.fromName(CommonUtils.isBlank(regionName) ? DEFAULT_REGION : regionName)));
		
		tm = TransferManagerBuilder.standard().withS3Client(s3).build();
	}

	public static AmazonS3Service newInstance(String accessKeyId, String accessKey) {
		return new AmazonS3Service(accessKeyId, accessKey, null);
	}
	
	public static AmazonS3Service newInstance(String accessKeyId, String accessKey, String regionName) {
		return new AmazonS3Service(accessKeyId, accessKey, regionName);
	}
	
	public List<String> getBucketNames() {
		List<String> bucketNames = new ArrayList<>();
		
		try {
			for (Bucket bucket : s3.listBuckets()) {
				bucketNames.add(bucket.getName());
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return bucketNames;
	}
	
	public static List<String> getRegionNames() {
		List<String> regionNames = new ArrayList<>();
		
		for (Regions region : Regions.values()) {
			regionNames.add(region.getName());
		}
		
		return regionNames;
	}
	
	public void uploadFile(String bucketName, String objectKey, File file) throws Exception {
		final PutObjectRequest putReq = new PutObjectRequest(bucketName, objectKey, file); 
		if (file.length() < CommonConstants.MULTIPART_UPLOAD_THRESHOLD) {
			s3.putObject(putReq);
		}
		else {
			Upload upload = tm.upload(putReq);			
			upload.waitForCompletion();
		}
	}
	
	public void deleteObject(String bucketName, String objectKey) throws Exception {
		s3.deleteObject(bucketName, objectKey);
	}
	
	public void deleteObjects(String bucketName, List<String> objectKeys) {
		for (String objectKey : objectKeys) {
			try {
				deleteObject(bucketName, objectKey);
				logger.info("Successfully deleted {} from S3", objectKey);
			}
			catch (Exception e) {
				logger.info("Failed to delete {} from S3", objectKey);
			}
		}
	}
	
	public S3Object getObject(String bucketName, String objectKey) throws Exception {
		return getObject(new GetObjectRequest(bucketName, objectKey));
	}
	
	public S3Object getObject(GetObjectRequest request) throws Exception {
		return s3.getObject(request);
	}
	
	public ObjectMetadata getObjectMetadata(String bucketName, String objectKey) throws Exception {
		return s3.getObjectMetadata(bucketName, objectKey);
	}
	
	// Find the latest object in the bucket
	public String getLatestObjectKey(String bucketName, String path, String filePrefix) {
		String objectKeyPrefix = CommonUtils.isBlank(path) ? filePrefix : path + CommonConstants.DEFAULT_S3_DELIMITER + filePrefix;
		ListObjectsRequest request = new ListObjectsRequest();
		request.setBucketName(bucketName);
		request.setDelimiter(CommonConstants.DEFAULT_S3_DELIMITER);
		request.setPrefix(objectKeyPrefix);
		ObjectListing objListing = s3.listObjects(request);
		
		S3ObjectSummary latestObj = null;
		boolean done = false;
		
		while (!done) {			
			for (S3ObjectSummary obj : objListing.getObjectSummaries()) {
				if (latestObj == null || latestObj.getLastModified().before(obj.getLastModified())) {
					latestObj = obj;
				}
			}
			
			if (objListing.isTruncated()) {
				objListing = s3.listNextBatchOfObjects(objListing);
			}
			else {
				done = true;
			}
		}
		
		return latestObj == null ? null : latestObj.getKey();
	}
	
	public boolean hasWritePermission(String bucketName) {
		boolean hasWritePermission = false;
		String key = "testWrite.txt";
		
		try {
			s3.putObject(bucketName, key, UUID.randomUUID().toString());
			hasWritePermission = true;
		}
		catch (Exception e) {			
		}
		finally {
			try {
				s3.deleteObject(bucketName, key);
			}
			catch(Exception e) {				
			}
		}
		
		return hasWritePermission;
	}

	@Override
	public void close() throws IOException {
		if (tm != null) {
			tm.shutdownNow();
		}
		
		if (s3 != null) {
			s3.shutdown();
		}
	}
}

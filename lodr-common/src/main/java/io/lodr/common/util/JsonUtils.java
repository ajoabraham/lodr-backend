package io.lodr.common.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.jayway.jsonpath.JsonPath;

public final class JsonUtils {
	private static final ObjectMapper objectMapper = new ObjectMapper();

	private JsonUtils() {
	}

	public static <T> T toObject(TypeReference<T> type, JsonNode jsonNode) throws JsonParseException,
			JsonMappingException, IOException {
		return toObject(type, toJsonString(jsonNode));
	}

	public static <T> T toObject(TypeReference<T> type, String jsonString) throws JsonParseException,
			JsonMappingException, IOException {
		if (jsonString == null || jsonString.trim().equals(""))
			return null;

		return objectMapper.readValue(jsonString, type);
	}

	public static String toJsonString(JsonNode node) throws JsonProcessingException {
		Object value = objectMapper.treeToValue(node, Object.class);
		return objectMapper.writeValueAsString(value);
	}

	public static String toJsonString(Object obj) throws JsonProcessingException {
		return objectMapper.writeValueAsString(obj);
	}
	
	public static String toJsonString(Object obj, TypeReference<?> type) throws JsonProcessingException {
		return objectMapper.writerFor(type).writeValueAsString(obj);
	}

	public static JsonNode toJsonNode(Object obj) throws IOException {
		String jsonString = toJsonString(obj);
		return toJsonNode(jsonString);
	}

	public static JsonNode toJsonNode(String jsonString) throws JsonProcessingException, IOException {
		return objectMapper.readTree(jsonString);
	}
	
	public static Object jsonPath(String jsonString, String path) {
		Object result = null;
		try {
			result = JsonPath.parse(jsonString).read(path.trim());
		}
		catch (Exception e) {
			result= Lists.newArrayList();
		}
		
		return result;
	}
}

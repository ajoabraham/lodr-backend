package io.lodr.common.util;

import static io.lodr.common.util.DbUtils.*;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.stream.Collectors;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.DataType;
import io.lodr.common.constant.RedshiftDistStyle;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.publisher.PublisherException;

public final class SqlGenerator {

	private SqlGenerator() {
	}
	
	public static String createTable(String tableName, List<ColumnInfo> columnInfos, RedshiftDistStyle distStyle, String distKey, List<String> sortKeys) {
		return generateTableDdl(tableName, columnInfos, distStyle, distKey, sortKeys, false);
	}
	
	public static String createTempTable(String tempTableName, List<ColumnInfo> columnInfos, RedshiftDistStyle distStyle, String distKey, List<String> sortKeys) {
		return generateTableDdl(tempTableName, columnInfos, distStyle, distKey, sortKeys, true);
	}
	
	private static String generateTableDdl(String tableName, List<ColumnInfo> columnInfos, RedshiftDistStyle distStyle, String distKey, List<String> sortKeys, boolean isTempTable) {
		StringBuffer sb = new StringBuffer();
		
		sb.append(isTempTable ? "create temp table " : "create table ").append(quote(tableName)).append(" (").append(System.lineSeparator());
		
		for (int i  = 0; i < columnInfos.size(); i++) {
			ColumnInfo columnInfo = columnInfos.get(i);
			sb.append(quote(columnInfo.getColumnName())).append("    ").append(columnInfo.generateTypeSql());
			
			if (i != (columnInfos.size() - 1)) {
				sb.append(",").append(System.lineSeparator());
			}
		}
		
		sb.append(")");
		genRedshiftKeys(sb, distStyle, distKey, sortKeys);
		
		return sb.toString();
	}
	
	private static void genRedshiftKeys(StringBuffer sb, RedshiftDistStyle distStyle, String distKey, List<String> sortKeys) {
		sb.append(" DISTSTYLE ").append(distStyle == null ? RedshiftDistStyle.EVEN.toString() : distStyle.toString());
		if (!CommonUtils.isBlank(distKey)) {
			sb.append(" DISTKEY (").append(quote(DbUtils.normalizeColumnName(distKey))).append(")");
		}

		if (sortKeys != null && !sortKeys.isEmpty()) {
			sb.append(" SORTKEY (").append(sortKeys.stream().map(k -> quote(DbUtils.normalizeColumnName(k))).collect(Collectors.joining(","))).append(")");
		}
	}
	
	private static final String DROP_TABLE_QUERY = "drop table if exists %s";
	public static String dropTable(String tableName) {
		return String.format(DROP_TABLE_QUERY, quote(tableName));
	}
	
	public static String createPrimaryKeys(String tableName, List<String> primaryKeys) {
		StringBuffer sb = new StringBuffer();

		sb.append("ALTER TABLE ").append(quote(tableName)).append(" ADD PRIMARY KEY (")
			.append(primaryKeys.stream().map(k -> quote(DbUtils.normalizeColumnName(k))).collect(Collectors.joining(","))).append(")");
		
		return sb.toString();
	}
	
	public static String updateColumnType(Statement statement, String tableName, ColumnInfo typeInfo)
			throws PublisherException, SQLException {
		StringBuffer sqls = new StringBuffer();
		String tempColumnName = DbUtils.getTempColumnName(typeInfo.getColumnName());

		// Add a temp column with latest type
		try{
			// drop column if the temp version already exists
			String dropColumnQuery = dropColumn(tableName, tempColumnName);
			statement.execute(dropColumnQuery);
		}catch(Exception e){
			// do nothing
		}
		String addColumnQuery = addColumn(tableName, tempColumnName, typeInfo.generateTypeSql());
//		String addColumnQuery = "alter table " + tableName + " add column " + tempColumnName + " "
//				+ typeInfo.generateTypeSql() + " default NULL";
		statement.execute(addColumnQuery);
		sqls.append(addColumnQuery).append(System.lineSeparator());

		// Copy old column into new column
		String copyQuery = "update " + quote(tableName) + " set " + quote(tempColumnName) + "=" + quote(typeInfo.getColumnName());
		// coercion to string when going into varchar
		if(typeInfo.getDataType() == DataType.VARCHAR){
			copyQuery = "update " + quote(tableName) + " set " + quote(tempColumnName) + "=" + quote(typeInfo.getColumnName()) + "|| ''";
		}
		statement.execute(copyQuery);
		sqls.append(copyQuery).append(System.lineSeparator());

		// Drop old column
		String dropColumnQuery = "alter table " + quote(tableName) + " drop column " + quote(typeInfo.getColumnName());
		statement.execute(dropColumnQuery);
		sqls.append(dropColumnQuery).append(System.lineSeparator());

		// Rename new column into old column name
		String renameColumnQuery = "alter table " + quote(tableName) + " rename column " + quote(tempColumnName) + " to "
				+ quote(typeInfo.getColumnName());
		statement.execute(renameColumnQuery);
		sqls.append(renameColumnQuery);

		return sqls.toString();
	}
	
	private static final String COPY_COMMAND_BASE = "copy %s from 's3://%s' credentials 'aws_access_key_id=%s;aws_secret_access_key=%s' gzip removequotes";
	public static String copyCommand(String tableName, String objectPath, String region, String accessId, String accessKey) {
		String copyCommand = String.format(COPY_COMMAND_BASE, quote(tableName), objectPath, 
				CommonUtils.isBlank(accessId) ? CommonConstants.DEFAULT_ACCESS_ID : accessId, 
				CommonUtils.isBlank(accessKey) ? CommonConstants.DEFAULT_ACCESS_KEY : accessKey);

		if (!CommonUtils.isBlank(region)) {
			copyCommand += " region '" + region + "'";
		}
		
		return copyCommand;
	}
	
	public static String upsertDelete(String tableName, String stageTableName, List<String> mergeKeys) {
		StringBuilder sb = new StringBuilder("delete from ");
		sb.append(quote(tableName)).append(" using ").append(quote(stageTableName)).append(" where ");

		int i = 0;
		for (String key : mergeKeys) {
			sb.append(quote(tableName)).append(".").append(quote(key))
			  .append("=")
			  .append(quote(stageTableName)).append(".").append(quote(key));

			if (i != mergeKeys.size() - 1) {
				sb.append(" and ");
			}

			i++;
		}

		return sb.toString();
	}
	
	private static final String UPSERT_INSERT_QUERY = "insert into %s (%s) select %s from %s";
	public static String upsertInsert(String tableName, List<ColumnInfo> columnInfos, String stageTableName) {
		String columnNames = columnInfos.stream().map(c -> quote(c.getColumnName())).collect(Collectors.joining(","));
		return String.format(UPSERT_INSERT_QUERY, quote(tableName), columnNames, columnNames, quote(stageTableName));
	}
	
	private static final String ADD_COLUMN_QUERY = "alter table %s add column %s %s default NULL";
	public static String addColumn(String tableName, String columnName, String typeSql) {
		return String.format(ADD_COLUMN_QUERY, quote(tableName), quote(columnName), typeSql);
	}
	
	private static final String DROP_COLUMN_QUERY = "alter table %s drop column %s";
	public static String dropColumn(String tableName, String columnName) {
		return String.format(DROP_COLUMN_QUERY, quote(tableName), quote(columnName));
	}
	
	private static final String DATA_TYPE_INFO_QUERY = "SELECT * FROM %s WHERE 1 = 2";
	public static String getDataTypeInfos(String tableName) {
		return String.format(DATA_TYPE_INFO_QUERY, quote(tableName));
	}
}

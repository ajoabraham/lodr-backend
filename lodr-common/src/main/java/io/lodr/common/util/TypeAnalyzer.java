package io.lodr.common.util;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.DataType;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.util.CommonUtils;

public final class TypeAnalyzer {	
	private static final Logger logger = LoggerFactory.getLogger(TypeAnalyzer.class);
	
	private static final List<String> BOOLEAN_VALUES = Arrays.asList("true", "false", "t", "f", "yes", "no", "y", "n", "1", "0");
	
	// Record currently identified type information
	private ColumnInfo columnInfo = null;
	private String columnName = null;
	private int columnPosition = 0;
	
	private long numOfValidated = 0;
	private long numOfValid = 0;
	private long numOfInvalid = 0;
	private long numOfBlank = 0;
	
	private DataType userDefinedDataType = null;
	
	public TypeAnalyzer(String columnName, int columnPosition) {
		this(columnName, columnPosition, null);
	}
	
	public TypeAnalyzer(String columnName, int columnPosition, DataType columnType) {
		this.columnName = columnName;
		this.columnPosition = columnPosition;
		this.userDefinedDataType = columnType;
	}

	public boolean analyze(String value) {
		boolean isValid = true;
		numOfValidated++;
		
		// Determine given value's type
		// If user defined data type is null, then just determine type based on value.
		// Otherwise, update the type information based on given type.
		
		if (CommonUtils.isBlank(value)) {
			numOfBlank++;
			return isValid;
		}
		
		if (userDefinedDataType == null) {
    		// This is the first time,
    		if (columnInfo == null) {
    			columnInfo = determineDataTypeInfo(value);
    		}
    		else {
    			updateDataTypeInfo(columnInfo, value);
    		}
		}
		else {
			if (columnInfo == null) {
				columnInfo = new ColumnInfo(columnName, columnPosition, userDefinedDataType);
			}
			
			isValid = adjustColumnInfo(columnInfo, value);
			if (isValid) {
				numOfValid++;
			}
			else {
				numOfInvalid++;
			}
		}
		
		return isValid;
	}
	
	public ColumnInfo getColumnInfo() {
		ColumnInfo ci = null;
		
		if (columnInfo == null) {
			ColumnInfo defaultType = new ColumnInfo();
			defaultType.setColumnName(columnName);
			defaultType.setColumnPosition(columnPosition);
			defaultType.setDataType(userDefinedDataType == null ? DataType.VARCHAR : userDefinedDataType);
			defaultType.setLength(CommonConstants.DEFAULT_VARCHAR_LENGTH);
			defaultType.setNullValue(true);
						
			ci = defaultType;
		}
		else {
			ci = columnInfo;
		}
		
		if (ci.getDataType() == DataType.DECIMAL && ci.getScale() <= 0) {
			ci.setScale(2);
			ci.setPrecision(ci.getPrecision() + 2);
			
			if (ci.getPrecision() <= 2) {
				ci.setPrecision(10);
			}
		}
		
		return ci;
	}
		
	public void clearValidationStats() {
		numOfValidated = 0;
		numOfValid = 0;
		numOfInvalid = 0;
		numOfBlank = 0;
	}
	
	public long getNumOfValidated() {
		return numOfValidated;
	}
	
	public long getNumOfValid() {
		return numOfValid;
	}
	
	public long getNumOfInvalid() {
		return numOfInvalid;
	}
	
	public long getNumOfBlank() {
		return numOfBlank;
	}
	
	public void printStats() {
		logger.info("Final Data Type : {}", columnInfo.getDataType());
		logger.info("Length: {}", columnInfo.getLength());
		if (columnInfo.getDataType() == DataType.DECIMAL) {
			logger.info("Precision: {} Scale: {}", columnInfo.getPrecision(), columnInfo.getScale());
		}
		logger.info("==================================================");
	}
	
	// Determine the data type based on given String value.
	private ColumnInfo determineDataTypeInfo(String value) {
		ColumnInfo typeInfo = new ColumnInfo(columnName, columnPosition);
		typeInfo.setLength(CommonUtils.getStringLength(value));
		
		if (CommonUtils.isNumeric(value)) {
			if (value.contains(".")) {
				typeInfo.setDataType(DataType.DECIMAL);
				String[] parts = value.split("\\.");
				typeInfo.setPrecision(parts[0].length() + parts[1].length());
				typeInfo.setScale(parts[1].length());
			}
			else {
				try {
    				long v = Long.parseLong(value);
    				
    				if (v > CommonConstants.MAX_INT || v < CommonConstants.MIN_INT) {
    					typeInfo.setDataType(DataType.BIGINT);
    				}
    				else {
    					typeInfo.setDataType(DataType.INTEGER);
    				}
				}
				catch (NumberFormatException e) {
					// Number is too big to parse.
					typeInfo.setDataType(DataType.BIGINT);
				}
			}
		}
		else if (BOOLEAN_VALUES.contains(value.trim().toLowerCase())) {
			typeInfo.setDataType(DataType.BOOLEAN);
		}
		else if (CommonUtils.isDate(value)) {
			typeInfo.setDataType(DataType.DATE);
		}
		else if (CommonUtils.isTimestamp(value)) {
			typeInfo.setDataType(DataType.TIMESTAMP);
		}
		else {
			typeInfo.setDataType(DataType.VARCHAR);
		}
			
		adjustVarcharSize(typeInfo);
		
		return typeInfo;
	}
	
	// Update given dataTypeInfo based on current value
	private void updateDataTypeInfo(ColumnInfo typeInfo, String value) {
		if (CommonUtils.isBlank(value)) {
			return;
		}

		int previousLength = typeInfo.getLength();
		
		int length = CommonUtils.getStringLength(value);
		if (length > previousLength) {
			typeInfo.setLength(length);
		}

		if (typeInfo.getDataType() == DataType.VARCHAR) {
			if (getOriginalVarcharSize(previousLength) < length) {
				columnInfo.setLength(length);
				adjustVarcharSize(columnInfo);
			}
		}
		else if (typeInfo.getDataType() == DataType.INTEGER) {
			if (CommonUtils.isNumeric(value)) {
				if (value.contains(".")) {
					typeInfo.setDataType(DataType.DECIMAL);
					String[] parts = value.split("\\.");
					typeInfo.setPrecision(parts[0].length() + parts[1].length());
					typeInfo.setScale(parts[1].length());
				}
				else {
					try {
	    				long v = Long.parseLong(value);
	    				
	    				if (v > CommonConstants.MAX_INT || v < CommonConstants.MIN_INT) {
	    					typeInfo.setDataType(DataType.BIGINT);
	    				}
					}
					catch (NumberFormatException e) {
						// Number is too big to parse.
						typeInfo.setDataType(DataType.BIGINT);
					}
				}
			}
			else {
				columnInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.BIGINT) {
			if (CommonUtils.isNumeric(value)) {
				if (value.contains(".")) {
					typeInfo.setDataType(DataType.DECIMAL);
					String[] parts = value.split("\\.");
					typeInfo.setPrecision(parts[0].length() + parts[1].length());
					typeInfo.setScale(parts[1].length());
				}
			}
			else {
				columnInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.DECIMAL) {			
			if (CommonUtils.isNumeric(value)) {
				adjustDecimalType(typeInfo, value);
			}
			else {				
				columnInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.BOOLEAN) {
			if (!BOOLEAN_VALUES.contains(value.trim().toLowerCase())) {
				typeInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.DATE) {
			if (!CommonUtils.isDate(value)) {
				typeInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.TIMESTAMP) {
			if (!CommonUtils.isTimestamp(value)) {
				typeInfo.setDataType(DataType.VARCHAR);
			}
		}
	}
	
	private boolean adjustColumnInfo(ColumnInfo columnInfo, String value) {
		// Adjust dataTypeInfo based on the current value. The data type will never change. If the given value is not
		// valid for given type, just return false.
		
		if (CommonUtils.isBlank(value)) {
			return true;
		}
		
		int previousLength = columnInfo.getLength();
		int length = CommonUtils.getStringLength(value);
		
		if (previousLength < length) {
			columnInfo.setLength(length);
		}
		
		if (columnInfo.getDataType() == DataType.VARCHAR) {
			if (getOriginalVarcharSize(previousLength) < length) {
				columnInfo.setLength(length);
				adjustVarcharSize(columnInfo);
			}
		}
		else if (columnInfo.getDataType() == DataType.INTEGER) {
			if (!CommonUtils.isInteger(value)) {
				try {
					Long.parseLong(value);
					columnInfo.setDataType(DataType.BIGINT);
				}
				catch (Exception e) {
					return false;
				}
			}
		}
		else if (columnInfo.getDataType() == DataType.BIGINT) {
			try {
				Long.parseLong(value);
			}
			catch (Exception e) {
				return false;
			}
		}
		else if (columnInfo.getDataType() == DataType.BOOLEAN) {
			if (!BOOLEAN_VALUES.contains(value.trim().toLowerCase())) {
				return false;
			}
		}
		else if (columnInfo.getDataType() == DataType.DECIMAL) {
			if (!CommonUtils.isNumeric(value)) {
				return false;
			}
			
			adjustDecimalType(columnInfo, value);
		}
		else if (columnInfo.getDataType() == DataType.DATE) {
			if (!CommonUtils.isDate(value)) {
				return false;
			}
		}
		else if (columnInfo.getDataType() == DataType.TIMESTAMP) {
			if (!CommonUtils.isTimestamp(value)) {
				return false;
			}
		}
				
		return true;
	}
	
	private void adjustDecimalType(ColumnInfo typeInfo, String value) {
		if (value.contains(".")) {
			String[] parts = value.split("\\.");
			int leftSize = NumberUtils.max(parts[0].length(), typeInfo.getPrecision() - typeInfo.getScale());
			int scale = NumberUtils.max(parts[1].length(), typeInfo.getScale());
			typeInfo.setPrecision(leftSize + scale);
			typeInfo.setScale(scale);
		}
		else {
			int leftSize = NumberUtils.max(value.length(), typeInfo.getPrecision() - typeInfo.getScale());
			typeInfo.setPrecision(leftSize + typeInfo.getScale());
		}
	}
	
	// This method should always return a new DataTypeInfo object.
	public static ColumnInfo mergeDataTypeInfo(ColumnInfo t1, ColumnInfo t2) {
		ColumnInfo merged = (ColumnInfo) t1.clone();
				
		if (t2.isNullValue()) {
			// do nothing
		}
		else if (merged.getDataType() == DataType.VARCHAR) {
			// If current data type is anything other than varchar, then just ignore it.
			// Otherwise, check the length of current data type and record the longer one
			if (t2.getDataType() == DataType.VARCHAR 
					&& t2.getLength() > merged.getLength()) {
				merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.INTEGER) {
			if (t2.getDataType() == DataType.INTEGER) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else if (t2.getDataType() == DataType.BIGINT
						|| t2.getDataType() == DataType.DECIMAL) {
				merged = (ColumnInfo) t2.clone();
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.BIGINT) {
			if (t2.getDataType() == DataType.INTEGER) {
				// Ignore
			}
			else if (t2.getDataType() == DataType.BIGINT) {
				if (t2.getLength() > merged.getLength()) {
					merged.setLength(t2.getLength());
				}
			}
			else if (t2.getDataType() == DataType.DECIMAL) {
				if (merged.getLength() > (t2.getPrecision() - t2.getScale()))
					t2.setPrecision(merged.getLength() + t2.getScale());
				
				merged = (ColumnInfo) t2.clone();
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.DECIMAL) {
			if (t2.getDataType() == DataType.INTEGER
					|| t2.getDataType() == DataType.BIGINT) {
				if (t2.getLength() > (merged.getPrecision() - merged.getScale()))
					merged.setPrecision(t2.getLength() + merged.getScale());
			}
			else if (t2.getDataType() == DataType.DECIMAL) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
				
				int scale = merged.getScale() > t2.getScale() ? merged.getScale() : t2.getScale();
				int leftSize = (merged.getPrecision() - merged.getScale()) > (t2.getPrecision() - t2.getScale()) ?
						(merged.getPrecision() - merged.getScale()) : (t2.getPrecision() - t2.getScale());
				merged.setScale(scale);
				merged.setPrecision(leftSize + scale);
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.BOOLEAN) {
			if (t2.getDataType() == DataType.BOOLEAN) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.DATE) {
			if (t2.getDataType() == DataType.DATE) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.TIMESTAMP) {
			if (t2.getDataType() == DataType.TIMESTAMP) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
				
		return merged;
	}
	
	private int getOriginalVarcharSize(int length) {
		return (int) (length / (1 + CommonConstants.VARCHAR_LENGTH_FACTOR));
	}
	
	private void adjustVarcharSize(ColumnInfo typeInfo) {
		if (typeInfo.getDataType() == DataType.VARCHAR) {
			int length = typeInfo.getLength();
			if (length <= CommonConstants.DEFAULT_VARCHAR_LENGTH_THRESHOLD) {
				typeInfo.setLength(CommonConstants.DEFAULT_VARCHAR_LENGTH);
			}
			else {
				typeInfo.setLength(length + (int)(length * CommonConstants.VARCHAR_LENGTH_FACTOR));
				
				if (typeInfo.getLength() > CommonConstants.MAX_VARCHAR) {
					typeInfo.setLength(CommonConstants.MAX_VARCHAR);
				}
			}
		}
	}
	
	public DataType getUserDefinedDataType() {
		return userDefinedDataType;
	}
}

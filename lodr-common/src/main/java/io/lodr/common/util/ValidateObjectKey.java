package io.lodr.common.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidateObjectKeyValidator.class })
@Documented
public @interface ValidateObjectKey {
	String message() default "Object key cannot be empty";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}


package io.lodr.common.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidateLoadModeValidator.class })
@Documented
public @interface ValidateLoadMode {
	String message() default "Merge keys cannot be empty when load mode is merge";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}

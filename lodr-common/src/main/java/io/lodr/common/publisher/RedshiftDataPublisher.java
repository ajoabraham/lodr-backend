package io.lodr.common.publisher;

import java.sql.Statement;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lodr.common.constant.LoadMode;
import io.lodr.common.constant.RedshiftDistStyle;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.PublishResult;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.DbUtils;
import io.lodr.common.util.JobStatusDbService;
import io.lodr.common.util.SqlGenerator;

/**
 * 
 * @author Tai Hu
 * 
 *         All columnInfos passed into publisher contains normalized and deduped
 *         column names.
 *
 */
public class RedshiftDataPublisher extends DataPublisherBase {
	private static final Logger logger = LoggerFactory.getLogger(RedshiftDataPublisher.class);
	
	protected RedshiftDistStyle distStyle = null;
	protected String distKey = null;
	protected List<String> sortKeys = null;
	private List<String> outputFileObjectKeys = null;

	public RedshiftDataPublisher(RedshiftDataPublisherBuilder builder) {
		super(builder);

		distStyle = builder.distStyle;
		distKey = builder.distKey;
		sortKeys = builder.sortKeys;
		outputFileObjectKeys = builder.outputFileObjectKeys;
	}

	@Override
	public PublishResult publish() throws PublisherException {
		PublishResult result = new PublishResult();

		try (final Statement statement = conn.createStatement()) {
			Thread cancelThread = new Thread(() -> {
				try {
					boolean isDone = false;
					
					while (!isDone) {
						if (statement == null || statement.isClosed()) {
							isDone = true;
						}						
						else if (JobStatusDbService.isCanceledJob(jobId)) {
        					statement.cancel();
        					isDone = true;
        				}
						else {
							Thread.sleep(3000);
						}
					}
				}
				catch (Exception e) {					
				}
			});
			
			cancelThread.start();
			
			executeSql(statement, preSql);
			
			List<ColumnInfo> previousColumnInfos = null;
			
			if (loadMode == LoadMode.REPLACE || !DbUtils.doesTableExist(conn, tableName)) {
				createOrReplaceTable(statement, tableName, currentColumnInfos, distStyle, distKey, sortKeys);
			}
			else {
				previousColumnInfos = getPreviousColumnInfos(statement, tableName);
				currentColumnInfos = updateSchema(statement, tableName, currentColumnInfos, previousColumnInfos);
			}

			// Execute COPY command
			String objectKeyPrefix = StringUtils.getCommonPrefix(outputFileObjectKeys.toArray(new String[0]));
			String objectPath = bucketName + "/" + objectKeyPrefix;

			String stageTableName = null;
//			if (loadMode == LoadMode.MERGE || loadMode == LoadMode.APPEND) {
			if (previousColumnInfos != null) {
				stageTableName = DbUtils.getStageTableName(tableName);
//				String ddl = "create temp table " + stageTableName + " (like " + tableName + ")";
				String ddl = SqlGenerator.createTempTable(stageTableName, currentColumnInfos, distStyle, distKey, sortKeys);
				logger.info("Stage table DDL = {}", ddl);

				// create stage table
				statement.execute(ddl);
			}

			String copyCommand = SqlGenerator.copyCommand(previousColumnInfos == null ? tableName : stageTableName, 
					objectPath, region, accessId, accessKey);

			logger.info("COPY command = {}", copyCommand);

			long startTime = System.currentTimeMillis();
			statement.execute(copyCommand);
			logger.info("It took {} ms to execute COPY command for {}", (System.currentTimeMillis() - startTime),
					(previousColumnInfos == null ? tableName : stageTableName));

			if (previousColumnInfos != null) {
				int deletedRows = 0;
				if (loadMode == LoadMode.MERGE) {
					// Upsert into main table
					String deleteQuery = SqlGenerator.upsertDelete(tableName, stageTableName, mergeKeys);
					logger.info("Delete query = {}", deleteQuery);
					deletedRows = statement.executeUpdate(deleteQuery);
					logger.info("Deleted {} from {} (Rows will be updated from staging table)", deletedRows, tableName);
					result.setUpdatedRows(deletedRows);					
				}

				// Insert all rows from staging table into original table
				String insertQuery = SqlGenerator.upsertInsert(tableName, currentColumnInfos, stageTableName);
				logger.info("Insert query = {}", insertQuery);
				int insertedRows = statement.executeUpdate(insertQuery);
				conn.commit();
				result.setInsertedRows(insertedRows - deletedRows);
				logger.info("Inserted {} rows into {}", insertedRows, tableName);
				logger.info("Merged staging table with {}", tableName);
				conn.setAutoCommit(true);

				// Drop staging table
				statement.execute(SqlGenerator.dropTable(stageTableName));
				logger.info("Dropped staging table {}", stageTableName);
			}
			
			executeSql(statement, postSql);
		}
		catch (PublisherException e) {
			throw e;
		}
		catch (Exception e) {
			throw new PublisherException(e.getMessage(), e);
		}
		finally {
			// Clean up S3
			if (outputFileObjectKeys != null) {
				AmazonS3Service s3 = AmazonS3Service.newInstance(accessId, accessKey, region);
				s3.deleteObjects(bucketName, outputFileObjectKeys);
			}
		}

		return result;
	}

	public static class RedshiftDataPublisherBuilder
			extends DataPublisherBaseBuilder<RedshiftDataPublisherBuilder, RedshiftDataPublisher> {
		private RedshiftDistStyle distStyle = null;
		private String distKey = null;
		private List<String> sortKeys = null;
		private List<String> outputFileObjectKeys = null;

		private RedshiftDataPublisherBuilder() {
		}

		public static RedshiftDataPublisherBuilder create() {
			return new RedshiftDataPublisherBuilder();
		}

		public RedshiftDataPublisherBuilder setOutputFileObjectKeys(List<String> outputFileObjectKeys) {
			this.outputFileObjectKeys = outputFileObjectKeys;
			return this;
		}

		public RedshiftDataPublisherBuilder setDistStyle(RedshiftDistStyle distStyle) {
			this.distStyle = distStyle;
			return this;
		}
		
		public RedshiftDataPublisherBuilder setDistKey(String distKey) {
			this.distKey = distKey;
			return this;
		}

		public RedshiftDataPublisherBuilder setSortKeys(List<String> sortKeys) {
			this.sortKeys = sortKeys;
			return this;
		}

		@Override
		public RedshiftDataPublisher build() {
			return new RedshiftDataPublisher(this);
		}

	}
}

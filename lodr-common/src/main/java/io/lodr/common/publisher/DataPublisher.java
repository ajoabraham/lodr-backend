package io.lodr.common.publisher;

import io.lodr.common.model.PublishResult;

public interface DataPublisher {
	public PublishResult publish() throws PublisherException;
}

package io.lodr.common.publisher;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lodr.common.constant.DataType;
import io.lodr.common.constant.LoadMode;
import io.lodr.common.constant.RedshiftDistStyle;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.SqlGenerator;
import io.lodr.common.util.TypeAnalyzer;

public abstract class DataPublisherBase implements DataPublisher {	
	private static final Logger logger = LoggerFactory.getLogger(DataPublisherBase.class);
	
	protected String jobId = null;
	protected Connection conn = null;
	protected String tableName = null;
	protected List<ColumnInfo> currentColumnInfos = null;
	protected LoadMode loadMode = LoadMode.APPEND;
	protected List<String> mergeKeys = null;
	protected String accessId = null;
	protected String accessKey = null;
	protected String bucketName = null;
//	protected String objectKey = null;
	protected String region = null;
	protected List<String> primaryKeys = null;
	protected String preSql = null;
	protected String postSql = null;
	
	public DataPublisherBase(DataPublisherBaseBuilder<?, ?> builder) {
		jobId = builder.jobId;
		conn = builder.conn;
		tableName = builder.tableName;
		currentColumnInfos = builder.columnInfos;
		loadMode = builder.loadMode;
		mergeKeys = builder.mergeKeys;
		accessId = builder.accessId;
		accessKey = builder.accessKey;
		bucketName = builder.bucketName;
//		objectKey = builder.objectKey;
		region = builder.region;
		this.primaryKeys = builder.primaryKeys;
		preSql = builder.preSql;
		postSql = builder.postSql;
	}
		
	// TH 11/15/2016, this is just a temp hacker. Later this code should be refactored out and not be Redshift specific
	protected void createOrReplaceTable(Statement statement, String tableName, List<ColumnInfo> columnInfos, RedshiftDistStyle distStyle, String distKey, List<String> sortKeys) throws SQLException, PublisherException {
		// Drop table if it exists.
		String dropQuery = SqlGenerator.dropTable(tableName);
		logger.info("Drop Query = {}", dropQuery);
		statement.execute(dropQuery);

		// Create table		
		String ddl = SqlGenerator.createTable(tableName, columnInfos, distStyle, distKey, sortKeys);
		logger.info("DDL = {}", ddl);
		statement.execute(ddl);
		
		if (primaryKeys != null && !primaryKeys.isEmpty()) {			
			String primaryKeyDdl = SqlGenerator.createPrimaryKeys(tableName, primaryKeys);
			logger.info("Create Primary Keys = {}", primaryKeyDdl);
			statement.execute(primaryKeyDdl);
		}
	}
		
	protected List<ColumnInfo> updateSchema(Statement statement, String tableName, List<ColumnInfo> currentColumnInfos, List<ColumnInfo> previousColumnInfos) throws PublisherException, SQLException {
		List<ColumnInfo> columnInfos = new ArrayList<>();

		// If column size is different throw the exception and stop the process.
//		if (previousColumnInfos.size() != currentColumnInfos.size()) {
//			throw new PublisherException("Current column size is different from existing column size. (existing: "
//					+ previousColumnInfos.size() + ", current: " + currentColumnInfos.size());
//		}

		StringBuffer alterTableSqls = new StringBuffer();

		for (ColumnInfo previous : previousColumnInfos) {
			ColumnInfo current = currentColumnInfos.stream().filter(t -> previous.getColumnName().equals(t.getColumnName()))
					.findAny().orElse(null);

			if (current == null) {
//				throw new PublisherException("Column name " + previous.getColumnName() + " does not exsit in CSV file.");
				// TH 04/02/2017, this should only happen when user manually added new column into existing table.
				continue;
			}

//			if (!previous.getColumnName().equals(current.getColumnName())) {
//				throw new PublisherException("Current column name is different from existing column name. (existing: "
//						+ previous.getColumnName() + ", current: " + current.getColumnName());
//			}

//			if (previous.getColumnPosition() != current.getColumnPosition()) {
//				throw new PublisherException("Current column position is different from existing column position. (existing: "
//						+ previous.getColumnPosition() + ", current: " + current.getColumnPosition());
//			}

			ColumnInfo columnInfo = TypeAnalyzer.mergeDataTypeInfo(previous, current);

			if (previous.getDataType() == columnInfo.getDataType()) {
				if (!previous.equals(columnInfo)) {
					// This means only size (length, precision or scale) is
					// different now.
					// Based on the logic in mergeDataTypeInfo() method,
					// typeInfo always hold
					// the correct type information. Database column need to be
					// updated to that
					// size.
					alterTableSqls.append(SqlGenerator.updateColumnType(statement, tableName, columnInfo))
						.append(System.lineSeparator());
				}
			}			
			else if (previous.getDataType() == DataType.INTEGER) {
				if (columnInfo.getDataType() == DataType.BIGINT || columnInfo.getDataType() == DataType.DECIMAL
						|| columnInfo.getDataType() == DataType.VARCHAR) {
					alterTableSqls.append(SqlGenerator.updateColumnType(statement, tableName, columnInfo));
				}
			}
			else if (previous.getDataType() == DataType.BIGINT) {
				if (columnInfo.getDataType() == DataType.DECIMAL || columnInfo.getDataType() == DataType.VARCHAR) {
					alterTableSqls.append(SqlGenerator.updateColumnType(statement, tableName, columnInfo))
						.append(System.lineSeparator());
				}
			}
			else if (previous.getDataType() == DataType.DECIMAL) {
				if (columnInfo.getDataType() == DataType.VARCHAR) {
					alterTableSqls.append(SqlGenerator.updateColumnType(statement, tableName, columnInfo))
						.append(System.lineSeparator());
				}
			}
			else if (previous.getDataType() == DataType.BOOLEAN) {
				if (columnInfo.getDataType() == DataType.VARCHAR) {
					alterTableSqls.append(SqlGenerator.updateColumnType(statement, tableName, columnInfo))
						.append(System.lineSeparator());
				}
			}
			else if (previous.getDataType() == DataType.DATE) {
				if (columnInfo.getDataType() == DataType.TIMESTAMP || columnInfo.getDataType() == DataType.VARCHAR) {
					alterTableSqls.append(SqlGenerator.updateColumnType(statement, tableName, columnInfo))
						.append(System.lineSeparator());
				}
			}
			else if (previous.getDataType() == DataType.TIMESTAMP) {
				if (columnInfo.getDataType() == DataType.VARCHAR) {
					alterTableSqls.append(SqlGenerator.updateColumnType(statement, tableName, columnInfo))
						.append(System.lineSeparator());
				}
			}
			else {
				throw new PublisherException("Invalid data type change. (existing: "
						+ previous.getDataType().getSqlTypeName() + ", current: "
						+ current.getDataType().getSqlTypeName());
			}

			columnInfos.add(columnInfo);
		}
		
		if (!CommonUtils.isBlank(alterTableSqls.toString())) {
			logger.info("Alter column type queries - {}", alterTableSqls.toString());
		}
		
		List<ColumnInfo> newColumnInfos = currentColumnInfos.stream()
			.filter(current -> !previousColumnInfos.stream().filter(previous -> previous.getColumnName().equals(current.getColumnName())).findAny().isPresent())
			.collect(Collectors.toList());
		
		if (!newColumnInfos.isEmpty()) {						
			for (ColumnInfo newColumnInfo : newColumnInfos) {
				String addColumnQuery = SqlGenerator.addColumn(tableName, newColumnInfo.getColumnName(), newColumnInfo.generateTypeSql());
				logger.info("Add new column query - {}", addColumnQuery);
				statement.execute(addColumnQuery);
				columnInfos.add(newColumnInfo);
			}
		}
		
		// Since CSV file is already written to disk with currentColumnInfos' order. Sort columnInfos order to be the
		// same as currentColumnInfos
		return currentColumnInfos.stream().map(c -> columnInfos.stream().filter(ci -> ci.getColumnName().equals(c.getColumnName())).findAny().get()).collect(Collectors.toList());
	}

	// Load latest data type info from database
	protected List<ColumnInfo> getPreviousColumnInfos(Statement statement, String tableName) throws SQLException {
		List<ColumnInfo> columnInfos = new ArrayList<>();
		ResultSet resultSet = statement.executeQuery(SqlGenerator.getDataTypeInfos(tableName));
		ResultSetMetaData metaData = resultSet.getMetaData();
		
		for (int i = 1; i <= metaData.getColumnCount(); i++) {
			ColumnInfo columnInfo = new ColumnInfo();
			columnInfo.setColumnPosition(i - 1);
			columnInfo.setColumnName(metaData.getColumnName(i));
			columnInfo.setLength(metaData.getColumnDisplaySize(i));
			DataType dataType = DataType.toDataType(metaData.getColumnType(i));
			if (dataType == DataType.DECIMAL) {
				columnInfo.setPrecision(metaData.getPrecision(i));
				columnInfo.setScale(metaData.getScale(i));
			}
			columnInfo.setDataType(dataType);
			
			columnInfos.add(columnInfo);
		}

		return columnInfos;
	}
	
	protected void executeSql(Statement statement, String sql) throws SQLException {
		if (!CommonUtils.isBlank(sql)) {
			String[] queries = sql.split(";");
			
			for (String query : queries) {
				if (!CommonUtils.isBlank(query)) {
					query = query.trim();
    				logger.info("Executing - {}", query);
    				try {
    					statement.execute(query);
    				}
    				catch (Exception e) {
    					throw new SQLException("Failed to execute - " + query, e);
    				}
				}
			}
		}
	}
		
	public abstract static class DataPublisherBaseBuilder<T extends DataPublisherBaseBuilder<T, V>, V extends DataPublisher> {
		private String jobId = null;
		private Connection conn = null;
		private String tableName = null;
		protected List<ColumnInfo> columnInfos = null;
		private LoadMode loadMode = LoadMode.APPEND;
		private List<String> mergeKeys = null;
		private String accessId = null;
		private String accessKey = null;
		private String bucketName = null;
//		private String objectKey = null;
		private String region = null;
		private List<String> primaryKeys = null; 
		private String preSql = null;
		private String postSql = null;
		
		DataPublisherBaseBuilder() {
			
		}
		
		@SuppressWarnings("unchecked")
		public T setJobId(final String jobId) {
			this.jobId = jobId;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setConnection(final Connection conn) {
			this.conn = conn;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setTableName(final String tableName) {
			this.tableName = tableName;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setColumnInfos(final List<ColumnInfo> columnInfos) {
			this.columnInfos = columnInfos;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setLoadMode(final LoadMode loadMode) {
			this.loadMode = loadMode;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setMergeKeys(final List<String> mergeKeys) {
			this.mergeKeys = mergeKeys;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setAccessId(final String accessId) {
			this.accessId = accessId;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setAccessKey(final String accessKey) {
			this.accessKey = accessKey;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setBucketName(final String bucketName) {
			this.bucketName = bucketName;
			return (T) this;
		}
		
//		@SuppressWarnings("unchecked")
//		public T setObjectKey(final String objectKey) {
//			this.objectKey = objectKey;
//			return (T) this;
//		}
		
		@SuppressWarnings("unchecked")
		public T setRegion(final String region) {
			this.region = region;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setPrimaryKeys(final List<String> primaryKeys) {
			this.primaryKeys = primaryKeys;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setPreSql(final String preSql) {
			this.preSql = preSql;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setPostSql(final String postSql) {
			this.postSql = postSql;
			return (T) this;
		}
		
		public abstract V build();
	}
}

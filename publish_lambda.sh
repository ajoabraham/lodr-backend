#!/bin/bash

if [ $# -ne 1 ] && [ $# -ne 2 ] 
then
    echo "Usage: publish_lambda.sh <alias name> <version (optional)>"
    exit
fi

functionNames=("SampleWranglerSourceFunction" "SampleSourceFunction" "ProcessDataFunction" "ProcessDataInMemoryFunction")
aliasName=$1

for n in "${functionNames[@]}"
do
    if [ -z "$2" ]
    then
    	publishCommand="aws lambda publish-version --function-name $n"
    	echo $publishCommand
    	returnVal=`$publishCommand`
    	versionNum=`echo $returnVal | grep -Po '(?<="Version": ")[^"]*'`
    else
	versionNum=$2
    fi

    updateCommand="aws lambda update-alias --function-name $n --name $aliasName --function-version $versionNum"
    echo $updateCommand
    returnVal=`$updateCommand`
    
    if [ -z "$2" ]
    then 
    	echo "Successfully published $n and pointed alias $aliasName to version $versionNum"
    else
	echo "Successfully pointed alias $aliasName to version $versionNum"
    fi
done


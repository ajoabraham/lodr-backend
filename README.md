lodr-backend
=================================

Lodr-backend contains all backend code for lodr product

What's included
=================================
pom.xml - Parent POM file for lodr-common and lodr-lambda Maven projects
repo - Maven local repository
lodr-common - Common source code used by both lodr-lambda and lodr-server projects
lodr-lambda - All source code for AWS Lambda
lodr-server - All source code for lodr Play server

How to build and run
==================================
1. Build Maven projects. Run the following command inside of lodr-backend folder
         
         mvn clean install -Dmaven.test.skip=true

2. Copy lodr-lambda/target/lodr-lambda-1.0.0.jar onto S3 under lodr-lambda/builds
3. In lodr-server folder run the following commands to build and start Play server

         activator (start Play console)
         compile (compile all source code)
         run (start Play server)

How to Deploy to Production
===================================
1. Build Maven projects. Run the following command inside of lodr-backend folder
         
         mvn clean install -Dmaven.test.skip=true

2. Copy lodr-lambda/target/lodr-lambda-1.0.0.jar onto S3 under lodr-lambda/builds
3. Login to Lodr Server EC2 instance and go into screen using the following command
               
          screen -r

4. Use Ctrl-C to kill the running server. Then run the following command inside of lodr/lodr-backend folder

           git pull (get the latest code)
           ./publish_lambda.sh prod (versioning the latest Lambda function)
           ./deploy_prod.sh (deploy and run Play server)

Set up TCP keepalive setting
==========================================
When JDBC execute long running query, such as COPY command. Sometimes underlying idle connection will be killed. If that happens,
in Redshift console it will show query is completed. But call to statement.execute() will never return. Use the steps below to
fix this issue.

1. Login to EC2 instance running Play Server
2. Edit /etc/sysctl.conf to add the following lines

net.ipv4.tcp_keepalive_time=200
net.ipv4.tcp_keepalive_intvl=200
net.ipv4.tcp_keepalive_probes=5

3. Reboot EC2 instance.
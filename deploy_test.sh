#!/bin/bash

# read -s -p "Enter Git Password: " pswd
# git pull --password $pswd
git pull
mvn clean install -Dmaven.test.skip=true
cd lodr-server
activator compile
activator -DappMode=test -DuploadBucket=lodr-user-uploads-staging -DdefaultRegion=us-west-2 run

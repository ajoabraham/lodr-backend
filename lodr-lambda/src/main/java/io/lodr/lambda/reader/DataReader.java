package io.lodr.lambda.reader;

import java.util.List;

import io.lodr.common.model.DataItem;

public interface DataReader extends AutoCloseable {
	public List<String> getHeaderNames() throws ReaderException;
	public DataItem readDataItem() throws ReaderException;
	public boolean isClosed();
}

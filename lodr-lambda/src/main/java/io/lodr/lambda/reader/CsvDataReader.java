package io.lodr.lambda.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lodr.common.model.DataItem;
import io.lodr.common.util.CSVUtils;

/**
 * 
 * @author Tai Hu
 *
 */
public class CsvDataReader implements DataReader {
	private static final Logger logger = LoggerFactory.getLogger(CsvDataReader.class);
	
	// TH 10/20/2016, NEVER access parser and iterator directly. Always call getCsvIterator() method.
	private CSVParser csvParser = null;
	private Iterator<CSVRecord> csvIterator = null;
	private boolean isClosed = false;

	private List<String> headerNames = null;
	private String delimiter;
	private String quote;	
	private Reader reader = null;
	private boolean includeHeaders = false;
	
	public CsvDataReader(Reader reader, List<String> headerNames, String delimiter, String quote, boolean includeHeaders) {
		this.reader = reader;
		this.headerNames = headerNames;
		this.delimiter = delimiter;
		this.quote = quote;
		this.includeHeaders = includeHeaders;
	}
	
	@Override
	public List<String> getHeaderNames() throws ReaderException {
		return headerNames;
	}

	@Override
	public DataItem readDataItem() throws ReaderException {
		if (isClosed())
			throw new ReaderException("Reader is already closed.");

		DataItem dataItem = null;
		
		try {			
			Iterator<CSVRecord> it = getCsvIterator();
        	CSVRecord r = it.next();
        
        	dataItem = new DataItem();
        	for (String headerName : getHeaderNames()) {
        		String value = "";
        		try {
        			value = r.get(headerName);
        		}
        		catch (Exception e) {        			
        		}
        		
        		dataItem.addValue(headerName, value);
        	}
		}
		catch (NoSuchElementException e) {
			// End of input source
		}
		catch (Exception e) {
			ReaderException readerException = new ReaderException(e.getMessage(), e);
			if (csvParser != null)
				readerException.setLineNumber(csvParser.getCurrentLineNumber());
			throw readerException;
		}
		
		return dataItem;
	}

	@Override
	public void close() {
		if (csvParser != null) {
			try {
				csvParser.close();
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}

			csvParser = null;
		}

		isClosed = true;
	}

	public boolean isClosed() {
		return isClosed;
	}
		
	private Iterator<CSVRecord> getCsvIterator() throws ReaderException {
		if (isClosed()) {
			throw new ReaderException("Reader is already closed.");
		}
		
		if (csvIterator == null) {
			try {
    			CSVFormat format = includeHeaders ? CSVUtils.createCSVFormat(delimiter, quote).withHeader(headerNames.toArray(new String[0])).withSkipHeaderRecord()
    					: CSVUtils.createCSVFormat(delimiter, quote).withHeader(headerNames.toArray(new String[0]));
    			csvParser = new CSVParser(new BufferedReader(reader), format);
    			csvIterator = csvParser.iterator();
			}
			catch (Exception e) {
				throw new ReaderException(e);
			}			
		}

		return csvIterator;
	}
}
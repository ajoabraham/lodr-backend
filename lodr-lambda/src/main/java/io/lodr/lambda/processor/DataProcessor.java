package io.lodr.lambda.processor;

import java.util.Map;

public interface DataProcessor {
	public Map<String, Object> process() throws ProcessorException;
}

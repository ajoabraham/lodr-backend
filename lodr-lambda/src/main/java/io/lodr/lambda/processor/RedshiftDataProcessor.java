package io.lodr.lambda.processor;

import java.util.LinkedHashMap;
import java.util.Map;

import io.lodr.common.util.TypeAnalyzer;

public class RedshiftDataProcessor extends DataProcessorBase {
//	private static final Logger logger = LoggerFactory.getLogger(RedshiftDataProcessor.class);
	
	public RedshiftDataProcessor(RedshiftDataProcessorBuilder builder) {
		super(builder);
	}

	@Override
	public Map<String, Object> process() throws ProcessorException {
		LinkedHashMap<String, TypeAnalyzer> typeAnalyzers = new LinkedHashMap<>();
		try {
			analyzeAndSaveData(typeAnalyzers);
		}
		catch (Exception e) {
			throw new ProcessorException(e);
		}
		
		return null;
	}
	
	public static class RedshiftDataProcessorBuilder extends DataProcessorBaseBuilder<RedshiftDataProcessorBuilder, RedshiftDataProcessor> {
		
		private RedshiftDataProcessorBuilder() {			
		}
		
		public static RedshiftDataProcessorBuilder create() {
			return new RedshiftDataProcessorBuilder();
		}
		
		@Override
		public RedshiftDataProcessor build() {
			return new RedshiftDataProcessor(this);
		}
	}

}

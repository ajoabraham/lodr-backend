package io.lodr.lambda.processor;

import static io.lodr.common.constant.CommonConstants.DEFAULT_CSV_DELIMITER;
import static io.lodr.common.constant.CommonConstants.DEFAULT_CSV_QUOTE;
import static io.lodr.common.constant.CommonConstants.DEFAULT_ENCODING_NAME;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.DataType;
import io.lodr.common.constant.JobState;
import io.lodr.common.model.ColumnData;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.DataItem;
import io.lodr.common.model.FieldError;
import io.lodr.common.model.JobStatus;
import io.lodr.common.model.RowData;
import io.lodr.common.model.command.Command;
import io.lodr.common.util.CSVUtils;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.DbUtils;
import io.lodr.common.util.JobStatusDbService;
import io.lodr.common.util.TypeAnalyzer;
import io.lodr.lambda.reader.CsvDataReader;
import io.lodr.lambda.reader.ReaderException;
import io.lodr.lambda.transform.Transformer;

/**
 * 
 * @author Tai Hu
 *
 */
public abstract class DataProcessorBase implements DataProcessor {
	private static final Logger logger = LoggerFactory.getLogger(DataProcessorBase.class);
	
	private String jobId = null;
	private int batchId = 0;
	private String bucketName = null;
	private String objectKey = null;
	private byte[] rawData = null;
	private int offset = 0;
	private int length = -1;
	private String delimiter = null;
	private String quote = null;
	private Charset encoding = null;
	private File outputFile = null;
	private Map<String, DataType> dataTypes = null;
	private List<Command> commands = null;
	// Column names in current input file
	private List<String> columnNames = null; 
	private List<String> originalColumnNames = null;
	private int postBackInterval = 0;
	
	private JobStatus jobStatus = null;
	private List<ColumnInfo> columnInfos = null;
	private long inputRows = 0;
	private long loadedRows = 0;
	private long failedRows = 0;
	private long filteredRows = 0;
	private Map<String, Integer> fieldErrors = new HashMap<>();
	private String sourceFileName = null;
	private Context context;

	public DataProcessorBase(DataProcessorBaseBuilder<?, ?> builder) {
		jobId = builder.jobId;
		batchId = builder.batchId;
		bucketName = builder.bucketName;
		objectKey = builder.objectKey;
		rawData = builder.rawData;
		offset = builder.offset;
		length = builder.length;
		delimiter = builder.delimiter;
		quote = builder.quote;
		encoding = builder.encoding;
		outputFile = builder.outputFile;
		columnInfos = builder.columnInfos;
		dataTypes = columnInfos.stream().collect(Collectors.toMap(c -> c.getColumnName().trim(), 
				c -> c.getDataType()));
		commands = builder.commands;
		columnNames = builder.columnNames;
		originalColumnNames = builder.originalColumnNames;
		jobStatus = builder.jobStatus;
		context = builder.context;
		
		if (builder.postBackInterval > CommonConstants.DEFAULT_POST_BACK_INTERVAL) {
			postBackInterval = builder.postBackInterval + (new Random()).nextInt(builder.postBackInterval);
		}
		else {
			postBackInterval = CommonConstants.DEFAULT_POST_BACK_INTERVAL + (new Random()).nextInt(CommonConstants.DEFAULT_POST_BACK_INTERVAL);
		}
		
		// Set up name and value for insertFileName command
		sourceFileName = CommonUtils.getSourceFileName(bucketName, objectKey);
	}

	void analyzeAndSaveData(LinkedHashMap<String, TypeAnalyzer> typeAnalyzers) throws Exception {		
		// Read in all the data and stored into Redshift ready file locally
		inputRows = 0;
		loadedRows = 0;
		failedRows = 0;
		filteredRows = 0;
		fieldErrors = new HashMap<>();
		
		if (jobStatus == null)
			jobStatus = JobStatusDbService.find(jobId, batchId);
		
		try (CsvDataReader dataReader = new CsvDataReader(new InputStreamReader(new ByteArrayInputStream(rawData, offset, length), encoding), columnNames, delimiter, quote, false);
				GZIPOutputStream gzipOutputStream = new GZIPOutputStream(new FileOutputStream(outputFile));
				CSVPrinter out = CSVUtils.createCSVPrinter(new PrintWriter(new OutputStreamWriter(gzipOutputStream, DEFAULT_ENCODING_NAME)),
						DEFAULT_CSV_DELIMITER, DEFAULT_CSV_QUOTE)) {
// TH 10/24/2016, CSV file for Redshift copy command does not need header row.			
//			out.printRecord(headerNames.stream().map(n -> CommonUtils.normalizeColumnName(n)).collect(Collectors.toList()));
			DataItem dataItem = null;
//			commands.forEach(c -> logger.info(c.getClass().getName() + " " + c.getColumnName()));			
			Transformer trans = Transformer.newInstance(commands);
			List<String> transformedColumnNames = null;
			
			for (;;) {
				try {
					if(context.getRemainingTimeInMillis() <= 500){
     					jobStatus.setJobState(JobState.TIMEDOUT);
     					JobStatusDbService.save(jobStatus);
     					break;
     				}
					inputRows++;
					
					dataItem = dataReader.readDataItem();
					
					if (dataItem == null) {
						// Remove the extra counted row
						inputRows--;
						break;
					}
					
					final RowData row = new RowData();
					row.setSourceFileName(sourceFileName);
					for (int i = 0; i < originalColumnNames.size(); i++) {
						String columnName = originalColumnNames.get(i);
						String value = dataItem.getValue(columnName);
						row.getColumnDatas().add(new ColumnData(columnName, value, i));
					}
					
//					logger.info("Before Transform: " + row.getColumnDatas().stream().map(c -> c.getName() + ":" + c.getValue()).collect(Collectors.joining("|")));
					final List<RowData> transformedRows = trans.transform(row);
					if (transformedRows == null || transformedRows.isEmpty()) {
						filteredRows++;
						continue;
					}
					else if (transformedRows.size() > 1) {
						inputRows = inputRows + (transformedRows.size() - 1);
					}
					
//					logger.info("After Transform: " + row.getColumnDatas().stream().map(c -> c.getName() + ":" + c.getValue()).collect(Collectors.joining("|")));
					
					// TH 04/02/2017, do not check this anymore. Since we will handle all new columns and missing columns cases, so the final
					// column names could be different.
//					if (transformedRows.stream().filter(r -> r.getColumnDatas().size() != finalColumnNames.size()).findAny().isPresent()) {
//						throw new Exception("Number of columns are incorrect after transformation.");
//					}
										
					for (RowData r : transformedRows) {
        				if (transformedColumnNames == null) {
        					transformedColumnNames = r.getColumnNames();
        				}
        				else if (!ListUtils.isEqualList(transformedColumnNames, r.getColumnNames())) {
        					logger.error("Expected columns {}, actual columns {}", transformedColumnNames, r.getColumnNames());
        					// TH 04/02/2017, this should never happen. If it happens, it means that 
        					// transformation code has bug in it.
        					throw new Exception("Columns are incorrect after transformation.");
        				}
						
        				List<String> values = new ArrayList<>();
        				        				
        				int index = 0;
        				for (ColumnData column : r.getColumnDatas()) {
        					final String columnName = column.getName();
        					
        					TypeAnalyzer typeAnalyzer = typeAnalyzers.get(columnName);
        					if (typeAnalyzer == null) {
        						typeAnalyzer = new TypeAnalyzer(columnName, index++, dataTypes.get(columnName));
        						typeAnalyzers.put(columnName, typeAnalyzer);
        					}
        
        					String value = column.getValue();
        					// Empty out invalid value
        					if (!typeAnalyzer.analyze(value)) {
        						value = "";
        						
        						int errorCount = fieldErrors.containsKey(columnName) ? fieldErrors.get(columnName) : 0;
        						fieldErrors.put(columnName, ++errorCount);
        					}
    
        					DataType dataType = typeAnalyzer.getColumnInfo().getDataType();
        					
        					if (CommonUtils.isBlank(value)) {
        						// TH 05/03/2017, Redshift does not like empty string for
        						// boolean type
        						if (dataType == DataType.BOOLEAN) {
        							values.add(null);
        						}
        						else {
        							values.add("");
        						}
        					}
        					else if (dataType == DataType.DATE) {
        						values.add(CommonUtils.convertToDefaultDateFormat(value));
        					}
        					else if (dataType == DataType.TIMESTAMP) {
        						values.add(CommonUtils.convertToDefaultTimestampFormat(value));
        					}
        					else {    						
        						if (dataType == DataType.VARCHAR
        								&& typeAnalyzer.getColumnInfo().getLength() >= CommonConstants.MAX_VARCHAR
        								&& CommonUtils.getStringLength(value) > CommonConstants.MAX_VARCHAR) {
        							// Make it slightly smaller to account for escape
        							value = CommonUtils.truncateString(value, CommonConstants.MAX_VARCHAR - 10);
        						}
        						
        						values.add(DbUtils.escapeForCopyCommand(value));
        					}
        				}
        
        				if (values.stream().filter(v -> !CommonUtils.isBlank(v)).findAny().isPresent()) {
            				out.printRecord(values);
            				loadedRows++;	
        				}
        				else {
        					failedRows++;
        				}
        				
        				if (inputRows % postBackInterval == 0) {
        					jobStatus.setInputRows(inputRows);
        					jobStatus.setLoadedRows(loadedRows);
        					jobStatus.setFailedRows(failedRows);
        					jobStatus.setFilteredRows(filteredRows);
        					JobStatusDbService.save(jobStatus);
        				}
					}
				}
				catch (ReaderException e) {
					// A invalid row encountered.
//					logger.info("Encounter invalid row at " + e.getLineNumber());
					failedRows++;
					
    				if (failedRows % postBackInterval == 0) {
    					jobStatus.setInputRows(inputRows);
    					jobStatus.setLoadedRows(loadedRows);
    					jobStatus.setFailedRows(failedRows);
    					jobStatus.setFilteredRows(filteredRows);
    					JobStatusDbService.save(jobStatus);
    				}
				}
			}
		}
		finally {
			// Update columnInfos
			List<ColumnInfo> updatedColumnInfos = new ArrayList<>();
			
 			for (Entry<String, TypeAnalyzer> e : typeAnalyzers.entrySet()) {
 				TypeAnalyzer typeAnalyzer = e.getValue();
 				ColumnInfo columnInfo = columnInfos.stream().filter(ci -> ci.getColumnName().equals(typeAnalyzer.getColumnInfo().getColumnName()))
 						.findAny().orElse(null);
 				
 				// New column
 				if (columnInfo == null) {
 					updatedColumnInfos.add(typeAnalyzer.getColumnInfo());
 				}
 				else {
     				columnInfo.setLength(Math.max(columnInfo.getLength(), typeAnalyzer.getColumnInfo().getLength()));
     				
     				if (columnInfo.getDataType() == DataType.INTEGER) {
     					if (typeAnalyzer.getColumnInfo().getDataType() == DataType.BIGINT) {
     						columnInfo.setDataType(DataType.BIGINT);
     					}
     				}
     				else if (typeAnalyzer.getColumnInfo().getDataType() == DataType.DECIMAL) {
     					int maxScale = Math.max(columnInfo.getScale(), typeAnalyzer.getColumnInfo().getScale());
     					int maxLeft = Math.max(columnInfo.getPrecision() - columnInfo.getScale(),
     							typeAnalyzer.getColumnInfo().getPrecision() - typeAnalyzer.getColumnInfo().getScale());
     					columnInfo.setPrecision(maxLeft + maxScale);
     					columnInfo.setScale(maxScale);
     				}
     				
     				updatedColumnInfos.add(columnInfo);
 				}
 			} 
 			 			
 			jobStatus.setColumnInfos(updatedColumnInfos);
 			jobStatus.setInputRows(inputRows);
 			jobStatus.setLoadedRows(loadedRows);
 			jobStatus.setFailedRows(failedRows);
 			jobStatus.setFilteredRows(filteredRows);
 			
			if (fieldErrors != null && !fieldErrors.isEmpty()) {
				jobStatus.setFieldErrors(fieldErrors.entrySet().stream().map(e -> new FieldError(e.getKey(), e.getValue()))
						.collect(Collectors.toList()));
			}						
//			JobStatusDbService.save(jobStatus);
		}
	}
	
	public static abstract class DataProcessorBaseBuilder<T extends DataProcessorBaseBuilder<T, V>, V extends DataProcessor> {
		private String jobId = null;
		private int batchId = 0;
		private String bucketName = null;
		private String objectKey = null;
		private byte[] rawData = null;
		private int offset = 0;
		private int length = -1;
		private String delimiter = null;
		private String quote = null;
		private Charset encoding = null;
		private File outputFile = null;
		private List<ColumnInfo> columnInfos = null;
		private List<Command> commands = null;
		private List<String> columnNames = null;
		private List<String> originalColumnNames = null;
		private JobStatus jobStatus = null;
		private int postBackInterval = 0;
		private Context context;
		
		DataProcessorBaseBuilder() {			
		}
				
		@SuppressWarnings("unchecked")
		public T setJobId(String jobId) {
			this.jobId = jobId;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setBatchId(int batchId) {
			this.batchId = batchId;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setBucketName(String bucketName) {
			this.bucketName = bucketName;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setObjectKey(String objectKey) {
			this.objectKey = objectKey;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setRawData(byte[] rawData) {
			this.rawData = rawData;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setOffset(int offset) {
			this.offset = offset;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setLength(int length) {
			this.length = length;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setDelimiter(String delimiter) {
			this.delimiter = delimiter;
			return ((T) this);
		}
		
		@SuppressWarnings("unchecked")
		public T setQuote(String quote) {
			this.quote = quote;
			return ((T) this);
		}
		
		@SuppressWarnings("unchecked")
		public T setEncoding(Charset encoding) {
			this.encoding = encoding;
			return ((T) this);
		}
		
		@SuppressWarnings("unchecked")
		public T setOutputFile(File outputFile) {
			this.outputFile = outputFile;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setColumnInfos(List<ColumnInfo> columnInfos) {
			this.columnInfos = columnInfos;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setCommands(List<Command> commands) {
			this.commands = commands;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setOriginalColumnNames(List<String> originalColumnNames) {
			this.originalColumnNames = originalColumnNames;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setColumnNames(List<String> columnNames) {
			this.columnNames = columnNames;
			return (T) this;
		}
				
		@SuppressWarnings("unchecked")
		public T setJobStatus(JobStatus jobStatus) {
			this.jobStatus = jobStatus;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setPostBackInterval(int postBackInterval) {
			this.postBackInterval = postBackInterval;
			return (T) this;
		}
		
		@SuppressWarnings("unchecked")
		public T setContext(Context context) {
			this.context = context;
			return (T) this;
		}
		
		public abstract V build();
	}
}

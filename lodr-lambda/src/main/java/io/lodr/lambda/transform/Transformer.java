package io.lodr.lambda.transform;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lodr.common.model.RowData;
import io.lodr.common.model.command.Command;
import io.lodr.common.model.command.FilterCommand;

public final class Transformer {
	private static final Logger logger = LoggerFactory.getLogger(Transformer.class);
	
	private List<Command> commands = null;
	
	private Transformer(List<Command> commands) {		
		this.commands = commands;
		
		if (commands != null && !commands.isEmpty()) {
			commands.stream().filter(c -> c instanceof FilterCommand).forEach(c -> {
				FilterCommand filterCommand = (FilterCommand) c;
				filterCommand.preprocessFilterValue();
				if (!filterCommand.isValid()) {
					logger.info("Invalid filter command: {} {} {}", c.getColumnName(),
							filterCommand.getOperator().getSymbol(), filterCommand.getRight());
				}
			});
		}
	}
	
	public static Transformer newInstance(List<Command> commands) {
		return new Transformer(commands);
	}
	
	// If this method return null or empty list, then mean this row should be ignored.
	public List<RowData> transform(RowData row) {
		List<RowData> transformedRows = new ArrayList<>();
		transformedRows.add(row);
		
		for (Command command : commands) {
			List<RowData> intermediaRows = new ArrayList<>();
			
			for (RowData r : transformedRows) {
				List<RowData> rows = command.execute(r);
				// Row is filtered out
				if (rows == null || rows.isEmpty()) {
					continue;
				}
				
				intermediaRows.addAll(rows);
			}
			
			if (intermediaRows.isEmpty()) {
				return null;
			}
			else {
				transformedRows.clear();
				transformedRows.addAll(intermediaRows);
			}
		}
		
		return transformedRows;
	}
}

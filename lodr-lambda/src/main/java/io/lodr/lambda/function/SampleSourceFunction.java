package io.lodr.lambda.function;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.SampleSourceInputData;
import io.lodr.common.model.SampleSourceOutputData;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.CSVUtils;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.Validator;

/**
 * 
 * @author Tai Hu
 * 
 * Create a quick sample of uploaded file to detect the following information
 * 1. Encoding
 * 2. Delimiter
 * 3. Quote
 * 4. Column size and names
 * 5. File size
 * 6. 10 rows of sample data
 *
 */
public class SampleSourceFunction implements RequestHandler<SampleSourceInputData, SampleSourceOutputData> {
	private static final Logger logger = LoggerFactory.getLogger(SampleSourceFunction.class);
	
	// Sample up to 100KB from S3
	private static final int SAMPLE_SIZE = 100 * 1024;
	// Number of rows to return in the output
	private static final int PREVIEW_ROW_SIZE = 10;
	
	@Override
	public SampleSourceOutputData handleRequest(SampleSourceInputData input, Context context) {
		final LodrTimer timer = LodrTimer.newInstance(SampleSourceFunction.class.getName());
		timer.start();
		
		final SampleSourceOutputData output = new SampleSourceOutputData();
		
		try {
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
			
    		final String bucketName = input.getBucketName();
    		final String objectKey = input.getObjectKey();
    		final boolean includesHeader = input.getIncludesHeader();
    		
    		AmazonS3Service s3 = AmazonS3Service.newInstance(input.getAccessId(), input.getAccessKey(), input.getRegion());    		
    		S3Object object = s3.getObject(new GetObjectRequest(bucketName, objectKey));
    		
    		output.setFileSize(object.getObjectMetadata().getInstanceLength());
    		    		    		
    		byte[] buf = new byte[CommonConstants.DEFAULT_BUFFER_SIZE];
    		byte[] rawSampleSource = null;
     		try (InputStream in = CommonUtils.getInputStream(objectKey, object.getObjectContent());
    				ByteArrayOutputStream out = new ByteArrayOutputStream()) { 
     			int count = 0;
     			int totalCount = 0;
     			while (totalCount < SAMPLE_SIZE && (count = in.read(buf)) != -1) {
     				out.write(buf, 0, count);
     				totalCount += count;
     			}
     			
     			rawSampleSource = out.toByteArray();
     		}
    		    		
    		Charset encoding = CommonUtils.detectEncoding(rawSampleSource);
    		output.setEncoding(encoding.name());
    
    		String sampleSource = new String(rawSampleSource, encoding);
    		
    		Character quote = CommonUtils.detectQuoteCharacter(sampleSource, true);    		
    		output.setQuote(quote.toString());
    		
    		Character delimiter = CommonUtils.isJsonFile(objectKey) ? '\0' : CommonUtils.detectDelimiter(sampleSource, true);
    		output.setDelimiter(delimiter.toString());
    		
    		List<String> columnNames = null;
    		List<List<String>> previewData = null;
    		
    		try {
    			CSVFormat format = CSVUtils.createCSVFormat(delimiter.toString(), quote.toString());
    			columnNames = CSVUtils.getColumnNames(sampleSource, format, includesHeader);			
    			previewData = CSVUtils.getPreviewData(sampleSource, columnNames, format, PREVIEW_ROW_SIZE, includesHeader);
    		}
    		catch (Exception e) {
    			// Preview failed try to read in entire line as a column
    			output.setDelimiter(String.valueOf('\0'));
    			CSVFormat format = CSVUtils.createCSVFormat(output.getDelimiter(), quote.toString());
    			columnNames = CSVUtils.getColumnNames(sampleSource, format, includesHeader);			
    			previewData = CSVUtils.getPreviewData(sampleSource, columnNames, format, PREVIEW_ROW_SIZE, includesHeader);
    		}
			
			output.setColumnNames(columnNames);
			output.setPreviewData(previewData);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			output.setThrowable(e);
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
		
		return output;
	}
}

package io.lodr.lambda.function;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.util.CommonUtils;

public class ParseDateTimeFunction implements RequestHandler<String, String> {
	private static final Logger logger = LoggerFactory.getLogger(ParseDateTimeFunction.class);

	public ParseDateTimeFunction() {
	}

	@Override
	public String handleRequest(String input, Context context) {
		logger.info("Input date string: {}", input);
		
		Date d = CommonUtils.parseDate(input, CommonConstants.DATE_FORMATS);
		
		if (!CommonUtils.isDate(input)) {
			return input + " is not a valid date";
		}
		
		return d == null ? (input + " cannot be parsed.") : d.toString();
	}
}

package io.lodr.lambda.function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import io.lodr.common.model.ConnInfo;
import io.lodr.common.model.TestConnectionOutputData;
import io.lodr.common.util.ConnUtils;
import io.lodr.common.util.LodrTimer;

public class TestConnectionFunction implements RequestHandler<ConnInfo, TestConnectionOutputData> {
	private static final Logger logger = LoggerFactory.getLogger(TestConnectionFunction.class);
	
	public TestConnectionFunction() {
		
	}

	@Override
	public TestConnectionOutputData handleRequest(ConnInfo connInfo, Context context) {
		final LodrTimer timer = LodrTimer.newInstance(TestConnectionFunction.class.getName());
		timer.start();
		
		final TestConnectionOutputData output = new TestConnectionOutputData();
		
		try { 
			ConnUtils.testConnection(connInfo);
			output.setSucceeded(true);
		}
		catch (Exception e) {
			output.setSucceeded(false);
			output.setThrowable(e);
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
		
		return output;
	}
}

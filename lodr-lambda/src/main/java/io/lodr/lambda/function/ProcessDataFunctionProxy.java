package io.lodr.lambda.function;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

import io.lodr.common.model.ProcessDataInputData;
import io.lodr.common.model.ProcessDataOutputData;

public interface ProcessDataFunctionProxy {
	@LambdaFunction(functionName="ProcessDataFunction")
	ProcessDataOutputData processData(ProcessDataInputData input);
}

package io.lodr.lambda.function;

import static io.lodr.common.constant.CommonConstants.DEFAULT_CSV_DELIMITER;
import static io.lodr.common.constant.CommonConstants.DEFAULT_CSV_QUOTE;
import static io.lodr.common.constant.CommonConstants.DEFAULT_ENCODING_NAME;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.DataType;
import io.lodr.common.constant.JobState;
import io.lodr.common.model.ColumnData;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.DataItem;
import io.lodr.common.model.FieldError;
import io.lodr.common.model.FileInfo;
import io.lodr.common.model.JobStatus;
import io.lodr.common.model.ProcessDataInputData;
import io.lodr.common.model.RowData;
import io.lodr.common.model.command.Command;
import io.lodr.common.model.command.DeleteRowsCommand;
import io.lodr.common.model.command.DummyCommand;
import io.lodr.common.model.command.MergeAllRowsCommand;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.CSVUtils;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.DbUtils;
import io.lodr.common.util.JobStatusDbService;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.TypeAnalyzer;
import io.lodr.lambda.reader.CsvDataReader;
import io.lodr.lambda.reader.ReaderException;
import io.lodr.lambda.transform.Transformer;

public class ProcessDataInMemoryFunction implements RequestHandler<ProcessDataInputData, Void> {
	private static final Logger logger = LoggerFactory.getLogger(ProcessDataInMemoryFunction.class);

	public ProcessDataInMemoryFunction() {
	}

	@Override
	public Void handleRequest(ProcessDataInputData input, Context context) {
		final LodrTimer timer = LodrTimer.newInstance(ProcessDataInMemoryFunction.class.getName());
		timer.start();

		final String jobId = input.getJobId();
		final int batchId = input.getBatchId();
		final List<FileInfo> fileInfos = input.getFileInfos();
		final String srcAccessId = input.getAccessId();
		final String srcAccessKey = input.getAccessKey();
		final String srcRegion = input.getRegion();
		final String destAccessId = input.getDestAccessId();
		final String destAccessKey = input.getDestAccessKey();
		final String destBucketName = input.getDestBucketName();
		final String destRegion = input.getDestRegion();
		final String outputFolderObjectKey = input.getOutputFolderObjectKey();
		final String outputFilePrefix = input.getOutputFilePrefix();
		final boolean includesHeader = input.getIncludesHeader();
		final List<Command> commands = input.getCommands();
		final List<String> columnNames = input.getColumnNames();
		final List<String> originalColumnNames = input.getOriginalColumnNames();

		final List<ColumnInfo> columnInfos = input.getColumnInfos();
		final int postBackInterval = input.getPostBackInterval();

		timer.setName(String.format("%s (Job ID: %s, Batch ID: %d)", timer.getName(), jobId, batchId));
		JobStatus jobStatus = null;

		try (final AmazonS3Service srcS3= AmazonS3Service.newInstance(srcAccessId, srcAccessKey, srcRegion);
				final AmazonS3Service destS3 = AmazonS3Service.newInstance(destAccessId, destAccessKey, destRegion)) {
			jobStatus = JobStatusDbService.find(input.getJobId(), input.getBatchId());
			jobStatus.setJobState(JobState.RUNNING);
			
			// Read all data into memory
			timer.startChild("Read raw data");
			
			// All stats
			long inputRows = 0;
			long loadedRows = 0;
			long failedRows = 0;
			long filteredRows = 0;
			Map<String, Integer> fieldErrors = new HashMap<>();

			final List<RowData> allRows = new ArrayList<>();

			for (FileInfo fileInfo : fileInfos) {
				S3Object object = srcS3.getObject(new GetObjectRequest(fileInfo.getBucketName(), fileInfo.getObjectKey()));
				 
				try (CsvDataReader dataReader = new CsvDataReader(new InputStreamReader(object.getObjectContent(), fileInfo.getEncoding()), columnNames, fileInfo.getDelimiter(), fileInfo.getQuote(), includesHeader)) {
					final String sourceFileName = CommonUtils.getSourceFileName(fileInfo.getBucketName(), fileInfo.getObjectKey());
					
					for (;;) {
						try {
							inputRows++;

							DataItem dataItem = dataReader.readDataItem();

							if (dataItem == null) {
								// Remove the extra counted row
								inputRows--;
								break;
							}

							final RowData rowData = new RowData();
							rowData.setSourceFileName(sourceFileName);
							for (int i = 0; i < originalColumnNames.size(); i++) {
								String columnName = originalColumnNames.get(i);
								String value = dataItem.getValue(columnName);
								rowData.getColumnDatas().add(new ColumnData(columnName, value, i));
							}

							allRows.add(rowData);
						}
						catch (ReaderException e) {
							failedRows++;
						}
					}
				}
			}
			
			logger.info("{} loaded {} rows into memory (failed row count {})", timer.getChildDurationDesc("Read raw data"), inputRows, failedRows);

			// Split the commands by MergeAllRows and DeleteRows command
			Map<Command, List<Command>> commandMap = new LinkedHashMap<>();
			List<Command> commandList = new ArrayList<>();
			for (Command command : commands) {
				if (command instanceof MergeAllRowsCommand || command instanceof DeleteRowsCommand) {
					commandMap.put(command, commandList);
					commandList = new ArrayList<>();
				}
				else {
					commandList.add(command);
				}
			}
			
			if (!commandList.isEmpty()) {
				commandMap.put(new DummyCommand(), commandList);
			}

			timer.startChild("Process data");
			File outputFile = new File(CommonConstants.LOCAL_TEMP_DIR, "output.csv");

			// Transform all rows
			for (Entry<Command, List<Command>> entry : commandMap.entrySet()) {
				if (!entry.getValue().isEmpty()) {
					Transformer trans = Transformer.newInstance(entry.getValue());
					final List<RowData> allTransformedRows = new ArrayList<>();

					for (RowData rowData : allRows) {
						final List<RowData> transformedRows = trans.transform(rowData);
						if (transformedRows == null || transformedRows.isEmpty()) {
							filteredRows++;
							continue;
						}

						allTransformedRows.addAll(transformedRows);
					}

					allRows.clear();
					allRows.addAll(allTransformedRows);
				}
				
				if (entry.getKey() instanceof MergeAllRowsCommand) {
					((MergeAllRowsCommand) entry.getKey()).setAllRows(allRows);
					entry.getKey().execute(null);
				}
				else if (entry.getKey() instanceof DeleteRowsCommand) {
					((DeleteRowsCommand) entry.getKey()).setAllRows(allRows);
					entry.getKey().execute(null);
				}
			}

			if (inputRows < allRows.size()) {
				inputRows = allRows.size();
			}

			// Now determine data type and write out to local file
			final Map<String, TypeAnalyzer> typeAnalyzers = new LinkedHashMap<>();
			final Map<String, DataType> dataTypes = columnInfos.stream().collect(Collectors.toMap(c -> c.getColumnName(), c -> c.getDataType()));
			
			try (GZIPOutputStream gzipOutputStream = new GZIPOutputStream(new FileOutputStream(outputFile));
					CSVPrinter out = CSVUtils.createCSVPrinter(new PrintWriter(new OutputStreamWriter(gzipOutputStream, DEFAULT_ENCODING_NAME)),
							DEFAULT_CSV_DELIMITER, DEFAULT_CSV_QUOTE)) {
				List<String> transformedColumnNames = null;
						
				for (RowData r : allRows) {
    				if (transformedColumnNames == null) {
    					transformedColumnNames = r.getColumnNames();
    				}
    				else if (!ListUtils.isEqualList(transformedColumnNames, r.getColumnNames())) {
    					logger.error("Expected columns {}, actual columns {}", transformedColumnNames, r.getColumnNames());
    					// TH 04/02/2017, this should never happen. If it happens, it means that 
    					// transformation code has bug in it.
    					throw new Exception("Columns are incorrect after transformation.");
    				}
    				
					List<String> values = new ArrayList<>();
					
					int index = 0;
					for (ColumnData column : r.getColumnDatas()) {
						final String columnName = column.getName();

						TypeAnalyzer typeAnalyzer = typeAnalyzers.get(columnName);						
						if (typeAnalyzer == null) {
							typeAnalyzer = new TypeAnalyzer(columnName, index++, dataTypes.get(columnName));
							typeAnalyzers.put(columnName, typeAnalyzer);
						}

						String value = column.getValue();
						// Empty out invalid value
						if (!typeAnalyzer.analyze(value)) {
							value = "";

							int errorCount = fieldErrors.containsKey(columnName) ? fieldErrors.get(columnName) : 0;
							fieldErrors.put(columnName, ++errorCount);
						}

						DataType dataType = typeAnalyzer.getColumnInfo().getDataType();

						if (CommonUtils.isBlank(value)) {
							values.add("");
						}
						else if (dataType == DataType.DATE) {
							values.add(CommonUtils.convertToDefaultDateFormat(value));
						}
						else if (dataType == DataType.TIMESTAMP) {
							values.add(CommonUtils.convertToDefaultTimestampFormat(value));
						}
						else {
							if (dataType == DataType.VARCHAR
									&& typeAnalyzer.getColumnInfo().getLength() >= CommonConstants.MAX_VARCHAR
									&& CommonUtils.getStringLength(value) > CommonConstants.MAX_VARCHAR) {
								// Make it slightly smaller to account for
								// escape
								value = CommonUtils.truncateString(value, CommonConstants.MAX_VARCHAR - 10);
							}

							values.add(DbUtils.escapeForCopyCommand(value));
						}
					}

					if (values.stream().filter(v -> !CommonUtils.isBlank(v)).findAny().isPresent()) {
						out.printRecord(values);
						loadedRows++;
						
						if (loadedRows % postBackInterval == 0) {
							jobStatus.setInputRows(inputRows);
							jobStatus.setLoadedRows(loadedRows);
							jobStatus.setFailedRows(failedRows);
							jobStatus.setFilteredRows(filteredRows);
							JobStatusDbService.save(jobStatus);
						}
					}
					else {
						failedRows++;
						
	    				if (failedRows % postBackInterval == 0) {
	    					jobStatus.setInputRows(inputRows);
	    					jobStatus.setLoadedRows(loadedRows);
	    					jobStatus.setFailedRows(failedRows);
	    					jobStatus.setFilteredRows(filteredRows);
	    					JobStatusDbService.save(jobStatus);
	    				}
					}
				}

			}
			finally {
				// Update columnInfos
				List<ColumnInfo> updatedColumnInfos = new ArrayList<>();
				
	 			for (Entry<String, TypeAnalyzer> e : typeAnalyzers.entrySet()) {
	 				TypeAnalyzer typeAnalyzer = e.getValue();
	 				ColumnInfo columnInfo = columnInfos.stream().filter(ci -> ci.getColumnName().equals(typeAnalyzer.getColumnInfo().getColumnName()))
	 						.findAny().orElse(null);
	 				
	 				// New column
	 				if (columnInfo == null) {
	 					updatedColumnInfos.add(typeAnalyzer.getColumnInfo());
	 				}
	 				else {
	     				columnInfo.setLength(Math.max(columnInfo.getLength(), typeAnalyzer.getColumnInfo().getLength()));
	     				
	     				if (columnInfo.getDataType() == DataType.INTEGER) {
	     					if (typeAnalyzer.getColumnInfo().getDataType() == DataType.BIGINT) {
	     						columnInfo.setDataType(DataType.BIGINT);
	     					}
	     				}
	     				else if (typeAnalyzer.getColumnInfo().getDataType() == DataType.DECIMAL) {
	     					int maxScale = Math.max(columnInfo.getScale(), typeAnalyzer.getColumnInfo().getScale());
	     					int maxLeft = Math.max(columnInfo.getPrecision() - columnInfo.getScale(),
	     							typeAnalyzer.getColumnInfo().getPrecision() - typeAnalyzer.getColumnInfo().getScale());
	     					columnInfo.setPrecision(maxLeft + maxScale);
	     					columnInfo.setScale(maxScale);
	     				}
	     				
	     				updatedColumnInfos.add(columnInfo);
	 				}
	 			} 
	 			 			
	 			jobStatus.setColumnInfos(updatedColumnInfos);
				jobStatus.setInputRows(inputRows);
				jobStatus.setLoadedRows(loadedRows);
				jobStatus.setFailedRows(failedRows);
				jobStatus.setFilteredRows(filteredRows);

				if (fieldErrors != null && !fieldErrors.isEmpty()) {
					jobStatus.setFieldErrors(fieldErrors.entrySet().stream().map(e -> new FieldError(e.getKey(), e.getValue())).collect(Collectors.toList()));
				}
			}

			logger.info(timer.getChildDurationDesc("Process data"));

			timer.startChild("Upload to S3");
			
			String outputFileObjectKey = CommonUtils.createOutputFileObjectKey(outputFolderObjectKey, outputFilePrefix, batchId);
			destS3.uploadFile(destBucketName, outputFileObjectKey, outputFile);
			logger.info(timer.getChildDurationDesc("Upload to S3"));

			jobStatus.setOutputFileObjectKey(outputFileObjectKey);
			jobStatus.setJobState(JobState.SUCCEEDED);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			// output.setThrowable(e);
			jobStatus.setError(e.getMessage());
			jobStatus.setErrorDetails(CommonUtils.convertToString(e));
			jobStatus.setJobState(JobState.FAILED);
		}
		finally {
			try {
				jobStatus.setEndTime(new Date());
				JobStatusDbService.saveJobStatusWithColumnInfos(jobStatus);
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		timer.stop();
		logger.info(timer.getDurationDesc());

		return null;
	}
}

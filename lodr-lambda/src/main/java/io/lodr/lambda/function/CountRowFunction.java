package io.lodr.lambda.function;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.CountRowInputData;
import io.lodr.common.model.CountRowOutputData;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.LodrTimer;

public class CountRowFunction implements RequestHandler<CountRowInputData, CountRowOutputData> {
	private static final Logger logger = LoggerFactory.getLogger(CountRowFunction.class);
	
	// Size of each chunk should be processed paralleled. (100MB by default)
	private static final int BATCH_SIZE = 100 * 1024 * 1024;
	private static final int THREAD_POOL_SIZE = 4;
	
	@Override
	public CountRowOutputData handleRequest(CountRowInputData input, Context context) {
		final LodrTimer timer = LodrTimer.newInstance(CountRowFunction.class.getName());
		timer.start();
		
		final CountRowOutputData output = new CountRowOutputData();
		
		try {    		    		
    		final String bucketName = input.getBucketName();
    		final String objectKey = input.getObjectKey();
    		
    		AmazonS3Service s3 = AmazonS3Service.newInstance(input.getAccessId(), input.getAccessKey(), input.getRegion());    		
    		ObjectMetadata metadata = s3.getObjectMetadata(bucketName, objectKey);
    		    		    		    		
    		long fileSize = metadata.getInstanceLength();
    		
    		// File is not big enough for multithreaded read
    		long rowCount = 0;
    		if (fileSize < (BATCH_SIZE * 1.2)) {
    			rowCount = countLineInSegment(s3, bucketName, objectKey, 0, fileSize - 1);
    		}
    		else {
    			List<Future<Long>> futures = new ArrayList<>();
    			ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    			
    			long startPosition = 0;
    			
    			while (startPosition < fileSize) {
    				long endPosition = startPosition + BATCH_SIZE - 1;
    				endPosition = (endPosition > fileSize - 1) ? (fileSize - 1) : endPosition;
    				
    				Future<Long> future = threadPool.submit(new CountLineTask(s3, bucketName, objectKey, startPosition, endPosition));    				
    				futures.add(future);
    				
    				startPosition = endPosition + 1;
    			}
    			
    			rowCount = futures.stream().mapToLong(f -> {
    				long lc = 0;
    				try {
						lc = f.get();						
					}
					catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
    				return lc;
    			}).sum();
    		}
    		
    		output.setRowCount(rowCount);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			output.setThrowable(e);
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
		
		return output;
	}
	
	private static long countLineInSegment(AmazonS3Service s3, String bucketName, String objectKey, long startPosition, long endPosition) throws Exception {
		long lineCount = 0;
		S3Object obj = s3.getObject(new GetObjectRequest(bucketName, objectKey).withRange(startPosition, endPosition));
		
 		try (InputStream in = obj.getObjectContent()) {
 			byte[] buf = new byte[CommonConstants.DEFAULT_BUFFER_SIZE];
 			int count = 0;
	
 			while ((count = in.read(buf)) != -1) {
 				for (int i = 0; i < count; i++) {
 					if (buf[i] == '\n') lineCount++;
 				}
 			}
 		}
 		
 		return lineCount;
	}
	
	private static class CountLineTask implements Callable<Long> {
		private AmazonS3Service s3 = null;
		private String bucketName = null;
		private String objectKey = null;
		private long startPosition = -1;
		private long endPosition = -1;
		
		public CountLineTask(AmazonS3Service s3, String bucketName, String objectKey, long startPosition, long endPosition) {
			this.s3 = s3;
			this.bucketName = bucketName;
			this.objectKey = objectKey;
			this.startPosition = startPosition;
			this.endPosition = endPosition;
		}
		
		@Override
		public Long call() throws Exception {
			return CountRowFunction.countLineInSegment(s3, bucketName, objectKey, startPosition, endPosition);
		}
	}
}

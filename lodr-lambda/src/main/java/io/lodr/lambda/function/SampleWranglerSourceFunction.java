package io.lodr.lambda.function;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CyclicBarrier;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.model.S3Object;

import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.SampleWranglerSourceInputData;
import io.lodr.common.model.SampleWranglerSourceOutputData;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.CSVUtils;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.TypeAnalyzer;
import io.lodr.common.util.Validator;

/**
 * 
 * @author Tai Hu
 * 
 * Sample the source file for wrangler UI and return the following information
 * 
 * 1. First 1000 rows (or user specific size)
 * 2. File row count
 * 3. Data type for each column
 */
public class SampleWranglerSourceFunction implements RequestHandler<SampleWranglerSourceInputData, SampleWranglerSourceOutputData> { 
	private static final Logger logger = LoggerFactory.getLogger(SampleWranglerSourceFunction.class);
	
	// When file size exceeds this threshold, just do an estimate row count 
	private static final int ROW_COUNT_THRESHOLD = 10 * 1024 * 1024;
	
	private long totalRowCount = 0;
	
	@Override
	public SampleWranglerSourceOutputData handleRequest(SampleWranglerSourceInputData input, Context context) {
		final LodrTimer timer = LodrTimer.newInstance(SampleWranglerSourceFunction.class.getName());
		timer.start();
		timer.startChild("Analyzer");
		
		final SampleWranglerSourceOutputData output = new SampleWranglerSourceOutputData();
		
		try {    		
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
			
    		final String bucketName = input.getBucketName();
    		final String objectKey = input.getObjectKey();
    		final Charset encoding = Charset.forName(input.getEncoding());
    		final String delimiter = input.getDelimiter();
    		final String quote = input.getQuote();
    		final int sampleSize = input.getSampleRowCount();
    		final boolean includesHeader = input.getIncludesHeader();
    		
    		final CyclicBarrier cb = new CyclicBarrier(2);
    		final List<String> sampleRows = new ArrayList<>();
    		
    		output.setEstimatedRowCount(false);
    		
    		final Thread t = new Thread(() -> {
            	boolean isBarrierReleased = false;
            	
        		try {
        			AmazonS3Service s3 = AmazonS3Service.newInstance(input.getAccessId(), input.getAccessKey(), input.getRegion());    		
            		S3Object object = s3.getObject(bucketName, objectKey);    		
            		
            		try (BufferedReader in = new BufferedReader(new InputStreamReader(CommonUtils.getInputStream(objectKey, object.getObjectContent()), encoding))) {
                		totalRowCount = 0;
                		String line = null;
                		
            			while ((line = in.readLine()) != null) {
            				totalRowCount++;
            				
            				if (totalRowCount < sampleSize) {
            					sampleRows.add(line);
            				}
            				else if (totalRowCount == sampleSize) {
            					sampleRows.add(line);
            					try {
            						isBarrierReleased = true;
            						cb.await();
            					}
            					catch (Exception e) {
            						logger.error(e.getMessage(), e);
            					}
            					
            					// Get an estimated count
            					long fileSize = object.getObjectMetadata().getInstanceLength();
            					if (ROW_COUNT_THRESHOLD < fileSize) {
            						totalRowCount = (long)((double) fileSize / String.join("\n", sampleRows).getBytes(encoding).length * totalRowCount);
            						output.setEstimatedRowCount(true);
            						break;
            					}
            				}
            			}
            		}
        		}
        		catch (Exception e) {
        			logger.error(e.getMessage(), e);
        		}
        		finally {
        			if (!isBarrierReleased) {
        				try {
        					cb.await();
        				}
        				catch (Exception e) {
        					logger.error(e.getMessage(), e);
        				}
        			}
        		}
    		});
    		
    		t.start();
    		
    		try {
    			cb.await();
    		}
    		catch (Exception e) {
    			logger.error(e.getMessage(), e);
    		}
    		    		    		    		
    		CSVFormat csvFormat = CSVUtils.createCSVFormat(delimiter, quote);
    		final String rawSampleRows = String.join("\n", sampleRows);
    		List<String> columnNames = CSVUtils.getColumnNames(rawSampleRows, csvFormat, includesHeader);
    		    		
    		Map<String, TypeAnalyzer> typeAnalyzers = new HashMap<>();
    		// Initialize type analyzers here, just in case input file is header only
    		for (int i = 0; i < columnNames.size(); i++) {
    			typeAnalyzers.put(columnNames.get(i), new TypeAnalyzer(columnNames.get(i), i));
    		}
    		
			List<List<String>> previewData = new ArrayList<>();
			
    		try (CSVParser parser = CSVParser.parse(rawSampleRows, 
    				includesHeader ? csvFormat.withHeader(columnNames.toArray(new String[0])).withSkipHeaderRecord()
    						: csvFormat.withHeader(columnNames.toArray(new String[0])))) {  
    			Iterator<CSVRecord> it = parser.iterator();
    			int sampleRowSize = sampleRows.size();
    			int rowCount = 0;
    			while (rowCount++ < sampleRowSize) {
    				try {
    					CSVRecord r = it.next();
    					
        				List<String> row = new ArrayList<>();
        				
        				for (int columnIndex = 0; columnIndex < columnNames.size(); columnIndex++) {
        					String columnName = columnNames.get(columnIndex);
        					TypeAnalyzer typeAnalyzer = typeAnalyzers.get(columnName);
        				
        					String value = "";
        					try {
        						value = r.get(columnName);
        					}
        					catch (Exception e) {        						
        					}
        					
        					typeAnalyzer.analyze(value);
        					row.add(value);
        				}
        				
        				previewData.add(row);
    				}
    				catch (NoSuchElementException e) {
    					break;
    				}
    				catch (Exception e) {    					
    				}
    			}
    		}
    		
    		List<ColumnInfo> columnInfos = new ArrayList<>();
    		for (String columnName : columnNames) {
    			TypeAnalyzer typeAnalyzer = typeAnalyzers.get(columnName);
    			ColumnInfo columnInfo = new ColumnInfo();
    			columnInfo.setColumnName(columnName);
    			columnInfo.setColumnPosition(typeAnalyzer.getColumnInfo().getColumnPosition());
    			columnInfo.setDataType(typeAnalyzer.getColumnInfo().getDataType());
    			columnInfos.add(columnInfo);
    		}
    		
    		output.setColumnInfos(columnInfos);
    		output.setPreviewData(previewData);
    		
    		logger.info(timer.getChildDurationDesc("Analyzer"));
    		
    		timer.startChild("waiting for row count");
    		try {
    			t.join();
    		}
    		catch (Exception e) {
    			logger.error(e.getMessage(), e);    			
    		}
    		logger.info(timer.getChildDurationDesc("waiting for row count"));
    		
    		output.setTotalRowCount(totalRowCount);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			output.setThrowable(e);
		}
		
		logger.info(timer.getDurationDesc());
		return output;
	}

}

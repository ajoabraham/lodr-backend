package io.lodr.lambda.function;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.JobState;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.JobStatus;
import io.lodr.common.model.ProcessDataInputData;
import io.lodr.common.model.command.Command;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.JobStatusDbService;
import io.lodr.common.util.LodrTimer;
import io.lodr.lambda.processor.DataProcessor;
import io.lodr.lambda.processor.RedshiftDataProcessor.RedshiftDataProcessorBuilder;

public class ProcessDataFunction implements RequestHandler<ProcessDataInputData, Void> {
	private static final Logger logger = LoggerFactory.getLogger(ProcessDataFunction.class);
	
	public ProcessDataFunction() {

	}

	@Override
	public Void handleRequest(ProcessDataInputData input, Context context) {
		final LodrTimer timer = LodrTimer.newInstance(ProcessDataFunction.class.getName());
		timer.start();
		
		final String jobId = input.getJobId();
		final int batchId = input.getBatchId();    
		final String srcAccessId = input.getAccessId();
		final String srcAccessKey = input.getAccessKey();
		final String srcBucketName = input.getBucketName();
		final String srcRegion = input.getRegion();
		final String destAccessId = input.getDestAccessId();
		final String destAccessKey = input.getDestAccessKey();
		final String destBucketName = input.getDestBucketName();
		final String destRegion = input.getDestRegion();
		final String objectKey = input.getObjectKey();
		final String outputFolderObjectKey = input.getOutputFolderObjectKey();
		final String outputFilePrefix = input.getOutputFilePrefix();
		final boolean includesHeader = input.getIncludesHeader();
		final List<Command> commands = input.getCommands();
		final List<String> columnNames = input.getColumnNames();
		final List<String> originalColumnNames = input.getOriginalColumnNames();
		
		final Charset encoding = Charset.forName(input.getEncoding());
		final String delimiter = input.getDelimiter();
		final String quote = input.getQuote();
		final List<ColumnInfo> columnInfos = input.getColumnInfos();
		final int postBackInterval = input.getPostBackInterval();
		
//		final ProcessDataOutputData output = new ProcessDataOutputData();
		JobStatus jobStatus = null;
		
		try (final AmazonS3Service srcS3= AmazonS3Service.newInstance(srcAccessId, srcAccessKey, srcRegion);
				final AmazonS3Service destS3 = AmazonS3Service.newInstance(destAccessId, destAccessKey, destRegion)) {
			jobStatus = JobStatusDbService.find(input.getJobId(), input.getBatchId());
			jobStatus.setJobState(JobState.RUNNING);
			
			final long startPosition = jobStatus.getStartPosition();
			final int batchSize = jobStatus.getBatchSize();
			
			timer.setName(String.format("%s (Batch ID: %d, Start Position: %d)", timer.getName(), batchId, startPosition));
//			JobStatusDbService.save(jobStatus);
			    		    				
    		// Make raw data slightly bigger, it is because we need to 
    		// read some extra bytes at the end until reach new line character.
    		byte[] rawData = new byte[batchSize + (1024 * 1024)];
    		
    		timer.startChild("Read raw data"); 
    		logger.info("Reading raw data in batch sizes of " + batchSize );
    		
    		int index = 0;
    		final boolean isUTF16 = encoding == StandardCharsets.UTF_16 || encoding == StandardCharsets.UTF_16BE || encoding == StandardCharsets.UTF_16LE;
    		
     		try (final InputStream in = srcS3.getObject(new GetObjectRequest(srcBucketName, objectKey).withRange(startPosition)).getObjectContent()) {
     			byte[] buf = new byte[CommonConstants.DEFAULT_BUFFER_SIZE];
     
     			// If this is the first chuck of data and header is not included
     			// then no need to skip first line.
     			boolean ignoredFirstLine = (startPosition == 0 && !includesHeader);
     			boolean isSlashR = false;
     			boolean isSlashN = false;
     			boolean isDone = false;
     			// Total number of bytes actually read from input stream.
     			long bytesRead = 0;
     			// Number of bytes read in each read() method call
     			int readCount = 0;     			
     			while (!isDone && (readCount = in.read(buf)) != -1) {
     				
     				if(context.getRemainingTimeInMillis() <= (0.80*300*1000)){
     					jobStatus.setJobState(JobState.TIMEDOUT);
     					break;
     				}
     				
     				for (int i = 0; i < readCount; i++) {
     					bytesRead++;
     					if (ignoredFirstLine) {
     						if (isSlashR) {
     							if (bytesRead <= batchSize && isUTF16 && buf[i] == '\0') {
     								continue;
     							}
     							else if (bytesRead <= batchSize && buf[i] == '\n') {
         							isSlashR = false;
         							isSlashN = true;
     								continue;
     							}
     							else {
     								isSlashR = false;
     							}
     						}
     						else if (isSlashN) {
     							isSlashN = false;
     							if (bytesRead <= batchSize && isUTF16 && buf[i] == '\0') {
     								continue;
     							}
     						}
     						
     						if (bytesRead > batchSize && (buf[i] == '\n' || buf[i] == '\r')) {
     							rawData[index++] = buf[i];
     							if (isUTF16) {
     								rawData[index++] = '\0'; 
     							}
     							
     							if (buf[i] == '\r') {
     								if (isUTF16) {
     									rawData[index++] = '\n';
     									rawData[index] = '\0';
     								}
     								else {
     									rawData[index] = '\n';
     								}
     							}
     							
     							isDone = true;     							
     							break;
     						}
     						
     						rawData[index++] = buf[i];
     					}
     					else if (buf[i] == '\r') {
     						ignoredFirstLine = true;
     						isSlashR = true;
     					}
     					else if (buf[i] == '\n') {
     						ignoredFirstLine = true;
     						isSlashN = true;
     					}
     				}
     			}
     			
         		logger.info("{} loaded {} bytes (actually read {} bytes)", timer.getChildDurationDesc("Read raw data"), index, bytesRead);
         		
         		jobStatus.setInputBytes(bytesRead);
//         		JobStatusDbService.save(jobStatus);
     		}

     		if(!jobStatus.isTimedOut()){
         		timer.startChild("Process data");
     			File outputFile = new File(CommonConstants.LOCAL_TEMP_DIR, "output.csv");
     			DataProcessor processor = RedshiftDataProcessorBuilder.create()
     					.setRawData(rawData)
     					.setJobId(jobId)
     					.setBatchId(batchId)
     					.setBucketName(srcBucketName)
     					.setObjectKey(objectKey)
     					.setOffset(0)
     					.setLength(index)
     					.setEncoding(encoding)
     					.setDelimiter(delimiter)
     					.setQuote(quote)
     					.setOutputFile(outputFile)
     					.setColumnInfos(columnInfos)
     					.setCommands(commands).setColumnNames(columnNames)
     					.setOriginalColumnNames(originalColumnNames)
     					.setContext(context)
     					.setJobStatus(jobStatus).setPostBackInterval(postBackInterval).build();
     			
     			processor.process();
     			logger.info(timer.getChildDurationDesc("Process data"));
         		
         		timer.startChild("Upload to S3");
         		String outputFileObjectKey = CommonUtils.createOutputFileObjectKey(outputFolderObjectKey, outputFilePrefix, batchId);
         		destS3.uploadFile(destBucketName, outputFileObjectKey, outputFile);
         		logger.info(timer.getChildDurationDesc("Upload to S3"));
         		
     			jobStatus.setOutputFileObjectKey(outputFileObjectKey);
     			jobStatus.setJobState(JobState.SUCCEEDED);
     		}
     		
		}
		catch(OutOfMemoryError e){
			jobStatus.setJobState(JobState.TIMEDOUT);
			try {
				logger.error(e.getMessage());
				JobStatusDbService.save(jobStatus);
			} catch (Exception e1) {
				logger.error("Could not save timedout status after out of memory error.");
				logger.error(e.getMessage(), e1);
			}
		}
		catch(AmazonS3Exception e){
			if(e.getErrorCode().equalsIgnoreCase("InvalidRange")){
				try {
					JobStatusDbService.delete(jobStatus);
				} catch (Exception e1) {
					handleFailedException(jobStatus, e1);
				}
			}else{
				handleFailedException(jobStatus, e);
			}
		}
		catch (Exception e) {
			handleFailedException(jobStatus, e);
		}
		finally {
			try {
				jobStatus.setEndTime(new Date());
				JobStatusDbService.saveJobStatusWithColumnInfos(jobStatus);
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
		
		return null;
	}
	
	private void handleFailedException(JobStatus jobStatus, Exception e){
		logger.error(e.getMessage(), e);
//		output.setThrowable(e);
		jobStatus.setError(e.getMessage());
		jobStatus.setErrorDetails(CommonUtils.convertToString(e));
		jobStatus.setJobState(JobState.FAILED);
		try {
			JobStatusDbService.save(jobStatus);
		} catch (Exception e1) {
			logger.error("Could not save error status after exception.");
			logger.error(e.getMessage(), e1);
		}
	}
}

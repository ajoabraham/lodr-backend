package io.lodr.lambda.function;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lodr.common.util.JobStatusDbService;

public final class CleanDataFunction {
	private static final Logger logger = LoggerFactory.getLogger(CleanDataFunction.class);
	
	public CleanDataFunction() {		
	}
	
	public void cleanData() {
		logger.info("Start cleaning data - {}", Instant.now().toString());
		
		try {
			// Clean up JobStatus table
			JobStatusDbService.cleanData(24);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}

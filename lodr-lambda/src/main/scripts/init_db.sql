-- A view of data loading errors
create view loadview as
(select distinct tbl, 
        trim(name) as table_name, 
        query, 
        starttime,
		trim(filename) as input, 
		line_number, 
		colname, 
		err_code,
		trim(err_reason) as reason
 from stl_load_errors sl, stv_tbl_perm sp
 where sl.tbl = sp.id);
 
 create table vero_schema_info (
   id 					int identity(0, 1),
   table_name			varchar(200),
   sqls 				varchar(65535),
   data_type_infos 		varchar(65535),
   created_on 			date default current_date
 );
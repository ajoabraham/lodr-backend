/**
 * Deploy AWS Lambda functions
 */

var AWS = require('aws-sdk');
var lambda = new AWS.Lambda();
    
exports.handler = function(event, context) {
    console.log("Deploying new jar file...")

    key = event.Records[0].s3.object.key
    bucket = event.Records[0].s3.bucket.name
//    version = event.Records[0].s3.object.versionId
    if (bucket == "lodr-lambda" && key == "builds/lodr-lambda-1.0.0.jar") {
        var functionNames = ["SampleSourceFunction", "SampleWranglerSourceFunction", 
                             "ProcessDataFunction", "CleanDataFunction",
                             "ProcessDataInMemoryFunction", "ParseDateTimeFunction"];
        var updateCallback = function(err, data) {
            if (err) {
                console.log(err, err.stack);
                context.fail(err);
            } else {
                console.log(data);
                context.succeed(data);
            }
        };
        
        for (var i in functionNames) {
            console.log("updating lambda function: " + functionNames[i]);
            var params = {
                FunctionName: functionNames[i],
                S3Key: key,
                S3Bucket: bucket,
            };
            lambda.updateFunctionCode(params, updateCallback);
        }
    } else {
        context.succeed("skipping jar " + key + " in bucket " + bucket + " with version " + version);
    }
};
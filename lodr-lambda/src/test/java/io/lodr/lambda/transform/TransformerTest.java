package io.lodr.lambda.transform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.model.ColumnData;
import io.lodr.common.model.RowData;
import io.lodr.common.model.command.Command;
import io.lodr.common.model.command.DeleteCommand;
import io.lodr.common.model.command.ExtractJsonCommand;
import io.lodr.common.util.JsonUtils;

public class TransformerTest {
	RowData row = new RowData();
	
	@Before
	public void setUp() throws Exception {
		row.getColumnDatas().add(new ColumnData("COLUMN_0", "199.72.81.55 - - [01/Jul/1995:00:00:01 -0400] \"GET /history/apollo/ HTTP/1.0\" 200 6245", 0));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTransformationNASAData() {
		try (BufferedReader in = new BufferedReader(new InputStreamReader(TransformerTest.class.getResourceAsStream("nasa_transform_commands.txt")))) {
			List<Command> commands = JsonUtils.toObject(new TypeReference<List<Command>>() {}, in.readLine());
			Transformer trans = Transformer.newInstance(commands);
			List<RowData> transformedRows = trans.transform(row);
			
			for (RowData r : transformedRows) { 
    			for (ColumnData col : r.getColumnDatas()) {
    				System.out.println("column Name: " + col.getName() + " value: " + col.getValue() + " position: " + col.getPosition());
    			}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testTransformJsonData() throws IOException {
		try (BufferedReader in = new BufferedReader(new InputStreamReader(TransformerTest.class.getResourceAsStream("ExtractJsonCommandTestInput.json")))) {
			String json = in.readLine();
			
			RowData rowData = new RowData();
			rowData.getColumnDatas().add(new ColumnData("my_json", json, 0));
			
			ExtractJsonCommand command = new ExtractJsonCommand();
			
			command.setColumnName("my_json");
			command.getOpts().put("path", "$.labs[*].name, $.labs[*].location, $.medications[*].antianginal[*].strength");
			command.getOpts().put("retain_column", "on");
			command.getOpts().put("expand_array", "on");
			
			DeleteCommand deleteCommand = new DeleteCommand();
			deleteCommand.setColumnName("my_json");
			
			Transformer trans = Transformer.newInstance(Arrays.asList(command, deleteCommand));
			List<RowData> transformedRows = trans.transform(rowData);
			
			transformedRows.forEach(r -> r.getColumnDatas().forEach(c -> System.out.println("Name = " + c.getName() + " value = " + c.getValue())));
		}
	}
	
	@Test
	public void testTransformJsonDataColName() throws IOException {
		InputStream x = TransformerTest.class.getResourceAsStream("ExtractJsonCommandTestInput.json");
		x.available();
		try (BufferedReader in = new BufferedReader(new InputStreamReader(TransformerTest.class.getResourceAsStream("ExtractJsonCommandTestInput.json")))) {
			String json = in.readLine();
			
			RowData rowData = new RowData();
			rowData.getColumnDatas().add(new ColumnData("my_json", json, 0));
			
			ExtractJsonCommand command = new ExtractJsonCommand();
			
			command.setColumnName("my_json");
			command.getOpts().put("path", "$.medications[0]..[*].name, $.labs[*].name, $.labs[*].time,$[*],$[*]");
			command.getOpts().put("retain_column", "on");
			command.getOpts().put("expand_array", "on");
			
			DeleteCommand deleteCommand = new DeleteCommand();
			deleteCommand.setColumnName("my_json");
			
			Transformer trans = Transformer.newInstance(Arrays.asList(command, deleteCommand));
			List<RowData> transformedRows = trans.transform(rowData);
			
			transformedRows.forEach(r -> r.getColumnDatas().forEach(c -> System.out.println("Name = " + c.getName() + " value = " + c.getValue())));
		}
	}

}

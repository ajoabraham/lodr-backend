package io.lodr.lambda.function;

import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.amazonaws.services.lambda.model.InvokeResult;
import com.fasterxml.jackson.core.JsonProcessingException;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.OutputData;
import io.lodr.common.model.SampleSourceInputData;
import io.lodr.common.util.AmazonLambdaService;
import io.lodr.common.util.JsonUtils;
import io.lodr.lambda.common.TestContext;
import io.lodr.lambda.function.SampleSourceFunction;

public class SampleSourceFunctionTest {
	private final SampleSourceInputData input = new SampleSourceInputData();

	@Before
	public void setUp() throws Exception {
		input.setBucketName("lodr-lambda");
		input.setUserId("test-data");
//		input.setObjectKey("test-data/uploads/2016.10.09.14.20.15.txt");
//		input.setObjectKey("test-data/uploads/bad_data_small.csv");
//		input.setObjectKey("test-data/uploads/100_NASA_access_log_Jul95.log");
//		input.setObjectKey("test-data/uploads/projects.csv");
//		input.setObjectKey("test-data/uploads/system.log");
//		input.setObjectKey("test-data/uploads/activity.i-2335830e.2017-01-10-05-10-56.log");
//		input.setObjectKey("test-data/uploads/booking_mstr_dim_base.zip");
		input.setObjectKey("test-data/uploads/with_comments.csv");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLocally() throws JsonProcessingException {
		SampleSourceFunction func = new SampleSourceFunction();
		
		System.out.println("Input = " + JsonUtils.toJsonString(input));
		
		OutputData output = func.handleRequest(input, new TestContext());
		
		System.out.println("output = " + JsonUtils.toJsonString(output));
	}
	
	@Test
	public void testRemotely() throws Exception {
		InvokeResult invokeResult = AmazonLambdaService.I.invoke(CommonConstants.SAMPLE_SOURCE_FUNCTION, CommonConstants.APP_MODE.getLambdaAlias(), input);
		
		if (invokeResult.getStatusCode() == 200) {
			System.out.println(new String(invokeResult.getPayload().array(), StandardCharsets.UTF_8));	
		}			
		else if (invokeResult.getFunctionError() != null) {
			System.out.println(invokeResult.getFunctionError());
		}
		else {
			System.out.println("Request failed with status code - " + invokeResult.getStatusCode());
		}
	}
}

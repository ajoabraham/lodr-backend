package io.lodr.lambda.function;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.lodr.common.model.ConnInfo;
import io.lodr.common.model.TestConnectionOutputData;
import io.lodr.common.util.JsonUtils;
import io.lodr.lambda.common.TestContext;
import io.lodr.lambda.function.TestConnectionFunction;

public class TestConnectionFunctionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JsonProcessingException {
		ConnInfo connInfo = new ConnInfo();
		connInfo.setServerAddress("vero-customer-dw-instance.cul81qssgn06.us-west-2.redshift.amazonaws.com");
		connInfo.setDatabaseName("lodr");
		connInfo.setUsername("veroadmin");
		connInfo.setPassword("ojAniluYiaT88");
		Map<String, String> props = new HashMap<>();
		props.put("ssl", "true");
		props.put("sslfactory", "com.amazon.redshift.ssl.NonValidatingFactory");
		connInfo.setProps(props);
		
		TestConnectionFunction func = new TestConnectionFunction();
		
		TestConnectionOutputData output = func.handleRequest(connInfo, new TestContext());
		
		System.out.println(JsonUtils.toJsonString(output));
		
		assertEquals(true, output.isSucceeded());
	}

}

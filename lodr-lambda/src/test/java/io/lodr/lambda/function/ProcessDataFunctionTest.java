package io.lodr.lambda.function;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.model.JobStatus;
import io.lodr.common.model.OutputData;
import io.lodr.common.model.ProcessDataInputData;
import io.lodr.common.model.RunJobInputData;
import io.lodr.common.model.command.Command;
import io.lodr.common.util.AmazonLambdaService;
import io.lodr.common.util.JobStatusDbService;
import io.lodr.common.util.JsonUtils;
import io.lodr.lambda.common.TestContext;

public class ProcessDataFunctionTest {
	private final String bucketName = "lodr-lambda";
	private final String objectKey = "test-data/uploads/2016.10.09.14.20.15.txt";
	// private final String objectKey =
	// "test-data/uploads/LCMemberExport_20161009.txt";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMultiprocessPerformance() throws Exception {
		long totalStartTime = System.currentTimeMillis();

		final AmazonLambdaService lambda = AmazonLambdaService.I;

		List<Future<OutputData>> futures = new ArrayList<>();
		ExecutorService threadPool = Executors.newFixedThreadPool(8);

		for (int i = 0; i < 20; i++) {
			final int startPosition = i;
			Future<OutputData> future = threadPool.submit(() -> {
				ProcessDataFunctionProxy func = lambda.getLambdaFunction(ProcessDataFunctionProxy.class);
				ProcessDataInputData input = createInput();
				System.out.println("Call lambda function (" + startPosition + ")...");
				long startTime = System.currentTimeMillis();
				OutputData output = func.processData(input);
				System.out.println("Lambda function (" + startPosition + ") took "
						+ (System.currentTimeMillis() - startTime) + "ms");
				return output;
			});

			futures.add(future);
		}

		futures.forEach(f -> {
			try {
				f.get();
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			catch (ExecutionException e) {
				e.printStackTrace();
			}
		});

		System.out.println("Total time = " + (System.currentTimeMillis() - totalStartTime) + "ms");
	}

	private static final String INPUT_JSON = "{\"userId\":1,\"encoding\":\"UTF-16LE\",\"delimiter\":\",\",\"quote\":\"\\\"\",\"objectKey\":\"test-data/uploads/vero_booking_status_dim.csv\",\"includesHeader\":true,\"accessId\": null,\"accessKey\": null,\"region\": null,\"bucketName\":\"lodr-lambda\",\"originalColumnNames\":[\"\\\"Booking_id\\\"\",\"Booking Status\",\"Bookings\"],\"tableName\":\"vero_booking_status_dim\",\"loadMode\":\"REPLACE\",\"primaryKeys\":[\"booking_id\"],\"mergeKeys\":null,\"sortKeys\":[],\"distKey\":null,\"distStyle\":\"EVEN\",\"columnInfos\":[{\"original_index\":0,\"columnPosition\":0,\"columnName\":\"booking_id\",\"dataType\":\"INTEGER\",\"length\":0,\"precision\":0,\"scale\":0,\"nullValue\":false,\"width\":170},{\"original_index\":1,\"columnPosition\":1,\"columnName\":\"booking_status\",\"dataType\":\"VARCHAR\",\"length\":0,\"precision\":0,\"scale\":0,\"nullValue\":false,\"width\":190},{\"original_index\":2,\"columnPosition\":2,\"columnName\":\"bookings\",\"dataType\":\"INTEGER\",\"length\":0,\"precision\":0,\"scale\":0,\"nullValue\":false,\"width\":160}],\"wrangleScript\":[{\"command\":\"rename\",\"columnName\":\"\\\"Booking_id\\\"\",\"opts\":{\"new_column_name\":\"booking_id\",\"col_name\":\"\\\"Booking_id\\\"\"}},{\"command\":\"rename\",\"columnName\":\"Booking Status\",\"opts\":{\"new_column_name\":\"booking_status\"}},{\"command\":\"rename\",\"columnName\":\"Bookings\",\"opts\":{\"new_column_name\":\"bookings\"}}],\"connInfo\":{\"name\":\"lhw redshift2\",\"serverAddress\":\"vero-customer-dw-instance.cul81qssgn06.us-west-2.redshift.amazonaws.com\",\"port\":5439,\"databaseName\":\"lodr\",\"username\":\"veroadmin\",\"password\":\"ojAniluYiaT88\",\"jdbcURL\":\"\",\"userId\":1,\"props\":{\"ssl\":\"true\",\"sslfactory\":\"com.amazon.redshift.ssl.NonValidatingFactory\"}},\"jobId\":\"73fd4f4b-2d81-4283-aca2-cf5cd41ac251\"}";
	@Test
	public void testUTF16EncodedFile() throws Exception {
		RunJobInputData input = JsonUtils.toObject(new TypeReference<RunJobInputData>(){}, INPUT_JSON);
		
		ProcessDataInputData processDataInput = new ProcessDataInputData();
		
		processDataInput.setJobId(input.getJobId());
		processDataInput.setBatchId(0);
		processDataInput.setBucketName(input.getBucketName());
		processDataInput.setObjectKey(input.getObjectKey());
		processDataInput.setUserId(input.getUserId());
		processDataInput.setDelimiter(input.getFileInfos().get(0).getDelimiter());
		processDataInput.setQuote(input.getFileInfos().get(0).getQuote());
		processDataInput.setEncoding(input.getFileInfos().get(0).getEncoding());
		
		processDataInput.setColumnInfos(input.getColumnInfos());
		processDataInput.setOutputFilePrefix(UUID.randomUUID().toString());
		processDataInput.setIncludesHeader(true);
		processDataInput.setWrangleScript(JsonUtils.toJsonString(input.getCommands(), new TypeReference<List<Command>>() {}));
		processDataInput.setOriginalColumnNames(input.getOriginalColumnNames());
		
		JobStatus jobStatus = new JobStatus();
		jobStatus.setStartPosition(0);
		jobStatus.setJobId(processDataInput.getJobId());
		jobStatus.setBatchId(processDataInput.getBatchId());
		JobStatusDbService.save(jobStatus);
		
		ProcessDataFunction func = new ProcessDataFunction();
		func.handleRequest(processDataInput, new TestContext());
	}
	
	private static final String FILE_WITH_SPLIT_COMMAND_JSON = "{\"originalColumnNames\":[\"COLUMN_0\"],\"tableName\":\"NASA_access_log_Jul95\",\"loadMode\":\"REPLACE\",\"primaryKeys\":[],\"mergeKeys\":null,\"sortKeys\":[],\"distKey\":null,\"distStyle\":\"EVEN\",\"preSql\":\"\",\"postSql\":\"\",\"columnInfos\":[{\"columnName\":\"COLUMN_0_0\",\"dataType\":\"VARCHAR\"},{\"columnName\":\"COLUMN_0_1\",\"dataType\":\"VARCHAR\"}],\"wrangleScript\":[{\"command\":\"split_delimiter\",\"columnName\":\"COLUMN_0\",\"opts\":{\"delimiter\":\"- -\",\"limit\":2,\"case_insensitive\":\"on\"}}],\"connInfo\":{\"name\":\"Lodr Redshift\",\"serverAddress\":\"vero-customer-dw-instance.cul81qssgn06.us-west-2.redshift.amazonaws.com\",\"port\":5439,\"databaseName\":\"lodr\",\"username\":\"veroadmin\",\"password\":\"ojAniluYiaT88\",\"jdbcURL\":\"\",\"userId\":2,\"props\":{\"ssl\":\"true\",\"sslfactory\":\"com.amazon.redshift.ssl.NonValidatingFactory\"}},\"fileInfos\":[{\"userId\":2,\"sampleRowCount\":500,\"encoding\":\"ISO-8859-1\",\"delimiter\":\"no_delimiter\",\"quote\":\"\\\"\",\"objectKey\":\"2/uploads/100_NASA_access_log_Jul95.log\",\"includesHeader\":false,\"accessId\":\"AKIAJX2HXHBWEJY7U6EA\",\"accessKey\":\"GbeN/YCc84KeQ9DAknmNXebAMbyc3qhAMM9u83as\",\"region\":\"us-west-2\",\"bucketName\":\"lodr-user-uploads-prod\"}],\"jobId\":\"2b8b10de-46f3-4291-af6f-c5cec994a9cf\",\"userId\":2}";
	@Test
	public void testFileWithSplitCommand() throws Exception {
		RunJobInputData input = JsonUtils.toObject(new TypeReference<RunJobInputData>(){}, FILE_WITH_SPLIT_COMMAND_JSON);
		
		ProcessDataInputData processDataInput = new ProcessDataInputData();
		
		processDataInput.setJobId(input.getJobId());
		processDataInput.setBatchId(0);
		processDataInput.setBucketName(input.getFileInfos().get(0).getBucketName());
		processDataInput.setObjectKey(input.getFileInfos().get(0).getObjectKey());
		processDataInput.setUserId(input.getFileInfos().get(0).getUserId());
		processDataInput.setDelimiter(input.getFileInfos().get(0).getDelimiter());
		processDataInput.setQuote(input.getFileInfos().get(0).getQuote());
		processDataInput.setEncoding(input.getFileInfos().get(0).getEncoding());
		processDataInput.setColumnInfos(input.getColumnInfos());
		processDataInput.setOutputFilePrefix(UUID.randomUUID().toString());
		processDataInput.setIncludesHeader(true);
		processDataInput.setWrangleScript(JsonUtils.toJsonString(input.getCommands(), new TypeReference<List<Command>>() {}));
		processDataInput.setOriginalColumnNames(input.getOriginalColumnNames());
		
		JobStatus jobStatus = new JobStatus();
		jobStatus.setStartPosition(0);
		jobStatus.setJobId(processDataInput.getJobId());
		jobStatus.setBatchId(processDataInput.getBatchId());
		JobStatusDbService.save(jobStatus);
		
		ProcessDataFunction func = new ProcessDataFunction();
		func.handleRequest(processDataInput, new TestContext());
	}

	private ProcessDataInputData createInput() {
		ProcessDataInputData input = new ProcessDataInputData();

		input.setBucketName(bucketName);
		input.setObjectKey(objectKey);
		input.setDelimiter("\t");
		input.setQuote("\"");
		input.setEncoding(StandardCharsets.UTF_8.name());

		return input;
	}

}

package io.lodr.lambda.function;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.lodr.common.model.CountRowInputData;
import io.lodr.common.model.OutputData;
import io.lodr.common.util.JsonUtils;
import io.lodr.lambda.common.TestContext;

public class CountLineFunctionTest {
	private final CountRowInputData input = new CountRowInputData();

	@Before
	public void setUp() throws Exception {
		input.setBucketName("lodr-lambda");
//		input.setObjectKey("test-data/uploads/LCMemberExport_20161009.txt");
		input.setObjectKey("test-data/uploads/ThreeMillionRows.csv");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JsonProcessingException {
		CountRowFunction func = new CountRowFunction();
		
		OutputData output = func.handleRequest(input, new TestContext());
		
		System.out.println("output = " + JsonUtils.toJsonString(output));
	}

}

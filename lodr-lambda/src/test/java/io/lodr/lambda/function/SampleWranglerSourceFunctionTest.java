package io.lodr.lambda.function;

import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.lodr.common.model.OutputData;
import io.lodr.common.model.SampleWranglerSourceInputData;
import io.lodr.common.util.JsonUtils;
import io.lodr.lambda.common.TestContext;

public class SampleWranglerSourceFunctionTest {
	private SampleWranglerSourceInputData input = new SampleWranglerSourceInputData();
	
	@Before
	public void setUp() throws Exception {
		input.setBucketName("lodr-lambda");
		input.setUserId("test-data");
//		input.setObjectKey("test-data/uploads/2016.10.09.14.20.15.txt");
//		input.setObjectKey("test-data/uploads/LCMemberExport_20161009.txt");
//		input.setObjectKey("test-data/uploads/bad_data_small.csv");
//		input.setObjectKey("test-data/uploads/projects.csv");
//		input.setDelimiter("|");
//		input.setQuote("\"");
//		input.setEncoding(StandardCharsets.UTF_8.name());
		input.setObjectKey("test-data/uploads/with_comments.csv");
		input.setDelimiter(";");
		input.setQuote("\"");
		input.setEncoding(StandardCharsets.ISO_8859_1.name());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JsonProcessingException {
		SampleWranglerSourceFunction func = new SampleWranglerSourceFunction();
		
		OutputData output = func.handleRequest(input, new TestContext());
		
		System.out.println(JsonUtils.toJsonString(output));
	}

}

package io.lodr.lambda.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Equator;
import org.junit.Test;

import io.lodr.common.constant.DataType;
import io.lodr.common.model.ColumnInfo;

public class ColumnComparisonTest {

	@Test
	public void testRemoveAllColumns() {
		List<ColumnInfo> currentColumnInfos = new ArrayList<>();
		List<ColumnInfo> previousColumnInfos = new ArrayList<>();
		
		for (int i = 0; i < 10; i++) {
			ColumnInfo current = new ColumnInfo("COLUMN_NAME_" + i, i, DataType.VARCHAR);
			ColumnInfo previous = new ColumnInfo("COLUMN_NAME_" + i, i, DataType.VARCHAR);
			currentColumnInfos.add(current);
			previousColumnInfos.add(previous);
		}
		
		Collection<ColumnInfo> newColumnInfos = CollectionUtils.removeAll(currentColumnInfos, previousColumnInfos, new Equator<ColumnInfo>() {
			@Override
			public boolean equate(ColumnInfo o1, ColumnInfo o2) {
				return o1.getColumnName().equals(o2.getColumnName());
			}

			@Override
			public int hash(ColumnInfo o) {
				return o.hashCode();
			}		
		});
		
		assertEquals(0, newColumnInfos.size());
	}

}

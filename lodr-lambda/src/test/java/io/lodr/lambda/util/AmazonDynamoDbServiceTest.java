package io.lodr.lambda.util;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import io.lodr.common.constant.JobState;
import io.lodr.common.model.JobStatus;
import io.lodr.common.util.AmazonDynamoDbService;

public class AmazonDynamoDbServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testItemLoadAndSave() throws Exception {
		JobStatus jobStatus = TestUtils.createJobStatus();
		
//		DynamoDBMapper mapper = AmazonDynamoDbService.I.getDynamoDbMapper();
		
		AmazonDynamoDbService.I.save(jobStatus);
		
		JobStatus savedJobStatus = AmazonDynamoDbService.I.load(JobStatus.class, jobStatus.getJobId(), 0);
		
		assertEquals(true, savedJobStatus.getJobState() == JobState.SUCCEEDED);
//		assertEquals(3, savedJobStatus.getInvalidRowNums().size());
	}

	@Test
	public void testBatchRead() throws Exception {
		Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":val1", new AttributeValue().withS("e6948fcb-07fa-45fc-b2c4-4726bac7e1cd"));
        eav.put(":val2", new AttributeValue().withS(JobState.SUCCEEDED.toString()));
		DynamoDBQueryExpression<JobStatus> query = new DynamoDBQueryExpression<JobStatus>()
				.withKeyConditionExpression("jobId = :val1")
				.withFilterExpression("jobState = :val2")
				.withExpressionAttributeValues(eav);
		List<JobStatus> jobs = AmazonDynamoDbService.I.query(JobStatus.class, query);
		
		System.out.println("jobs = " + jobs.size());
	}
}

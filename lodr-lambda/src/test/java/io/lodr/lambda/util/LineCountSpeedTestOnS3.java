package io.lodr.lambda.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.lodr.common.util.AmazonS3Service;

public class LineCountSpeedTestOnS3 {
	private static final String BUCKET_NAME = "lodr-lambda";
//	private static final String OBJECT_KEY = "test-data/2016.10.09.14.20.15.txt";
	private static final String OBJECT_KEY = "test-data/LCMemberExport_20161009.txt";
	
	private static int BUFFER_SIZE = 1024 * 1024;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void useBufferedReader() throws Exception {
		long startTime = System.currentTimeMillis();
		int count = 0;
		
		AmazonS3Service s3 = AmazonS3Service.newInstance(null, null);
		
		try (BufferedReader in = new BufferedReader(new InputStreamReader(s3.getObject(BUCKET_NAME, OBJECT_KEY).getObjectContent()))) {
			while (in.readLine() != null) {
				count++;
			}
		}
		System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to load " + count + " rows by using BufferedReader");
	}
	
	@Test
	public void useLineNumberReader() throws Exception {
		long startTime = System.currentTimeMillis();
		int count = 0;
		
		AmazonS3Service s3 = AmazonS3Service.newInstance(null, null);
		try (LineNumberReader in = new LineNumberReader(new InputStreamReader(s3.getObject(BUCKET_NAME, OBJECT_KEY).getObjectContent(), Charset.defaultCharset()), BUFFER_SIZE)) {
			in.skip(Long.MAX_VALUE);
			count = in.getLineNumber();
		}
		System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to load " + count + " rows by useing LineNumberReader");
	}
	
	@Test
	public void useByteArray() throws Exception {
		long startTime = System.currentTimeMillis();
		int count = 0;

		AmazonS3Service s3 = AmazonS3Service.newInstance(null, null);
		try (InputStream fis = s3.getObject(BUCKET_NAME, OBJECT_KEY).getObjectContent()) {
    	    byte[] buffer = new byte[BUFFER_SIZE];
    	    int read;
    
    	    while ((read = fis.read(buffer)) != -1) {
    	        for (int i = 0; i < read; i++) {
    	            if (buffer[i] == '\n') count++;
    	        }
    	    }
		}

	    System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to load " + count + " rows by using byte array");
	}
}

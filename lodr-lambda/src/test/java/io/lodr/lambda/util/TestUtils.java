package io.lodr.lambda.util;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.UUID;

import io.lodr.common.constant.DataType;
import io.lodr.common.constant.JobState;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.JobStatus;
import io.lodr.common.model.ProcessDataInputData;

public final class TestUtils {
	private TestUtils() {
		
	}
	
	public static JobStatus createJobStatus() {
		JobStatus jobStatus = new JobStatus();
		String jobId = UUID.randomUUID().toString();
		jobStatus.setJobId(jobId);
		jobStatus.setBatchId(0);
		jobStatus.setJobState(JobState.SUCCEEDED);
		jobStatus.setStartTime(new Date());
//		jobStatus.setInvalidRowNums(Arrays.asList(3L, 6L ,9L));
		for (int i = 0; i < 2; i++) {
			ColumnInfo columnInfo = new ColumnInfo("col_" + i, i, DataType.VARCHAR);
			jobStatus.getColumnInfos().add(columnInfo);
		}
		
		ProcessDataInputData inputData = new ProcessDataInputData();
		inputData.setAccessId("testaccessid");
		inputData.setAccessKey("testaccesskey");
		inputData.setBatchId(0);
		inputData.setBucketName("lodr-lambda");
		inputData.setColumnInfos(jobStatus.getColumnInfos());
		inputData.setDelimiter("|");
		inputData.setEncoding(StandardCharsets.ISO_8859_1.toString());
		inputData.setJobId(jobId);
		inputData.setObjectKey("test-data/uploads/test.csv");
		inputData.setOutputFilePrefix("prefix");
		inputData.setQuote("\"");
		inputData.setRegion("us-east-1");
		jobStatus.setStartPosition(10000);
		inputData.setUserId("test-user-id");
		
		jobStatus.setInputData(inputData);
		
		return jobStatus;
	}
}

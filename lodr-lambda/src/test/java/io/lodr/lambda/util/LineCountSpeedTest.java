package io.lodr.lambda.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LineCountSpeedTest {
	private static final String INPUT_FILE_NAME = "2016.10.09.14.20.15.txt";
	private static int BUFFER_SIZE = 8 * 1024;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void useBufferedReader() throws FileNotFoundException, IOException {
		long startTime = System.currentTimeMillis();
		int count = 0;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(INPUT_FILE_NAME)))) {
			while (in.readLine() != null) {
				count++;
			}
		}
		System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to load " + count + " rows by using BufferedReader");
	}
	
	@Test
	public void useLineNumberReader() throws FileNotFoundException, IOException {
		long startTime = System.currentTimeMillis();
		int count = 0;
		try (LineNumberReader in = new LineNumberReader(new FileReader(INPUT_FILE_NAME), BUFFER_SIZE)) {
			in.skip(Long.MAX_VALUE);
			count = in.getLineNumber();
		}
		System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to load " + count + " rows by useing LineNumberReader");
	}
	
	@Test
	public void useByteArray() throws IOException {
		long startTime = System.currentTimeMillis();
		int count = 0;

		try (FileInputStream fis = new FileInputStream(INPUT_FILE_NAME)) {
    	    byte[] buffer = new byte[BUFFER_SIZE];
    	    int read;
    
    	    while ((read = fis.read(buffer)) != -1) {
    	        for (int i = 0; i < read; i++) {
    	            if (buffer[i] == '\n') count++;
    	        }
    	    }
		}

	    System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to load " + count + " rows by using byte array");
	}
	
	@Test
	public void useNio() throws IOException {
		long startTime = System.currentTimeMillis();
	    long count;
	    try (Stream<String> s = Files.lines(Paths.get(INPUT_FILE_NAME), Charset.defaultCharset())) {
	        count = s.count();
	    } 

	    System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to load " + count + " rows by using java.nio");
	}

}

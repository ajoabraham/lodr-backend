#!/bin/bash

if [ $# -ne 2 ] 
then
    echo "Usage: create_alias.sh <alias name> <version>"
    exit
fi

functionNames=("SampleWranglerSourceFunction" "SampleSourceFunction" "ProcessDataFunction "ProcessDataInMemoryFunction")
aliasName=$1
version=$2

for n in "${functionNames[@]}"
do
    echo `aws lambda create-alias --function-name $n --name $aliasName --function-version $version`
done

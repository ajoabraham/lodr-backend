import com.google.inject.AbstractModule;

import play.Configuration;
import play.Environment;

import static io.lodr.common.constant.ConfigParam.*;

public class Module extends AbstractModule {

//	private final Environment environment;
	private final Configuration config;
	    
	public Module(Environment environment, Configuration config) {
//		this.environment = environment;
		this.config = config;
	}

	@Override
	protected void configure() {		
		/*
		 * appMode
		 * accessId
		 * accessKey
		 * defaultRegion
		 * uploadBucket
		 * jobStatusTbl
		 * columnInfoTbl
		 * canceledJobTbl
		 * canceledQueryTbl
		 * processDataFunction
		 * processDataInMemoryFunction
		 * sampleSourceFunction
		 * sampleWranglerSourceFunction
		 */
		
		if (System.getProperty(PARAM_APP_MODE.getName()) == null && System.getenv(PARAM_APP_MODE.getName()) == null
				&& config.getString(PARAM_APP_MODE.getName()) != null) {
			System.setProperty(PARAM_APP_MODE.getName(), config.getString(PARAM_APP_MODE.getName()));
		}
		
		if (System.getProperty(PARAM_ACCESS_ID.getName()) == null && System.getenv(PARAM_ACCESS_ID.getName()) == null
				&& config.getString(PARAM_ACCESS_ID.getName()) != null) {
			System.setProperty(PARAM_ACCESS_ID.getName(), config.getString(PARAM_ACCESS_ID.getName()));
		}
		
		if (System.getProperty(PARAM_ACCESS_KEY.getName()) == null && System.getenv(PARAM_ACCESS_KEY.getName()) == null
				&& config.getString(PARAM_ACCESS_KEY.getName()) != null) {
			System.setProperty(PARAM_ACCESS_KEY.getName(), config.getString(PARAM_ACCESS_KEY.getName()));
		}
		
		if (System.getProperty(PARAM_DEFAULT_REGION.getName()) == null && System.getenv(PARAM_DEFAULT_REGION.getName()) == null
				&& config.getString(PARAM_DEFAULT_REGION.getName()) != null) {
			System.setProperty(PARAM_DEFAULT_REGION.getName(), config.getString(PARAM_DEFAULT_REGION.getName()));
		}
		
		if (System.getProperty(PARAM_UPLOAD_BUCKET.getName()) == null && System.getenv(PARAM_UPLOAD_BUCKET.getName()) == null
				&& config.getString(PARAM_UPLOAD_BUCKET.getName()) != null) {
			System.setProperty(PARAM_UPLOAD_BUCKET.getName(), config.getString(PARAM_UPLOAD_BUCKET.getName()));
		}
		
		if (System.getProperty(PARAM_JOB_STATUS_TBL.getName()) == null && System.getenv(PARAM_JOB_STATUS_TBL.getName()) == null
				&& config.getString(PARAM_JOB_STATUS_TBL.getName()) != null) {
			System.setProperty(PARAM_JOB_STATUS_TBL.getName(), config.getString(PARAM_JOB_STATUS_TBL.getName()));
		}
		
		if (System.getProperty(PARAM_COLUMN_INFO_TBL.getName()) == null && System.getenv(PARAM_COLUMN_INFO_TBL.getName()) == null
				&& config.getString(PARAM_COLUMN_INFO_TBL.getName()) != null) {
			System.setProperty(PARAM_COLUMN_INFO_TBL.getName(), config.getString(PARAM_COLUMN_INFO_TBL.getName()));
		}
		
		if (System.getProperty(PARAM_CANCELED_JOB_TBL.getName()) == null && System.getenv(PARAM_CANCELED_JOB_TBL.getName()) == null
				&& config.getString(PARAM_CANCELED_JOB_TBL.getName()) != null) {
			System.setProperty(PARAM_CANCELED_JOB_TBL.getName(), config.getString(PARAM_CANCELED_JOB_TBL.getName()));
		}
		
		if (System.getProperty(PARAM_CANCELED_QUERY_TBL.getName()) == null && System.getenv(PARAM_CANCELED_QUERY_TBL.getName()) == null
				&& config.getString(PARAM_CANCELED_QUERY_TBL.getName()) != null) {
			System.setProperty(PARAM_CANCELED_QUERY_TBL.getName(), config.getString(PARAM_CANCELED_QUERY_TBL.getName()));
		}
		
		if (System.getProperty(PARAM_PROCESS_DATA_FUNCTION.getName()) == null && System.getenv(PARAM_PROCESS_DATA_FUNCTION.getName()) == null
				&& config.getString(PARAM_PROCESS_DATA_FUNCTION.getName()) != null) {
			System.setProperty(PARAM_PROCESS_DATA_FUNCTION.getName(), config.getString(PARAM_PROCESS_DATA_FUNCTION.getName()));
		}
		
		if (System.getProperty(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName()) == null && System.getenv(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName()) == null
				&& config.getString(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName()) != null) {
			System.setProperty(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName(), config.getString(PARAM_PROCESS_DATA_IN_MEMORY_FUNCTION.getName()));
		}
		
		if (System.getProperty(PARAM_SAMPLE_SOURCE_FUNCTION.getName()) == null && System.getenv(PARAM_SAMPLE_SOURCE_FUNCTION.getName()) == null
				&& config.getString(PARAM_SAMPLE_SOURCE_FUNCTION.getName()) != null) {
			System.setProperty(PARAM_SAMPLE_SOURCE_FUNCTION.getName(), config.getString(PARAM_SAMPLE_SOURCE_FUNCTION.getName()));
		}
		
		if (System.getProperty(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName()) == null && System.getenv(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName()) == null
				&& config.getString(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName()) != null) {
			System.setProperty(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName(), config.getString(PARAM_SAMPLE_WRANGLER_SOURCE_FUNCTION.getName()));
		}
		
		if (System.getProperty(PARAM_LAMBDA_READ_BATCH_SIZE.getName()) == null && System.getenv(PARAM_LAMBDA_READ_BATCH_SIZE.getName()) == null
				&& config.getString(PARAM_LAMBDA_READ_BATCH_SIZE.getName()) != null) {
			System.setProperty(PARAM_LAMBDA_READ_BATCH_SIZE.getName(), config.getString(PARAM_LAMBDA_READ_BATCH_SIZE.getName()));
		}
	}
}

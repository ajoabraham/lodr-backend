package io.lodr.server.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;

import io.lodr.common.constant.SchemaStatus;
import io.lodr.common.model.ConnInfo;
import io.lodr.common.model.PostBackTableSchema;
import io.lodr.common.model.SchemaColumn;
import io.lodr.common.model.SchemaTable;
import io.lodr.common.model.SchemaTable.TableType;
import io.lodr.common.model.TableSchemaInputData;
import io.lodr.common.model.TableSchemaOutputData;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.ConnUtils;
import io.lodr.common.util.DbUtils;
import io.lodr.common.util.JobManager;
import io.lodr.common.util.JsonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.Validator;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.mvc.BodyParser;
import play.mvc.Result;

public class TableSchemaController extends PostBackController {
	private static final ALogger logger = Logger.of(TableSchemaController.class);
	
	public TableSchemaController() {
		
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result tableSchema() {
		final LodrTimer timer = LodrTimer.newInstance(TableSchemaController.class.getName());
		timer.start();
				
		final TableSchemaOutputData output = new TableSchemaOutputData();
		
		try {   
			final String inputJson = JsonUtils.toJsonString(request().body().asJson());
			logger.info(inputJson);
			
			final TableSchemaInputData input = JsonUtils.toObject(new TypeReference<TableSchemaInputData>(){}, inputJson);
			
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
			
			JobManager.I.execute(new TableSchemaHandler(input, ws));
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			output.setThrowable(e);
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
		
		return output.getError() == null ? ok(Json.stringify(Json.toJson(output)))
				: internalServerError(Json.stringify(Json.toJson(output)));
	}
	
	private static class TableSchemaHandler implements Callable<Void> {
		private static final String SCHEMA_QUERY = "select c.table_name as tablename, c.table_schema as schemaname, 'view' as type, c.column_name as columnname, c.data_type as datatype, c.character_maximum_length as length, c.numeric_precision as precision, c.numeric_scale as scale, td.encoding as compressiontype, td.distkey as distkey, td.sortkey as sortkey, c.ordinal_position as position, v.definition as viewdef "
				+ "from information_schema.columns c, pg_table_def td, pg_views v "
				+ "where c.table_schema %s and c.table_name = '%s' and c.table_schema = td.schemaname and c.table_name = td.tablename and c.column_name = td.\"column\" and td.schemaname = v.schemaname and td.tablename = v.viewname "
				+ "union all "
				+ "select c.table_name as tablename, c.table_schema as schemaname, 'table' as type, c.column_name as columnname, c.data_type as datatype, c.character_maximum_length as length, c.numeric_precision as precision, c.numeric_scale as scale, td.encoding as compressiontype, td.distkey as distkey, td.sortkey as sortkey, c.ordinal_position as position, '' as viewdef "
				+ "from information_schema.columns c, pg_table_def td, pg_tables t "
				+ "where c.table_schema %s and c.table_name = '%s' and c.table_schema = td.schemaname and c.table_name = td.tablename and c.column_name = td.\"column\" and td.schemaname = t.schemaname and td.tablename = t.tablename "
				+ "order by schemaname, tablename, position";
		
		private static final String PRIMARY_KEY_QUERY = "select t.tablename as tablename, t.schemaname as schemaname, c.conkey as primarykeys "
				+ "from pg_tables t, pg_class cls, pg_constraint c "
				+ "where t.schemaname %s and t.tablename = '%s' and t.tablename = cls.relname and cls.relfilenode = c.conrelid and c.contype = 'p'";
		
		private static final String TABLE_SIZE_QUERY = "select \"schema\" as schemaname, \"table\" as tablename, \"size\" as tablesize from svv_table_info where \"schema\" %s and \"table\" = '%s'";
		
		private ConnInfo connInfo = null;
		private String selectedSchemaName = null;
		private String selectedTableName = null;
		private String webhook = null;
		private WSClient ws = null;
		
		public TableSchemaHandler(TableSchemaInputData input, WSClient ws) {
			connInfo = input.getConnInfo();
			selectedSchemaName = input.getSchemaName();
			selectedTableName = input.getTableName();
			webhook = input.getWebhook();
			this.ws = ws;
		}
		
		@Override
		public Void call() throws Exception {
			PostBackTableSchema status = new PostBackTableSchema();
		
			final String schemaQuery = String.format(SCHEMA_QUERY, "in ('" + selectedSchemaName + "')", selectedTableName, "in ('" + selectedSchemaName + "')", selectedTableName);
			final String primaryKeyQuery = String.format(PRIMARY_KEY_QUERY, "in ('" + selectedSchemaName + "')", selectedTableName);
			final String tableSizeQuery = String.format(TABLE_SIZE_QUERY, "in ('" + selectedSchemaName + "')", selectedTableName);
			
			try (Connection conn = ConnUtils.getConnection(connInfo);
					PreparedStatement schemaStatement = conn.prepareStatement(schemaQuery);
					Statement otherStatement = conn.createStatement()) {
				
				// Need to set search path in order for pg_table_def to see tables in other
				// schemas other than information_schema and public
				DbUtils.setSearchPath(conn, DbUtils.getUserSchemas(conn));
				
				ResultSet schemaResult = schemaStatement.executeQuery();
				
				SchemaTable schemaTable = null;
				while (schemaResult.next()) {										
					// This is a new table, populate table information first
					if (schemaTable == null) {
						schemaTable = new SchemaTable();
					
						schemaTable.setSchema(schemaResult.getString("schemaname"));
						schemaTable.setName(schemaResult.getString("tablename"));
						schemaTable.setTableType("table".equalsIgnoreCase(schemaResult.getString("type")) ? TableType.TABLE : TableType.VIEW);
						if (schemaTable.getTableType() == TableType.VIEW) {
							schemaTable.setViewDef(schemaResult.getString("viewdef"));
						}					
					}
					
					// Set up columns
					SchemaColumn column = new SchemaColumn();
					column.setName(schemaResult.getString("columnname"));
					column.setDataType(schemaResult.getString("datatype"));
					column.setLength(schemaResult.getInt("length"));
					column.setPrecision(schemaResult.getInt("precision"));
					column.setScale(schemaResult.getInt("scale"));
					column.setCompressionType(schemaResult.getString("compressiontype"));
					column.setDistKey(schemaResult.getBoolean("distkey"));
					column.setSortKey(schemaResult.getInt("sortKey"));
					
					schemaTable.getColumns().add(column);
				}
				
				try {
					// Look up for table size now
					ResultSet tableSizeResult = otherStatement.executeQuery(tableSizeQuery);
					
					if (tableSizeResult.next() && schemaTable != null) {					
						schemaTable.setTableSize(tableSizeResult.getInt("tableSize"));
					}
				}
				catch (Exception e) {
					logger.info(e.getMessage(), e);
				}
				
				try {
					// Look up for primary key now
					ResultSet primaryKeyResult = otherStatement.executeQuery(primaryKeyQuery);
					
					if (primaryKeyResult.next() && schemaTable != null) {
						List<Short> primaryKeys = Lists.newArrayList((Short[]) primaryKeyResult.getArray("primarykeys").getArray());
						
						for (int primaryKey : primaryKeys) {
							schemaTable.getColumns().get(primaryKey - 1).setPrimaryKey(true);
						}
					}
				}
				catch (Exception e) {
					logger.info(e.getMessage(), e);
				}
				
				status.setSchemaStatus(SchemaStatus.SCHEMA_READY);
				status.setName(schemaTable.getName());
				status.setSchema(schemaTable.getSchema());
				status.setTableType(schemaTable.getTableType());
				status.setColumns(schemaTable.getColumns());
				status.setViewDef(schemaTable.getViewDef());
				status.setTableSize(schemaTable.getTableSize());
				
				postBack(ws, webhook, status);
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
				status.setSchemaStatus(SchemaStatus.SCHEMA_FAILED);
				status.setSchemaError(CommonUtils.convertToString(e));
				postBack(ws, webhook, status);
			}
			
			return null;
		}			
	}
}

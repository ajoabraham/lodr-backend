package io.lodr.server.controller;

import java.nio.charset.StandardCharsets;

import com.amazonaws.services.lambda.model.InvokeResult;
import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.model.SampleWranglerSourceInputData;
import io.lodr.common.util.AmazonLambdaService;
import io.lodr.common.util.JsonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.Validator;
import play.Logger;
import play.Logger.ALogger;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

public class SampleWranglerSourceController extends Controller {
	private static final ALogger logger = Logger.of(SampleWranglerSourceController.class);
	
	public SampleWranglerSourceController() {		
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result sampleWranglerSource() {		
		final LodrTimer timer = LodrTimer.newInstance(SampleWranglerSourceController.class.getName());
		timer.start();
		
		Result result = null;
		
		try {
			final String inputJson = JsonUtils.toJsonString(request().body().asJson());
			logger.info(inputJson);
			
			final SampleWranglerSourceInputData input = JsonUtils.toObject(new TypeReference<SampleWranglerSourceInputData>(){}, inputJson);
			
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
			
			InvokeResult invokeResult = AmazonLambdaService.I.invoke(CommonConstants.SAMPLE_WRANGLER_SOURCE_FUNCTION, CommonConstants.APP_MODE.getLambdaAlias(), input);
			logger.info("Submitted request to {}", CommonConstants.SAMPLE_WRANGLER_SOURCE_FUNCTION);
			
			if (invokeResult.getStatusCode() == 200) {
				result = ok(new String(invokeResult.getPayload().array(), StandardCharsets.UTF_8));	
			}			
			else if (invokeResult.getFunctionError() != null) {
				throw new Exception(invokeResult.getFunctionError());
			}
			else {
				throw new Exception("Request failed with status code - " + invokeResult.getStatusCode());
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			result = internalServerError(e.getMessage());
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
				
		return result;
	}
}

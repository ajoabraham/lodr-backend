package io.lodr.server.controller;

import io.lodr.common.util.JobStatusDbService;
import play.Logger;
import play.Logger.ALogger;
import play.mvc.Controller;
import play.mvc.Result;

public class CancelJobController extends Controller {
	private static final ALogger logger = Logger.of(CancelJobController.class);
	
	public CancelJobController() {
	}
	
	public Result cancelJob(String jobId) {
		Result result = null;
		try {
			logger.info("Cancel job - " + jobId);
			JobStatusDbService.cancelJob(jobId);
			result = ok("Successfully canceled job - " + jobId);
		}
		catch (Exception e) {
			result = internalServerError(e.getMessage());
		}
		
		return result;
	}
}

package io.lodr.server.controller;

import io.lodr.common.util.JobStatusDbService;
import play.Logger;
import play.Logger.ALogger;
import play.mvc.Controller;
import play.mvc.Result;

public class CancelQueryController extends Controller {
	private static final ALogger logger = Logger.of(CancelQueryController.class);
	
	public CancelQueryController() {
	}
	
	public Result cancelQuery(String queryId) {
		Result result = null;
		try {
			logger.info("Cancel query - " + queryId);
			JobStatusDbService.cancelQuery(queryId);
			result = ok("Successfully canceled query - " + queryId);
		}
		catch (Exception e) {
			result = internalServerError(e.getMessage());
		}
		
		return result;
	}
}


package io.lodr.server.controller;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.node.ObjectNode;

import io.lodr.common.model.PostBackDatabaseSchema;
import io.lodr.common.model.PostBackJobStatus;
import io.lodr.common.model.PostBackQueryResults;
import io.lodr.common.model.PostBackTableSchema;
import io.lodr.common.model.PostBackUnzipStatus;
import io.lodr.common.util.CommonUtils;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.mvc.Controller;

public abstract class PostBackController extends Controller {
	private static final ALogger logger = Logger.of(PostBackController.class);
	
	@Inject
	protected WSClient ws;
	
	public PostBackController() {
	}

	static <T> void postBack(WSClient ws, String webhook, T data) {		
		if (ws == null || CommonUtils.isBlank(webhook)) return;
		
		try {
			ObjectNode json = Json.newObject();
			if (data instanceof PostBackJobStatus) {				
				json.set("load_job", Json.toJson(data));
			}
			else if (data instanceof PostBackUnzipStatus) {
				json.set("zip_file", Json.toJson(data));
			}
			else if (data instanceof PostBackQueryResults) {
				json.set("execute_queries", Json.toJson(data));
			}
			else if (data instanceof PostBackDatabaseSchema) {
				json.set("database", Json.toJson(data));
			}
			else if (data instanceof PostBackTableSchema) {
				json.set("table", Json.toJson(data));
			}
			
			ws.url(webhook).put(json);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}

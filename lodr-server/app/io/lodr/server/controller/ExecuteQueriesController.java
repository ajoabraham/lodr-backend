package io.lodr.server.controller;

import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.constant.QueryState;
import io.lodr.common.model.ConnInfo;
import io.lodr.common.model.ExecuteQueriesInputData;
import io.lodr.common.model.ExecuteQueriesOutputData;
import io.lodr.common.model.PostBackQueryResults;
import io.lodr.common.model.SchemaColumn;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.ConnUtils;
import io.lodr.common.util.DbUtils;
import io.lodr.common.util.JobManager;
import io.lodr.common.util.JobStatusDbService;
import io.lodr.common.util.JsonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.Validator;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.mvc.BodyParser;
import play.mvc.Result;

public class ExecuteQueriesController extends PostBackController {
	private static final ALogger logger = Logger.of(ExecuteQueriesController.class);
	
	public ExecuteQueriesController() {
		
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result executeQueries() {
		final LodrTimer timer = LodrTimer.newInstance(ExecuteQueriesController.class.getName());
		timer.start();
				
		final ExecuteQueriesOutputData output = new ExecuteQueriesOutputData();
		
		try {   
			final String inputJson = JsonUtils.toJsonString(request().body().asJson());
			logger.info(inputJson);
			
			final ExecuteQueriesInputData input = JsonUtils.toObject(new TypeReference<ExecuteQueriesInputData>(){}, inputJson);
			
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
			
			JobManager.I.execute(new ExecuteQueriesHandler(input, ws));
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			output.setThrowable(e);
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
		
		return output.getError() == null ? ok(Json.stringify(Json.toJson(output)))
				: internalServerError(Json.stringify(Json.toJson(output)));
	}
	
	private static class ExecuteQueriesHandler implements Callable<Void> {		
		private String queryId = null;
		private List<String> queries = new ArrayList<>();
		private int rowLimit = 2000;
		private ConnInfo connInfo = null;
		private String webhook = null;
		private WSClient ws = null;
		
		public ExecuteQueriesHandler(ExecuteQueriesInputData input, WSClient ws) {
			queryId = input.getQueryId();
			queries = input.getQueries();
			rowLimit = input.getRowLimit();
			connInfo = input.getConnInfo();
			webhook = input.getWebhook();
			this.ws = ws;
		}
		
		@Override
		public Void call() throws Exception {
			final AtomicBoolean isCanceled = new AtomicBoolean(false);
			
			PostBackQueryResults postBack = new PostBackQueryResults();
			postBack.setQueryId(queryId);
			
			try (final Connection conn = ConnUtils.getConnection(connInfo);
					final Statement statement = conn.createStatement()) {
				
				Thread cancelThread = new Thread(() -> {
					while (!isCanceled.get()) {
						if (JobStatusDbService.isCanceledQuery(queryId)) {
							try {
								statement.cancel();
							}
							catch(Exception e) {								
							}
							finally {
								try {
									statement.close();
								}
								catch (Exception e) {									
								}
								isCanceled.set(true);
							}
						}
						else {
							try {
								Thread.sleep(3000);
							}
							catch (InterruptedException e) {
							}
						}
					}
				});
				
				cancelThread.start();
				
				for (String query : queries) {
					logger.info("Executing query - {}", query);
					if (CommonUtils.isBlank(query)) continue;
					
					if (isCanceled.get()) {
						logger.info("Execute queries process is canceled.");
						break;
					}
					
					postBack.clear();
					postBack.setLastExecutedQuery(query);
					
					if (query.toLowerCase().startsWith("select")) {
						postBack.setTotalRows(getTotalRows(statement, query));
//						statement.setMaxRows(rowLimit);
						query = DbUtils.appendLimitClause(query, rowLimit);
						logger.info("Modified query is {}", query);
						ResultSet resultSet = statement.executeQuery(query);
						ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

						int columnCount = resultSetMetaData.getColumnCount();
						final List<SchemaColumn> columns = new ArrayList<>();

						for (int i = 1; i <= columnCount; i++) {
							SchemaColumn column = new SchemaColumn();
							column.setName(resultSetMetaData.getColumnName(i));
							column.setDataType(resultSetMetaData.getColumnTypeName(i));
							column.setJdbcType(resultSetMetaData.getColumnType(i));
							column.setLength(resultSetMetaData.getColumnDisplaySize(i));
							column.setPrecision(resultSetMetaData.getPrecision(i));
							column.setScale(resultSetMetaData.getScale(i));
							columns.add(column);
						}
						
						List<List<String>> values = new ArrayList<>();
						
						while (resultSet.next()) {
							List<String> row = new ArrayList<>();
							for (SchemaColumn column : columns) {
								String value = null;
								if (column.getJdbcType() == Types.ARRAY) {
									Array a = resultSet.getArray(column.getName());
									if (a != null) {
										value = "{" + Arrays.stream(((Object[])a.getArray())).map(Object::toString).collect(Collectors.joining(",")) + "}";
									}
								}
								else {
									value = resultSet.getString(column.getName());
								}
								row.add(CommonUtils.isBlank(value) ? "" : value);
							}
							values.add(row);
						}
						
						postBack.getResultData().setColumns(columns);
						postBack.getResultData().setData(values);
						postBack.setFetchedRows(values.size());
						postBack.setInfo("Successfully executed query");
					}
					else {
						int updatedRows = statement.executeUpdate(query);
						postBack.setTotalRows(updatedRows);
						postBack.setFetchedRows(updatedRows);
						postBack.setInfo("Successfully executed query");						
					}
					
					postBack.setFinishedAt(Instant.now().toString());
					postBack(ws, webhook, postBack);
				}				
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
				if (!isCanceled.get()) { 
    				postBack.setStatus(QueryState.FAILED);
    				postBack.setInfo("Failed to execute query - " + postBack.getLastExecutedQuery());
    				postBack.setQueryError(CommonUtils.convertToString(e));
    				postBack.setFinishedAt(Instant.now().toString());
    				postBack(ws, webhook, postBack);
				}
			}
			finally {
				isCanceled.compareAndSet(false, true);
			}
			
			return null;
		}
		
		private static String ROW_COUNT_QUERY = "select count(*) from (%s)";
		private long getTotalRows(Statement statement, String selectQuery) {
			long rowCount = 0;
			
			try {
				String rowCountQuery = String.format(ROW_COUNT_QUERY, selectQuery);
				logger.info("Row count query is {}", rowCountQuery);
				ResultSet resultSet = statement.executeQuery(rowCountQuery);
				if (resultSet.next()) {
					rowCount = resultSet.getLong(1);
				}
			}
			catch (Exception e) {				
				logger.error(e.getMessage(), e);
			}
			
			return rowCount;
		}
		
	}
}
  
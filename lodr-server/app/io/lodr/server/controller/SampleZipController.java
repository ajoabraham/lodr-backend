package io.lodr.server.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.UnzipState;
import io.lodr.common.constant.ZipFormat;
import io.lodr.common.model.PostBackUnzipStatus;
import io.lodr.common.model.PostBackUnzipStatus.ZipFileInfo;
import io.lodr.common.model.SampleZipInputData;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.JobManager;
import io.lodr.common.util.JsonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.Validator;
import play.Logger;
import play.Logger.ALogger;
import play.libs.ws.WSClient;
import play.mvc.BodyParser;
import play.mvc.Result;

public class SampleZipController extends PostBackController {
	private static final ALogger logger = Logger.of(SampleZipController.class);
	
//	private static final String[] HANDLED_FILE_TYPES = {".csv", ".txt", ".log", ".psv", ".tsv", ".json", ".jsonl", ".jsonp"};
	
	public SampleZipController() {		
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result sampleZip() {		
		final LodrTimer timer = LodrTimer.newInstance(SampleZipController.class.getName());
		timer.start();
			
		Result result = ok("Start processing Zip file.");
		
		try {
			final String inputJson = JsonUtils.toJsonString(request().body().asJson());
			logger.info(inputJson);
			
			final SampleZipInputData input = JsonUtils.toObject(new TypeReference<SampleZipInputData>(){}, inputJson);
			
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
			
			try (final AmazonS3Service s3 = AmazonS3Service.newInstance(input.getAccessId(), input.getAccessKey(), input.getRegion())) {
				if (s3.hasWritePermission(input.getBucketName())) {
					input.setDestAccessId(input.getAccessId());
					input.setDestAccessKey(input.getAccessKey());
					input.setDestBucketName(input.getBucketName());
					input.setDestRegion(input.getRegion());
				}
				else {
					input.setDestAccessId(CommonConstants.DEFAULT_ACCESS_ID);
					input.setDestAccessKey(CommonConstants.DEFAULT_ACCESS_KEY);
					input.setDestBucketName(CommonConstants.DEFAULT_BUCKET_NAME);
					input.setDestRegion(CommonConstants.DEFAULT_REGION);
					input.setDiffBucket(true);
				}
			}
			catch (Exception e) {		
				input.setDestAccessId(input.getAccessId());
				input.setDestAccessKey(input.getAccessKey());
				input.setDestBucketName(input.getBucketName());
				input.setDestRegion(input.getRegion());
			}
			
			JobManager.I.execute(new ZipFileHandler(input, ws));
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			result = internalServerError(e.getMessage());
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
				
		return result;
	}
	
	public static class ZipFileHandler implements Callable<List<String>> {
		private WSClient ws;
		private String srcAccessId = null;
		private String srcAccessKey = null;
		private String srcRegion = null;
		private String srcBucketName = null;
		private String destAccessId = null;
		private String destAccessKey = null;
		private String destRegion = null;
		private String destBucketName = null;
		private String objectKey = null;
		private String webhook = null;
		
		private LodrTimer timer = null;
		private final File baseDir = new File(UUID.randomUUID().toString());
		private final PostBackUnzipStatus status = new PostBackUnzipStatus();
		private String objectKeyPrefix = null;
		private String inputFileName = null;
		private boolean useSubFolder = false;
		
		public ZipFileHandler(SampleZipInputData input, WSClient ws) {
			this(input, ws, false);
		}
		
		public ZipFileHandler(SampleZipInputData input, WSClient ws, boolean useSubFolder) {
			this.ws = ws;
			this.useSubFolder = useSubFolder;
			this.srcAccessId = input.getAccessId();
			this.srcAccessKey = input.getAccessKey();
			this.srcRegion = input.getRegion();
			this.srcBucketName = input.getBucketName();
			this.destAccessId = input.getDestAccessId();
			this.destAccessKey = input.getDestAccessKey();
			this.destBucketName = input.getDestBucketName();
			this.destRegion = input.getDestRegion();
			this.objectKey = input.getObjectKey();
			this.webhook = input.getWebhook();
			
			timer = LodrTimer.newInstance("Processing " + objectKey);
			
			String[] pathAndFileName = CommonUtils.getPathAndFileName(objectKey);
			inputFileName = pathAndFileName[1].toLowerCase();
			
			if (input.isDiffBucket()) {
				objectKeyPrefix = CommonUtils.getDefaultUploadObjectKeyPrefix(input.getUserId());
			}
			else {
				objectKeyPrefix = CommonUtils.isBlank(pathAndFileName[0]) ? "" : pathAndFileName[0] + CommonConstants.DEFAULT_S3_DELIMITER;
			}
    		
    		if (input.isDiffBucket()) {
    			status.setDestAccessId(destAccessId);
    			status.setDestAccessKey(destAccessKey);
    			status.setDestBucketName(destBucketName);
    			status.setDestRegion(destRegion);
    		}
		}
				
		@Override
		public List<String> call() throws Exception {
			timer.start();
    		// Unzip files to local disk
			List<File> unzippedFiles = null;
			List<String> unzippedObjectKeys = new ArrayList<>();
						
			try (final AmazonS3Service srcS3 = AmazonS3Service.newInstance(srcAccessId, srcAccessKey, srcRegion);
					final AmazonS3Service destS3 = AmazonS3Service.newInstance(destAccessId, destAccessKey, destRegion)) {				
//				postBack(ws, webhook, status);
								
	    		S3Object object = srcS3.getObject(srcBucketName, objectKey);
	    		
    			baseDir.mkdir();
    			
    			if (inputFileName.endsWith(ZipFormat.ZIP.getFileExtension())) {
    				unzippedFiles = processZipFile(object.getObjectContent());
    			}
    			else if (inputFileName.endsWith(ZipFormat.GZ.getFileExtension()) || inputFileName.endsWith(ZipFormat.GZIP.getFileExtension())) {
    				unzippedFiles = processGZipFile(object.getObjectContent(), inputFileName.substring(0, inputFileName.lastIndexOf(".")));
    			}
    			else if (inputFileName.endsWith(ZipFormat.BZ2.getFileExtension()) || inputFileName.endsWith(ZipFormat.BZIP2.getFileExtension())) {
    				unzippedFiles = processBZip2File(object.getObjectContent(), inputFileName.substring(0, inputFileName.lastIndexOf(".")));
    			}
    			else {
    				throw new Exception("Unsupported ZIP format. (Support Zip, GZip, BZip2)");
    			}
    			
	    		// Upload all files back onto S3
	    		timer.startChild("Upload " + unzippedFiles.size() + " files to S3 bucket - " + destBucketName);
	   
	    		String subFolderName = null;
	    		if (useSubFolder) {
	    			subFolderName = UUID.randomUUID().toString() + CommonConstants.DEFAULT_S3_DELIMITER;
		    		unzippedObjectKeys.add(subFolderName);
	    		}
	    		
	    		for (File unzippedFile : unzippedFiles) {
	    			String objectKey = objectKeyPrefix
	    					+ (useSubFolder ? subFolderName : "")
	    					+ unzippedFile.getName();
	    			destS3.uploadFile(destBucketName, objectKey, unzippedFile);
	    			unzippedObjectKeys.add(objectKey);
	    		}
	    		
	    		logger.info(timer.getChildDurationDesc("Upload " + unzippedFiles.size() + " files to S3 bucket - " + destBucketName));
					    		
	    		status.setStatus(UnzipState.SUCCEEDED);
				postBack(ws, webhook, status);
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
				status.setStatus(UnzipState.FAILED);
				status.setFailedReason(CommonUtils.convertToString(e));
				postBack(ws, webhook, status);
			}
			finally {
				if (unzippedFiles != null) {
    	    		// Clean up local files
    	    		for (File unzippedFile : unzippedFiles) {
    	    			unzippedFile.delete();
    	    		}
				}
				
				if (baseDir.exists()) {
    	    		baseDir.delete();
				}
			}
			
			timer.stop();
			logger.info(timer.getDurationDesc());
			
			return unzippedObjectKeys;
		}
		
		private List<File> processZipFile(InputStream inputStream) throws IOException {
			List<File> unzippedFiles = new ArrayList<>();
			
			try (ZipInputStream zipIn = new ZipInputStream(inputStream)) {	    			
				ZipEntry e = null;

				while ((e = zipIn.getNextEntry()) != null) {
					if (!e.isDirectory() && CommonUtils.isHandledFileType(e.getName())) {
						String fileName = e.getName();
						if (fileName.contains("/")) {
							fileName = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.length());
						}
						
						// ignore hidden file
						if (fileName.startsWith(".")) {
							continue;
						}
						
						timer.startChild("Unzipping " + fileName);
						logger.info("Unzipping {}", fileName);
						
						ZipFileInfo zipFileInfo = new ZipFileInfo(fileName, e.getSize(), objectKeyPrefix + fileName);
						status.getZipFiles().add(zipFileInfo);
//						postBack(ws, webhook, status);
						
						File unzippedFile = new File(baseDir, fileName);
						
						try (FileOutputStream fos = new FileOutputStream(unzippedFile)) {
							byte[] buffer = new byte[1024];
							int len = 0;
							while ((len = zipIn.read(buffer)) > 0) {
								fos.write(buffer, 0, len);
							}
						}
						logger.info(timer.getChildDurationDesc("Unzipping " + fileName));
						unzippedFiles.add(unzippedFile);
					}
				}
			}
			
			return unzippedFiles;
		}
		
		private List<File> processGZipFile(InputStream inputStream, String fileName) throws IOException {
			List<File> unzippedFiles = new ArrayList<>();
			
			try (GZIPInputStream zipIn = new GZIPInputStream(inputStream)) {
				timer.startChild("Unzipping GZIP file " + fileName);
				logger.info("Unzipping GZIP file {}", fileName);
							
				File unzippedFile = new File(baseDir, fileName);
				
				try (FileOutputStream fos = new FileOutputStream(unzippedFile)) {
					byte[] buffer = new byte[1024];
					int len = 0;
					while ((len = zipIn.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}
				}
				
				logger.info(timer.getChildDurationDesc("Unzipping GZIP file " + fileName));
				unzippedFiles.add(unzippedFile);
				
				ZipFileInfo zipFileInfo = new ZipFileInfo(fileName, unzippedFile.length(), objectKeyPrefix + fileName);
				status.getZipFiles().add(zipFileInfo);
//				postBack(ws, webhook, status);
			}
			
			return unzippedFiles;
		}
		
		private List<File> processBZip2File(InputStream inputStream, String fileName) throws IOException {
			List<File> unzippedFiles = new ArrayList<>();
			
			try (BZip2CompressorInputStream zipIn = new BZip2CompressorInputStream(inputStream)) {
				timer.startChild("Unzipping BZIP2 file " + fileName);
				logger.info("Unzipping BZIP2 file {}", fileName);
							
				File unzippedFile = new File(baseDir, fileName);
				
				try (FileOutputStream fos = new FileOutputStream(unzippedFile)) {
					byte[] buffer = new byte[1024];
					int len = 0;
					while ((len = zipIn.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}
				}
				
				logger.info(timer.getChildDurationDesc("Unzipping BZIP2 file " + fileName));
				unzippedFiles.add(unzippedFile);
				
				ZipFileInfo zipFileInfo = new ZipFileInfo(fileName, unzippedFile.length(), objectKeyPrefix + fileName);
				status.getZipFiles().add(zipFileInfo);
//				postBack(ws, webhook, status);
			}
			
			return unzippedFiles;
		}
	}
}

package io.lodr.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;

import io.lodr.common.model.ConnInfo;
import io.lodr.common.model.TestConnectionOutputData;
import io.lodr.common.util.ConnUtils;
import io.lodr.common.util.JsonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.Validator;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

public class TestConnectionController extends Controller {
	private static final ALogger logger = Logger.of(TestConnectionController.class);
		
	public TestConnectionController() {		
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result testConnection() {		
		final LodrTimer timer = LodrTimer.newInstance(TestConnectionController.class.getName());
		timer.start();
		
		Result result = null;
		
		try {
			final String inputJson = JsonUtils.toJsonString(request().body().asJson());
			logger.info(inputJson);
			
			final ConnInfo input = JsonUtils.toObject(new TypeReference<ConnInfo>(){}, inputJson);
			
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
			
			final TestConnectionOutputData output = new TestConnectionOutputData();
			
			try { 
				ConnUtils.testConnection(input);
				output.setSucceeded(true);
			}
			catch (Exception e) {
				output.setSucceeded(false);
				output.setThrowable(e);
			}
			
			result = ok(Json.stringify(Json.toJson(output)));
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			result = internalServerError(e.getMessage());
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
				
		return result;
	}
}

package io.lodr.server.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;

import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;

import io.lodr.common.constant.CSVDefaultSettings;
import io.lodr.common.constant.CommonConstants;
import io.lodr.common.constant.DataType;
import io.lodr.common.constant.JobState;
import io.lodr.common.constant.LoadMode;
import io.lodr.common.model.ColumnInfo;
import io.lodr.common.model.ConnInfo;
import io.lodr.common.model.FieldError;
import io.lodr.common.model.FileInfo;
import io.lodr.common.model.JobStatus;
import io.lodr.common.model.PostBackJobStatus;
import io.lodr.common.model.ProcessDataInputData;
import io.lodr.common.model.PublishResult;
import io.lodr.common.model.RunJobInputData;
import io.lodr.common.model.RunJobOutputData;
import io.lodr.common.model.SampleZipInputData;
import io.lodr.common.model.command.Command;
import io.lodr.common.model.command.DeleteRowsCommand;
import io.lodr.common.model.command.MergeAllRowsCommand;
import io.lodr.common.publisher.DataPublisher;
import io.lodr.common.publisher.RedshiftDataPublisher.RedshiftDataPublisherBuilder;
import io.lodr.common.util.AmazonLambdaService;
import io.lodr.common.util.AmazonS3Service;
import io.lodr.common.util.CSVUtils;
import io.lodr.common.util.CommonUtils;
import io.lodr.common.util.ConnUtils;
import io.lodr.common.util.DbUtils;
import io.lodr.common.util.JobManager;
import io.lodr.common.util.JobStatusDbService;
import io.lodr.common.util.JsonUtils;
import io.lodr.common.util.LodrTimer;
import io.lodr.common.util.Validator;
import io.lodr.server.controller.SampleZipController.ZipFileHandler;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.mvc.BodyParser;
import play.mvc.Result;

public final class RunJobController extends PostBackController {
	private static final ALogger logger = Logger.of(RunJobController.class);
	
	public RunJobController() {		
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result runJob() {
		final LodrTimer timer = LodrTimer.newInstance(RunJobController.class.getName());
		timer.start();
				
		final RunJobOutputData output = new RunJobOutputData();
		
		try {   
			final String inputJson = JsonUtils.toJsonString(request().body().asJson());
			logger.info(inputJson);
			
			final RunJobInputData input = JsonUtils.toObject(new TypeReference<RunJobInputData>(){}, inputJson);
			
			String validationMsg = Validator.I.validateInput(input);
			if (validationMsg != null) {
				throw new Exception(validationMsg);
			}
   	
    		JobManager.I.execute(new RunJobHandler(input, ws));
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			output.setThrowable(e);
		}
		
		timer.stop();
		logger.info(timer.getDurationDesc());
		
		return output.getError() == null ? ok(Json.stringify(Json.toJson(output)))
				: internalServerError(Json.stringify(Json.toJson(output)));
	}
		
	private static class RunJobHandler implements Callable<Void> {
		private String jobId = null;
		private String srcAccessId = null;
		private String srcAccessKey = null;
		private String srcBucketName = null;
		private String srcRegion = null;
		private String destAccessId = null;
		private String destAccessKey = null;
		private String destBucketName = null;
		private String destRegion = null;
		private String userId = null;
		private List<ColumnInfo> columnInfos = null;
		private List<Command> commands = null;
		private String wrangleScript = null;
		private List<String> originalColumnNames = null;
		private boolean useFilePrefix = false;
		private String filePrefix = null;
		private List<FileInfo> fileInfos = null;
		private ConnInfo connInfo = null;
		private String tableName  = null;
		private LoadMode loadMode = null;
		private String webhook = null;
		private List<String> mergeKeys = null;
		private String preSql = null;
		private String postSql = null;
		
		private WSClient ws = null;
		
		private boolean isDiffBucket = false;
		private boolean useInMemoryProcessor;
		private AtomicInteger batchId;
		
		// We need reference to original input data
		// for retry case. This is the original list of
		// jobs. Only useful for retrieving and reusing
		// ProcessDataInput. The job state etc will be wrong
		// Since it is not updated from the db
		private List<JobStatus> jobStatusCache = Lists.newArrayList();
		private boolean cancelled = false;
		
		public RunJobHandler(RunJobInputData input, WSClient ws) {
			
			jobId = input.getJobId();
			srcAccessId = input.getFileInfos().get(0).getAccessId();
			srcAccessKey = input.getFileInfos().get(0).getAccessKey();
			srcRegion = input.getFileInfos().get(0).getRegion();
    		userId = input.getFileInfos().get(0).getUserId();
			columnInfos = input.getColumnInfos();
			commands = input.getCommands();
			try {
    			wrangleScript = commands.isEmpty() ? null 
    					: JsonUtils.toJsonString(input.getCommands(), new TypeReference<List<Command>>() {});
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			originalColumnNames = input.getOriginalColumnNames();
			fileInfos = input.getFileInfos();
			loadMode = input.getLoadMode();
			useFilePrefix = input.getUseFilePrefix();
			filePrefix = input.getFilePrefix();
    		srcBucketName = input.getFileInfos().get(0).getBucketName();
			connInfo = input.getConnInfo();
			tableName = input.getTableName();
			webhook = input.getWebhook();
			mergeKeys = input.getMergeKeys();
			preSql = input.getPreSql();
			postSql = input.getPostSql();
			this.ws = ws;
			
			try (final AmazonS3Service s3 = AmazonS3Service.newInstance(srcAccessId, srcAccessKey, srcRegion)) {
    			if (s3.hasWritePermission(srcBucketName)) {
    				destAccessId = srcAccessId;
    				destAccessKey = srcAccessKey;
    				destBucketName = srcBucketName;
    				destRegion = srcRegion;
    			}
    			else {
    				destAccessId = CommonConstants.DEFAULT_ACCESS_ID;
    				destAccessKey = CommonConstants.DEFAULT_ACCESS_KEY;
    				destBucketName = CommonConstants.DEFAULT_BUCKET_NAME;
    				destRegion = CommonConstants.DEFAULT_REGION;
    				isDiffBucket = true;
    			}
			}
			catch (Exception e) {				
				destAccessId = srcAccessId;
				destAccessKey = srcAccessKey;
				destBucketName = srcBucketName;
				destRegion = srcRegion;
			}
		}
		
		@Override
		public Void call() throws Exception {
    		// All splitted output files on S3 splits folder
    		List<String> outputFileObjectKeys = new ArrayList<>();
    		final List<String> unzippedObjectKeys = new ArrayList<>();
    		PostBackJobStatus postBackJobStatus = new PostBackJobStatus();
    		postBackJobStatus.setStatus(JobState.RUNNING);
    		
    		AmazonS3Service srcS3 = null;
    		AmazonS3Service destS3 = null;
			try {				
				srcS3 = AmazonS3Service.newInstance(srcAccessId, srcAccessKey, srcRegion);
				destS3 = AmazonS3Service.newInstance(destAccessId, destAccessKey, destRegion);
						
    			final LodrTimer timer = LodrTimer.newInstance(RunJobHandler.class.getName());
    			timer.start();
    			    						    			
    			postBackJobStatus.setJobId(jobId);
    			postBackJobStatus.setInfo("Start processing job...");
    			postBack(ws, webhook, postBackJobStatus);

    			if (fileInfos.isEmpty()) {
    				throw new Exception("No file info in the input parameters.");
    			}
    			
        		List<JobStatus> jobs = new ArrayList<>();
        		final String outputFilePrefix = UUID.randomUUID().toString();
        		batchId = new AtomicInteger();
        		
        		String latestObjectKey = null;
        		boolean isZipFile = false;
    			
        		if (useFilePrefix) {        			
        			FileInfo fileInfo = fileInfos.get(0);
        			
        			// Find latest file. Assume there is only one fileInfo
        			String[] pathAndFileName = CommonUtils.getPathAndFileName(fileInfo.getObjectKey());
        			latestObjectKey = srcS3.getLatestObjectKey(fileInfo.getBucketName(), pathAndFileName[0], filePrefix);
        			
        			if (CommonUtils.isBlank(latestObjectKey)) {
        				throw new Exception("No file matched file prefix - " + filePrefix);
        			}
        			
    				postBackJobStatus.setInfo("Processing latest file matched by file prefix - " + latestObjectKey);
    				postBackJobStatus.setSelectedFile(latestObjectKey);
    				
    				logger.info("Processing latest file {} matched by file prefix {}", latestObjectKey, filePrefix);
    				
    				postBack(ws, webhook, postBackJobStatus);
    				
        			if (CommonUtils.isZipFile(latestObjectKey)) {
        				SampleZipInputData zipInputData = new SampleZipInputData();
        				zipInputData.setAccessId(srcAccessId);
        				zipInputData.setAccessKey(srcAccessKey);
        				zipInputData.setBucketName(fileInfo.getBucketName());
        				zipInputData.setRegion(fileInfo.getRegion());
        				zipInputData.setDestAccessId(destAccessId);
        				zipInputData.setDestAccessKey(destAccessKey);
        				zipInputData.setDestBucketName(destBucketName);
        				zipInputData.setDestRegion(destRegion);
        				zipInputData.setObjectKey(latestObjectKey);
        				zipInputData.setUserId(userId);
        				zipInputData.setDiffBucket(isDiffBucket);
        				
        				ZipFileHandler zipFileHandler = new ZipFileHandler(zipInputData, null, true);
        				
        				postBackJobStatus.setInfo("Unzipping " + latestObjectKey + "...");
        				postBack(ws, webhook, postBackJobStatus);
        				
        				Future<List<String>> future = JobManager.I.execute(zipFileHandler);
        				
        				unzippedObjectKeys.addAll(future.get());
        				
        				// UUID subfolder name is at index 0
        				if (unzippedObjectKeys.size() < 2) {
        					throw new Exception("Zip file " + latestObjectKey + " is empty.");
        				}
        				
        				// For zip file situation, dest info become src info        				
        				try {
            				srcAccessId = destAccessId;
            				srcAccessKey = destAccessKey;
            				srcBucketName = destBucketName;
            				srcRegion = destRegion;
            				fileInfos.get(0).setBucketName(srcBucketName);
            				
        					srcS3.close();
        				}
        				catch (Exception e) {        					
        				}
        				finally {
        					srcS3 = null;
        					srcS3 = AmazonS3Service.newInstance(srcAccessId, srcAccessKey, srcRegion);
        				}
        				
        				// If there is only one unzipped file, then just process that file
        				// If there are multiple unzipped files and given file infos contains
        				// file which does not exist in unzipped files, then just process the
        				// first unzipped file
        				// Otherwise just process all files in file infos.
        				if (unzippedObjectKeys.size() == 1) {
        					latestObjectKey = unzippedObjectKeys.get(0);
        				}
        				else {
        					String subFolderName = unzippedObjectKeys.get(0);
        					List<String> tempObjectKeys = unzippedObjectKeys.stream().skip(1).map(k -> k.replace(subFolderName, "")).collect(Collectors.toList());
        					
        					if (fileInfos.stream().map(FileInfo::getObjectKey).filter(k -> !tempObjectKeys.contains(k)).findAny().isPresent()) {	
        						latestObjectKey = unzippedObjectKeys.get(1);
        					}
        					else {
        						// Let old file info pointing to new subfolder
        						for (FileInfo f : fileInfos) {
        							int index = tempObjectKeys.indexOf(f.getObjectKey());
        							f.setObjectKey(unzippedObjectKeys.get(index + 1));
        							f.setBucketName(srcBucketName);
        						}
                				isZipFile = true;
            				}
        				}
        			}
        		}
        		else if (CommonUtils.isZipFile(fileInfos.get(0).getObjectKey())) {
        			// Check if user submit a zip file. For zip file case, we assume all files
        			// in the file infos are zip files and ignore all other files        			
    				final List<FileInfo> newFileInfos = new ArrayList<>();
    				
    				for (FileInfo fileInfo : fileInfos) {
    					if (!CommonUtils.isZipFile(fileInfo.getObjectKey())) {
    						logger.info("Ingnored non zip file - {}", fileInfo.getObjectKey());
    						continue;
    					}
    					
    					List<String> objectKeys = new ArrayList<>();
    					
        				SampleZipInputData zipInputData = new SampleZipInputData();
        				zipInputData.setAccessId(srcAccessId);
        				zipInputData.setAccessKey(srcAccessKey);
        				zipInputData.setBucketName(fileInfo.getBucketName());
        				zipInputData.setRegion(fileInfo.getRegion());
        				zipInputData.setDestAccessId(destAccessId);
        				zipInputData.setDestAccessKey(destAccessKey);
        				zipInputData.setDestBucketName(destBucketName);
        				zipInputData.setDestRegion(destRegion);
        				zipInputData.setObjectKey(fileInfo.getObjectKey());
        				zipInputData.setUserId(userId);
        				zipInputData.setDiffBucket(isDiffBucket);
        				
        				ZipFileHandler zipFileHandler = new ZipFileHandler(zipInputData, null, true);
        				
        				postBackJobStatus.setInfo("Unzipping " + fileInfo.getObjectKey() + "...");
        				postBack(ws, webhook, postBackJobStatus);
        				
        				Future<List<String>> future = JobManager.I.execute(zipFileHandler);
        				
        				objectKeys.addAll(future.get());
        				
        				// UUID subfolder name is at index 0
        				if (objectKeys.size() < 2) {
        					throw new Exception("Zip file " + fileInfo.getObjectKey() + " is empty.");
        				}
        				
        				// For zip file situation, dest info become src info        				
        				try {
            				srcAccessId = destAccessId;
            				srcAccessKey = destAccessKey;
            				srcBucketName = destBucketName;
            				srcRegion = destRegion;
            				fileInfos.get(0).setBucketName(srcBucketName);
            				
        					srcS3.close();
        				}
        				catch (Exception e) {        					
        				}
        				finally {
        					srcS3 = null;
        					srcS3 = AmazonS3Service.newInstance(srcAccessId, srcAccessKey, srcRegion);
        				}
        				
        				objectKeys.stream().skip(1).forEach(k -> {
        					FileInfo fi = (FileInfo) fileInfo.clone();
        					fi.setObjectKey(k);
        					fi.setBucketName(srcBucketName);
        					newFileInfos.add(fi);
        				});
        				
        				if (unzippedObjectKeys.isEmpty()) {
        					unzippedObjectKeys.addAll(objectKeys);
        				}
        				else {
        					unzippedObjectKeys.addAll(objectKeys.stream().skip(1).collect(Collectors.toList()));
        				}
    				}
    				
    				// Replace FileInfos with unzip files
    				fileInfos.clear();
    				fileInfos.addAll(newFileInfos);
    			}
        		
        		
        		useInMemoryProcessor = commands.stream().filter(c -> c instanceof MergeAllRowsCommand || c instanceof DeleteRowsCommand)
        				.findAny().isPresent();
        		
        		int totalRowCount = 0;    			
        		for (FileInfo fileInfo : fileInfos) {
        			List<String> columnNames = null;
        			
            		final String bucketName = fileInfo.getBucketName();
            		final String objectKey = useFilePrefix && !isZipFile ? latestObjectKey : fileInfo.getObjectKey();
            		final String encoding = fileInfo.getEncoding();
            		final String delimiter = fileInfo.getDelimiter();
            		final String quote = fileInfo.getQuote();
            		final boolean includesHeader = fileInfo.getIncludesHeader();
            		totalRowCount += fileInfo.getRowCount();
            		
            		logger.info("Creating batch jobs for {} in bucket {}", objectKey, bucketName);
            		
            		// TH 07/24/2017, S3Object need to be closed, otherwise it will run out of connection pool
        			try (final S3Object object = srcS3.getObject(bucketName, objectKey)) {            				
       		
                		// Validation logic:
                		// 1. If includesHeader is false, just continue, otherwise go to step 2
                		// 2. Current column names should match up with original column names. Order of column
                		// names could be different.
                		// 3. For each source files, all column names have to be exactly matched (both name and order)
            			
            			// TH 04/02/2017, all column names and order are not longer need to be matched. For all missing
            			// columns just insert empty string, for additional columns, just add to the end
            			if (includesHeader) {
            				try (BufferedReader in = new BufferedReader(new InputStreamReader(object.getObjectContent(), encoding))) {
                				String source = in.readLine();
                				
                				// TH 02/11/2018, ignore empty lines and comments
                				while (source != null && (source.trim().equals("") || source.trim().startsWith(CSVDefaultSettings.COMMENT_MARKER))) {
                					source = in.readLine();
                				}
                				
            					CSVFormat format = CSVUtils.createCSVFormat(delimiter, quote);
            					columnNames = CSVUtils.getColumnNames(source, format, includesHeader);
            					originalColumnNames.addAll(columnNames.stream().filter(n -> !originalColumnNames.contains(n)).collect(Collectors.toList()));
            				}
            			}
            			else {
            				columnNames = originalColumnNames;
            			}
                		
            			if (useInMemoryProcessor) {
            				final JobStatus job = new JobStatus();        			
                			ProcessDataInputData batchInput = new ProcessDataInputData();
            				
            				batchInput.setJobId(jobId);
            				batchInput.setBatchId(batchId.getAndIncrement());
            				batchInput.setAccessId(srcAccessId);
            				batchInput.setAccessKey(srcAccessKey);
            				batchInput.setRegion(srcRegion);
            				batchInput.setDestAccessId(destAccessId);
            				batchInput.setDestAccessKey(destAccessKey);
            				batchInput.setDestBucketName(destBucketName);
            				batchInput.setDestRegion(destRegion);
            				batchInput.setUserId(userId);
            				batchInput.setColumnInfos(columnInfos);
            				batchInput.setOutputFilePrefix(outputFilePrefix);
            				batchInput.setIncludesHeader(fileInfo.getIncludesHeader());
            				batchInput.setFileInfos(Lists.newArrayList(fileInfo));
            				batchInput.setWrangleScript(wrangleScript);
            				batchInput.setColumnNames(columnNames);
            				batchInput.setOriginalColumnNames(originalColumnNames);
            				
            				job.setJobId(jobId);
            				job.setBatchId(batchInput.getBatchId());
            				job.setInputData(batchInput);
            											
            				jobs.add(job);
            			}
            			else {
                    		long fileSize = object.getObjectMetadata().getInstanceLength();
                    		    				    		
                    		long position = 0;
                    		
                    		while (position < fileSize) {	
                    			final JobStatus job = new JobStatus();
                    			final long startPosition = position;
                    			
                    			ProcessDataInputData batchInput = new ProcessDataInputData();
                				
                				batchInput.setJobId(jobId);
                				batchInput.setBatchId(batchId.getAndIncrement());
                				batchInput.setAccessId(srcAccessId);
                				batchInput.setAccessKey(srcAccessKey);
                				batchInput.setRegion(srcRegion);
                				batchInput.setBucketName(bucketName);
                				batchInput.setDestAccessId(destAccessId);
                				batchInput.setDestAccessKey(destAccessKey);
                				batchInput.setDestBucketName(destBucketName);
                				batchInput.setDestRegion(destRegion);
                				batchInput.setObjectKey(objectKey);
                				batchInput.setUserId(userId);
                				batchInput.setDelimiter(delimiter);
                				batchInput.setQuote(quote);
                				batchInput.setEncoding(encoding);
                				job.setStartPosition(startPosition);
                				batchInput.setColumnInfos(columnInfos);
                				batchInput.setOutputFilePrefix(outputFilePrefix);
                				batchInput.setIncludesHeader(includesHeader);
                				batchInput.setWrangleScript(wrangleScript);
                				batchInput.setColumnNames(columnNames);
                				batchInput.setOriginalColumnNames(originalColumnNames);
                				
                				job.setJobId(jobId);
                				job.setBatchId(batchInput.getBatchId());
                				job.setInputData(batchInput);
                											
                				jobs.add(job);
                				
                				position = position + job.getBatchSize();
                    		}
            			}
                		
                		// If this is file prefix match and latest file is not a zip file,
                		// or latest file is a zip file but zip file only contains one file or file infos
                		// are not matched with unzipped files, then we only process one file.
                		if (useFilePrefix && !isZipFile) {
                			fileInfo.setObjectKey(latestObjectKey);            			
                			break;
                		}
        			}
        		}
        		
        		// Persist all jobs first
        		jobStatusCache.addAll(jobs);
        		JobStatusDbService.save(jobs);
        		
        		// Calculate post back interval
        		int postBackInterval = (int) ((totalRowCount / jobs.size()) * 0.1);
        		
        		// Submit all jobs to ProcessDataFunction
        		if (useInMemoryProcessor) {
        			JobStatus job = jobs.get(0);
        			job.getInputData().setPostBackInterval(postBackInterval);
        			AmazonLambdaService.I.invokeAsync(CommonConstants.PROCESS_DATA_IN_MEMORY_FUNCTION, 
        					CommonConstants.APP_MODE.getLambdaAlias(), job.getInputData()); 
        		    logger.info("Submitted job to {} (jobId = {}, batchId = {}, alias = {}, bucketName = {}, objectKey = {}, postBackInterval = {})", 
        		    		CommonConstants.PROCESS_DATA_IN_MEMORY_FUNCTION, jobId, job.getBatchId(), CommonConstants.APP_MODE.getLambdaAlias(), 
        		    		job.getInputData().getFileInfos().get(0).getBucketName(), job.getInputData().getFileInfos().get(0).getObjectKey(),
        		    		job.getInputData().getPostBackInterval());		
        		}
        		else {
            		for (JobStatus job : jobs) {
            			job.getInputData().setPostBackInterval(postBackInterval);
            			AmazonLambdaService.I.invokeAsync(CommonConstants.PROCESS_DATA_FUNCTION, 
            					CommonConstants.APP_MODE.getLambdaAlias(), job.getInputData()); 
            		    logger.info("Submitted job to {} (jobId = {}, batchId = {}, alias = {}, bucketName = {}, objectKey = {}, postBackInterval = {})", 
            		    		CommonConstants.PROCESS_DATA_FUNCTION, jobId, job.getBatchId(), CommonConstants.APP_MODE.getLambdaAlias(), 
            		    		job.getInputData().getBucketName(), job.getInputData().getObjectKey(),
            		    		job.getInputData().getPostBackInterval());		
            		}
        		}
        		
    			postBackJobStatus.setInfo("Submitted " + jobs.size() + " jobs");
    			postBack(ws, webhook, postBackJobStatus);
       	
    			if (unzippedObjectKeys != null && unzippedObjectKeys.size() > 1) {
    				unzippedObjectKeys.remove(0);
    			}
    			
    			// All jobs are submitted and monitoring the job results
        		// Check if task is done
        		boolean isDone = false;
        		long startTime = System.currentTimeMillis();
        		jobs = null;
        		        		
        		// If jobs are not failed or all succeeded and total run time is not exceeded max wait time, just continue
        		// to wait.
        		timer.startChild("transforming data");
        		isDone = waitForJobs(isDone, startTime, postBackJobStatus, postBackInterval);
        		jobs = JobStatusDbService.findAll(jobId);  
        		
        		if (!isDone) {
        			if (cancelled) {
        				logger.info("Job {} is canceled", jobId);
        				return null;
        			}else{
        				postBackJobStatus.setStatus(JobState.FAILED);
            			postBackJobStatus.setInfo("One or more batch jobs are not responding within time limit.");
            			postBackJobStatus.setExecutionErrors("One or more batch jobs are not finished within time limit.");
            			postBackJobStatus.setFinishedAt(Instant.now().toString());
            			postBack(ws, webhook, postBackJobStatus);
        			}	
        		}
        		
        		if (postBackJobStatus.getStatus() == JobState.FAILED) {
        			return null;
        		}
    
        		postBackJobStatus.setInfo("All data transformation are done successfully.");
        		postBackJobStatus.setLambdaDoneAt(Instant.now().toString());
        		postBack(ws, webhook, postBackJobStatus);
        		
        		logger.info("All batch jobs are done now ({})", timer.getChildDurationDesc("transforming data"));
        		
        		// Final total line count
        		long inputRows = 0;
        		long loadedRows = 0;
        		long failedRows = 0;
        		long filteredRows = 0;
        		List<FieldError> fieldErrors = new ArrayList<>();
        		
        		// Final merged column infos
        		List<ColumnInfo> mergedColumnInfos = null;
        		
        		for (JobStatus job : jobs) {
        			inputRows = inputRows + job.getInputRows();
        			loadedRows = loadedRows + job.getLoadedRows();
        			failedRows = failedRows + job.getFailedRows();
        			filteredRows = filteredRows + job.getFilteredRows();
        			for (FieldError fieldError : job.getFieldErrors()) {
        				FieldError existingFieldError = fieldErrors.stream().filter(e -> e.getColumnName().equals(fieldError.getColumnName())).findAny().orElse(null);
        				if (existingFieldError == null) {
        					fieldErrors.add(fieldError);
        				}
        				else {
        					existingFieldError.setFailedRows(existingFieldError.getFailedRows() + fieldError.getFailedRows()); 
        				}
        			}
    				outputFileObjectKeys.add(job.getOutputFileObjectKey());
    				
    				List<ColumnInfo> cis = job.getColumnInfos();
    				
    				if (mergedColumnInfos == null) {
    					mergedColumnInfos = new ArrayList<>();
    					mergedColumnInfos.addAll(cis);
    				}
    				else {
    					for (int i = 0; i < mergedColumnInfos.size(); i++) {
    						ColumnInfo mergedColumnInfo = mergedColumnInfos.get(i);
    						ColumnInfo columnInfo = cis.get(i);
    						
    						mergedColumnInfo.setLength(Math.max(mergedColumnInfo.getLength(), columnInfo.getLength()));
    						
    						if (mergedColumnInfo.getDataType() == DataType.INTEGER) {
    		 					if (columnInfo.getDataType() == DataType.BIGINT) {
    		 						mergedColumnInfo.setDataType(DataType.BIGINT);
    		 					}
    		 				}
    		 				else if (mergedColumnInfo.getDataType() == DataType.DECIMAL) {
    		 					int maxScale = Math.max(columnInfo.getScale(), mergedColumnInfo.getScale());
    		 					int maxLeft = Math.max(columnInfo.getPrecision() - columnInfo.getScale(),
    		 							mergedColumnInfo.getPrecision() - mergedColumnInfo.getScale());
    		 					mergedColumnInfo.setPrecision(maxLeft + maxScale);
    		 					mergedColumnInfo.setScale(maxScale);
    		 				}
    					}
    				}
        		}
        		
        		if (loadedRows > 0) {
            		postBackJobStatus.setInfo("Merged all batch job results and start coping into Redshift");
            		postBack(ws, webhook, postBackJobStatus);
        		}
        		else {
        			postBackJobStatus.setStatus(JobState.SUCCEEDED);
        			postBackJobStatus.setInputRows(inputRows);
            		postBackJobStatus.setTotalRowsLoaded(loadedRows);
            		postBackJobStatus.setTotalFailedRows(failedRows);
            		postBackJobStatus.setFilteredRows(filteredRows);
            		postBackJobStatus.setFieldErrors(fieldErrors);
        			postBackJobStatus.setFinishedAt(Instant.now().toString());
        			postBackJobStatus.setInfo("Import job finished successfully. No rows to load");
        			postBack(ws, webhook, postBackJobStatus);
        			
        			return null;
        		}
        		
    			if (JobStatusDbService.isCanceledJob(jobId)) {
    				logger.info("Job {} is caneled", jobId);
    				return null;
    			}
        		
        		logger.info("Start copy into Redshift (LoadMode = {})...", loadMode);
        		timer.startChild("coping data into Redshift");
        		try (Connection conn = ConnUtils.getConnection(connInfo)) {        		
        			// TH 10/27/2016, before send columnInfos into publisher, all column names
        			// need to be normalized and dedup    			
        			DataPublisher dataPublisher = RedshiftDataPublisherBuilder.create()
        					.setJobId(jobId)
        					.setOutputFileObjectKeys(outputFileObjectKeys)
        					.setConnection(conn)
        					.setAccessId(destAccessId)
        					.setAccessKey(destAccessKey)
        					.setRegion(destRegion)
        					.setBucketName(destBucketName)
    //    					.setObjectKey(objectKey)
        					.setTableName(tableName)
        					.setMergeKeys(mergeKeys)
        					.setLoadMode(loadMode)
        					.setColumnInfos(DbUtils.normalizeColumnNames(mergedColumnInfos))
        					.setPreSql(preSql)
        					.setPostSql(postSql)
        					.build();
        			PublishResult result = dataPublisher.publish();
        			
        			if (cancelled) {
        				logger.info("Job {} is canceled", jobId);
        				return null;
        			}
        			
        			postBackJobStatus.setInputRows(inputRows);
            		postBackJobStatus.setTotalRowsLoaded(loadedRows);
            		postBackJobStatus.setTotalFailedRows(failedRows);
            		postBackJobStatus.setFilteredRows(filteredRows);
            		postBackJobStatus.setFieldErrors(fieldErrors);
            		
            		if (loadMode == LoadMode.MERGE) {
            			postBackJobStatus.setUpdatedRows(result.getUpdatedRows());
            			postBackJobStatus.setInsertedRows(result.getInsertedRows());
            		}
            		else {
            			postBackJobStatus.setInsertedRows(loadedRows);
            		}
            		
            		logger.info("Import job finished successfully (inputRows = {}, totalRowsLoaded = {}, totalFailedRows = {}, updatedRows = {}, insertedRows = {}, filteredRows = {})",
            				postBackJobStatus.getInputRows(), postBackJobStatus.getTotalRowsLoaded(), postBackJobStatus.getTotalFailedRows(),
            				postBackJobStatus.getUpdatedRows(), postBackJobStatus.getInsertedRows(), postBackJobStatus.getFilteredRows());
            		postBackJobStatus.setStatus(JobState.SUCCEEDED);
            		postBackJobStatus.setInfo("Import job finished successfully.");
        		}
        		catch (Exception e) {
        			logger.error(e.getMessage(), e);
        			
        			postBackJobStatus.setStatus(JobState.FAILED);
        			postBackJobStatus.setInfo("Failed to copy data into Redshift");
        			postBackJobStatus.setExecutionErrors(CommonUtils.convertToString(e));
        		}
        		finally {
            		postBackJobStatus.setFinishedAt(Instant.now().toString());
            		postBack(ws, webhook, postBackJobStatus);    			
        		}
        		
        		logger.info(timer.getChildDurationDesc("coping data into Redshift"));
        		logger.info(timer.getDurationDesc());
			}
			catch (Exception e) {
				if(!cancelled){
					logger.error(e.getMessage(), e);
	    			
	    			postBackJobStatus.setStatus(JobState.FAILED);
	    			postBackJobStatus.setInfo("Failed to run job");
	    			postBackJobStatus.setExecutionErrors(CommonUtils.convertToString(e));
	    			
	        		postBackJobStatus.setFinishedAt(Instant.now().toString());
	        		postBack(ws, webhook, postBackJobStatus);    			
	    			
	    			throw e;
				}else{
					postBackJobStatus.setStatus(JobState.FAILED);
	    			postBackJobStatus.setInfo("Job was cancelled");	    			
	        		postBackJobStatus.setFinishedAt(Instant.now().toString());
	        		postBack(ws, webhook, postBackJobStatus);  
				}
			}
			finally {
				if (unzippedObjectKeys != null && !unzippedObjectKeys.isEmpty()) {
					srcS3.deleteObjects(srcBucketName, unzippedObjectKeys);
				}
				
				if (postBackJobStatus.getStatus() == JobState.FAILED && outputFileObjectKeys != null && !outputFileObjectKeys.isEmpty()) {					
					destS3.deleteObjects(srcBucketName, outputFileObjectKeys);
				}
				
				if (srcS3 != null) {
					srcS3.close();
				}
				
				if (destS3 != null) {
					destS3.close();
				}
				
			}
    	    		
    		return null;
		}
		
		private boolean waitForJobs(boolean isDone, long startTime, PostBackJobStatus postBackJobStatus, int postBackInterval) throws Exception{
			int waitTime = CommonConstants.DEFAULT_MAX_WAIT_TIME;
    		while (!isDone && ((System.currentTimeMillis() - startTime) <= waitTime)) {
    			if (JobStatusDbService.isCanceledJob(jobId)) {
    				logger.info("Job {} is canceled", jobId);
    				cancelled = true;
    				return false;
    			}
    			
    			List<JobStatus> jobs = JobStatusDbService.findAll(jobId);   
    			postBackJobStatus.setInputRows(0);
    			postBackJobStatus.setTotalRowsLoaded(0);
    			postBackJobStatus.setTotalFailedRows(0);
    			postBackJobStatus.setFilteredRows(0);
    			postBackJobStatus.setInputBytes(0);
    			postBackJobStatus.setInfo("Preparing and transforming data for Redshift database.");
    			
    			// Collect all stats
				isDone = true;
				List<JobStatus> retries = Lists.newArrayList();
				
    			for (JobStatus job : jobs) {  
    				boolean isRetry = false;
    				// If one of batch job is failed, just stop all processes and report the error
    				if (job.getJobState() == JobState.FAILED || (job.getJobState() == JobState.TIMEDOUT && job.getAttemptCount() >= 3) ) {
    					isDone = true;
    					postBackJobStatus.setStatus(JobState.FAILED);
    					postBackJobStatus.setExecutionErrors(job.getErrorDetails());
    					postBackJobStatus.setFinishedAt(Instant.now().toString());
    					postBackJobStatus.setInfo(String.format("One of the batch jobs (Job ID: %s, Batch ID: %d) failed.",
    							jobId, job.getBatchId()));
    					    					
    					break;
    				}
    				else if (!useInMemoryProcessor && job.getJobState() == JobState.TIMEDOUT) {
    					logger.info(String.format("Retrying Job id %s, batchId: %d that timed out", job.getJobId(), job.getBatchId() ));
    					Optional<ProcessDataInputData> origInputOpt = jobStatusCache.stream()
								.filter(jc -> jc.getBatchId()==job.getBatchId() && jc.getJobId().equalsIgnoreCase(job.getJobId()))
								.map(JobStatus::getInputData)
								.findFirst();
    					
    					if(!origInputOpt.isPresent()){
    						logger.error("Original job status not found in job status cache.  Cannot retry timed out job.");
    						isDone = true;
        					postBackJobStatus.setStatus(JobState.FAILED);
        					postBackJobStatus.setExecutionErrors(job.getErrorDetails());
        					postBackJobStatus.setFinishedAt(Instant.now().toString());
        					postBackJobStatus.setInfo(String.format("One of the batch jobs (Job ID: %s, Batch ID: %d) timedout and can't be retried.",
        							jobId, job.getBatchId()));
        					break;
    					}
    					
    					isRetry = true;
    					isDone = false;    					
    					JobStatus jobRetry1 = job.retry(origInputOpt.get().clone(), batchId.getAndIncrement(), job.getBatchSize()/2, job.getStartPosition());
    					
    					boolean iseven = job.getBatchSize() % 2 == 0;
    					
    					JobStatus jobRetry2 = null;
    					long newStart = jobRetry1.getStartPosition()+jobRetry1.getBatchSize();
    					if(iseven){
    						jobRetry2= job.retry(origInputOpt.get().clone(), batchId.getAndIncrement(), job.getBatchSize()/2, newStart);
    					}else{
    						jobRetry2 = job.retry(origInputOpt.get().clone(), batchId.getAndIncrement(), (job.getBatchSize()/2)+1, newStart);
    					}    	
    					
    					retries.add(jobRetry1);
    					retries.add(jobRetry2);
    					
    					JobStatusDbService.delete(job); 	
    					
    				}
    				else if (isDone && job.getJobState() != JobState.SUCCEEDED) {
    					isDone = false;
    				}
    				
    				if(!isRetry){
    					postBackJobStatus.setInputRows(postBackJobStatus.getInputRows() + job.getInputRows());
        				postBackJobStatus.setTotalRowsLoaded(postBackJobStatus.getTotalRowsLoaded() + job.getLoadedRows());
        				postBackJobStatus.setFilteredRows(postBackJobStatus.getFilteredRows() + job.getFilteredRows());
        				postBackJobStatus.setInputBytes(postBackJobStatus.getInputBytes() + job.getInputBytes());
        				postBackJobStatus.setTotalFailedRows(postBackJobStatus.getTotalFailedRows() + job.getFailedRows());
    				}
    				
    			}
    			
    			postBack(ws, webhook, postBackJobStatus);    			
    			
    			if(!retries.isEmpty()){
    				JobStatusDbService.save(retries);
    				int totalRowCount = (int) (postBackInterval/0.1) * (jobStatusCache.size());
    				jobStatusCache.addAll(retries);
    				postBackInterval = (int) ((totalRowCount / jobStatusCache.size()) * 0.1);    				
    				
    				for (JobStatus retry : retries) {
    					retry.getInputData().setPostBackInterval(postBackInterval);
            			AmazonLambdaService.I.invokeAsync(CommonConstants.PROCESS_DATA_FUNCTION, 
            					CommonConstants.APP_MODE.getLambdaAlias(), retry.getInputData()); 
            		    logger.info("Retrying job to {} (jobId = {}, batchId = {}, alias = {}, bucketName = {}, objectKey = {}, postBackInterval = {})", 
            		    		CommonConstants.PROCESS_DATA_FUNCTION, jobId, retry.getBatchId(), CommonConstants.APP_MODE.getLambdaAlias(), 
            		    		retry.getInputData().getBucketName(), retry.getInputData().getObjectKey(),
            		    		retry.getInputData().getPostBackInterval());		
            		}
    				
    				waitTime = waitTime + CommonConstants.DEFAULT_MAX_WAIT_TIME;
    			}
    			
    			
    			if (!isDone) {
    				try {
						Thread.sleep(5000);
					}
					catch (InterruptedException e) {
						logger.error(e.getMessage(), e);
					}
    			}
    		}
    		
    		return isDone;
		}
	}
}

name := """lodr-server"""

version := "1.0.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

EclipseKeys.projectFlavor := EclipseProjectFlavor.Java           // Java project. Don't expect Scala IDE
EclipseKeys.createSrc := EclipseCreateSrc.ValueSet(EclipseCreateSrc.ManagedClasses, EclipseCreateSrc.ManagedResources)  // Use .class files instead of generated .scala files for views and routes

libraryDependencies ++= Seq(
    javaWs,
	"io.lodr" % "lodr-common" % "1.0.0",
	"com.amazonaws" % "aws-java-sdk-core" % "1.11.49",
	"com.amazonaws" % "aws-java-sdk-s3" % "1.11.49",
	"com.amazon" % "redshift-jdbc" % "1.1.17",
	"org.apache.commons" % "commons-compress" % "1.13",
	"org.hibernate" % "hibernate-validator" % "5.3.3.Final",
	"javax.el" % "javax.el-api" % "2.2.4",
	"org.glassfish.web" % "javax.el" % "2.2.4",
	"org.apache.commons" % "commons-csv" % "1.3",
	"org.apache.commons" % "commons-collections4" % "4.1",
	"org.apache.commons" % "commons-lang3" % "3.5"
)
		
// Add .m2 repo
resolvers += Resolver.mavenLocal

// Add lodr local repo
resolvers += "Lodr Repo" at file("../repo").toURI.toASCIIString 
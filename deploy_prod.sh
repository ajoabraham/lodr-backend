#!/bin/bash

rm -rf ../prod
mkdir ../prod
git pull
mvn clean install -Dmaven.test.skip=true
cd lodr-server
activator playUpdateSecret
activator compile
activator dist
cp target/universal/lodr-server-1.0.0.zip ../../prod
cd ../../prod
unzip lodr-server-1.0.0.zip
cd lodr-server-1.0.0
bin/lodr-server -J-mx7168m -DappMode=prod -DuploadBucket=lodr-user-uploads-prod -DdefaultRegion=us-west-2
